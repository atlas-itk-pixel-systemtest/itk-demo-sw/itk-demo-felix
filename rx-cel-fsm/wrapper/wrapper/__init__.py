from wrapper._logging import log
from wrapper.wrapper import WrapperSettings


def __getattr__(name: str):
    """Redirect all traffic to Wrapper"""
    from .wrapper import Wrapper

    log.debug("Redirect to %s", name)
    return Wrapper(name)


__all__ = ["CommandLog", "WrapperSettings"]
