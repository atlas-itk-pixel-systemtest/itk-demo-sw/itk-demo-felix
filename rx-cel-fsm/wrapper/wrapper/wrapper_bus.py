from dataclasses import dataclass
from subprocess import CalledProcessError, CompletedProcess
from types import TracebackType
from typing import TYPE_CHECKING, ContextManager

from rich import print
from rich._inspect import Inspect
from rich.pretty import Pretty
from rich.table import Table

from wrapper.messages import (
    Command,
    CommandHandler,
    Event,
    EventHandler,
    Query,
    QueryHandler,
    QueryResult,
    UnitOfWork,
    bus_factory,
)
from wrapper.wrapper import Wrapper

from ._logging import log


class SubprocessUnitOfWork(UnitOfWork):
    event_history: list = []
    dump_table: bool = False
    verbose: bool = False

    _events: list[Event]

    def __enter__(self):
        return self

    def __exit__(
        self,
        exc_type: type[BaseException] | None,
        exc_value: BaseException | None,
        traceback: TracebackType | None,
    ) -> bool | None:
        if self.dump_table:
            print(self)
        return exc_type is None

    def collect_events(self):
        while self._events:
            yield self._events.pop(0)

    def add(self, obj):
        super().add(obj)
        self.event_history.append(obj)

        # inspect(error)

    def __getitem__(self, key):
        return self.event_history[key]

    def list(self):
        # for e in self.events:
        # print(e)
        return self.event_history

    def __rich__(self):
        table = Table(
            "When",
            # "Duration",
            "Result",
            "Parsed",
            highlight=True,
        )

        for c in self.event_history:
            # if self.verbose:
            #     if isinstance(c.result, CompletedProcess):
            #         render_result = f"{c.result!r}"
            #         render_parsed = Pretty(c.parsed)
            #     elif isinstance(c.result, CalledProcessError):
            #         render_result = f"{c.result!r}"
            #         render_parsed = f"{c.result.stderr}"
            #     elif isinstance(c.result, Exception):
            #         exc = c.result
            #         render_result = Inspect(
            #             exc,
            #             all=False,
            #             value=False,
            #         )
            #         render_parsed = getattr(exc, "message", repr(exc))
            #     else:
            #         from rich.scope import render_scope

            #         render_result = render_scope(
            #             vars(c.result),
            #             title=c.result.__class__.__name__,
            #             # indent_guides=True,
            #             # max_length=10,
            #             max_string=200,
            #         )
            #         render_parsed = c.parsed
            # else:
            #     # render_result = f"{c.result!r}"
            #     # render_parsed = f"{c.parsed}"

            #     render_result = ""
            #     render_parsed = ""

            # if isinstance(c.parsed, str):
            #     render_parsed = c.parsed

            # if is_renderable(c.parsed):
            #     render_parsed = c.parsed
            # else:
            #     render_parsed = Pretty(c.parsed, indent_size=2)

            table.add_row(
                c.msg_timestamp.strftime("%Y-%m-%d %H:%M:%S"),
                repr(c),
                # f"{c.duration:0.4f}",
                # render_result,
                # render_parsed,
            )
        return table


@dataclass(frozen=True, kw_only=True)
class WrapperCommand(Command):
    cmd: list[str]
    wrapper: Wrapper


@dataclass(frozen=True, kw_only=True)
class WrapperEvent(Event):
    name: str = ""


@dataclass(frozen=True, kw_only=True)
class WrapperQuery(Query):
    name: str = ""


# @rich.repr.auto
# @dataclass(frozen=True, kw_only=True)
# class WrapperEvent(Event):
#     # when: datetime.datetime = field(default_factory=datetime.datetime.now(datetime.UTC))
#     duration: int = 0
#     result: (
#         subprocess.CompletedProcess
#         | subprocess.CalledProcessError
#         | type["Wrapper"]
#         | Exception
#     )
#     parsed: dict | str = ""


class WrapperCommandHandler(CommandHandler):
    def __call__(self, command: Command) -> None:
        with self.uow as uow_:
            # cmd: WrapperCommand = command
            log.debug(command)
            uow_.add(WrapperEvent(name="Wrapper event"))


class WrapperEventHandler(EventHandler):
    def __call__(self, event: Event) -> None:
        with self.uow:
            log.info(event)


class WrapperQueryHandler(QueryHandler):
    def __call__(self, query: Query) -> QueryResult:
        with self.uow:
            print(query)
        return QueryResult(data={})


# if __name__ == "__main__":
# rich_inspect(bus)
# rich_inspect(bus.event_handlers.values())
# rich_inspect(bus.command_handlers.values())
# bus.on(WrapperCommand, Wrapper_command_handler)

# bus.command_handlers.update({WrapperCommand: WrapperCommandHandler})
# bus.event_handlers.update({WrapperEvent: [WrapperEventHandler]})
# bus.query_handlers.update({WrapperQuery: WrapperQueryHandler})

# bus.handle(WrapperCommand(name="Wrapper command"))

# from wrapper import felix

# cmd = felix()
# cmd_ = WrapperCommand(cmd)
# print(cmd)
