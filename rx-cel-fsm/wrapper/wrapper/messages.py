"""
Message Bus with CQRS

messages are commands, events or queries

SOLID
 - SRP: single responsibility per message+handler
 - OCP: add new message+handler
 - LSP: messages share common interface
 - ISP: specific handler per message
 - DIP: dependencies injected to bus and encapsulated in uow
"""

import inspect
import logging
from dataclasses import dataclass, field
from datetime import UTC, datetime
from typing import Any, Callable, Generator, Generic, Protocol, TypeVar
from uuid import UUID, uuid4

# from rich import inspect as rich_inspect
# from rich import print
from rich.logging import RichHandler

# from rich.traceback import install
# install(show_locals=True)

FORMAT = "%(message)s"
logging.basicConfig(
    level="NOTSET", format=FORMAT, datefmt="[%X]", handlers=[RichHandler()]
)
log = logging.getLogger(__name__)

# Messages

TMessage = TypeVar("TMessage")


@dataclass(frozen=True, kw_only=True)
class Message(Generic[TMessage]):
    """Message base class"""

    msg_id: UUID = field(default_factory=uuid4)
    msg_timestamp: datetime = field(default_factory=lambda: datetime.now(UTC))


@dataclass(frozen=True, kw_only=True)
class Event(Message):
    """Event base class"""

    pass


@dataclass(frozen=True, kw_only=True)
class Command(Message):
    """Command base class"""

    pass


@dataclass(frozen=True, kw_only=True)
class Query(Message):
    """Query base class"""

    pass


# Results


@dataclass(frozen=True, kw_only=True)
class Result:
    """Result base class"""


@dataclass(frozen=True, kw_only=True)
class QueryResult(Result):
    """Query result base class"""

    data: dict


# Repository

T = TypeVar("T", contravariant=True)


class Repository(Protocol[T]):
    def add(self, obj: T): ...
    def get(self, key: T): ...


class MessageRepository(Repository[Message]):
    _seen: set
    _messages: list[Message]

    def __init__(self) -> None:
        self._seen = set()
        self._messages = []

    def add(self, msg: Message):
        self._messages.append(msg)

    def get(self, key): ...

    @property
    def seen(self):
        return self._seen

    @property
    def messages(self):
        return self._messages


# Unit of Work

# class UnitOfWork(Protocol):
#     """Unit of Work Protocol"""
#     def __enter__(self) -> "UnitOfWork": ...
#     def __exit__(self, *args): ...
#     def add(self, msg: Message): ...
#     def collect_events(self) -> Generator[Message, None, None]: ...


class UnitOfWork:
    """Base Unit of Work

    just collects events locally without underlying persistence repo
    """

    # repo: Repository
    _events: list[Message]

    def __init__(self) -> None:
        self._events = []

    def __enter__(self) -> "UnitOfWork":
        self._events = []
        return self

    def __exit__(self, *args):
        pass

    def add(self, msg: Message):
        self._events.append(msg)

    def collect_events(self):
        while self._events:
            log.debug(f"'{self._events[0]!s}'")
            yield self._events.pop(0)

        # for obj in self.repo.seen:
        #     while obj.events:
        #         yield obj.events.pop(0)


# Handlers


class MessageHandler(Generic[TMessage]):
    uow: UnitOfWork

    def __init__(self, uow: UnitOfWork) -> None:
        self.uow = uow

    def __call__(self, message: TMessage) -> None | Result:
        raise NotImplementedError


class EventHandler(MessageHandler[Event]):
    def __call__(self, event: Event) -> None:
        raise NotImplementedError


class CommandHandler(MessageHandler[Command]):
    def __call__(self, command: Command) -> None:
        raise NotImplementedError


class QueryHandler(MessageHandler[Query]):
    def __call__(self, query: Query) -> QueryResult:
        raise NotImplementedError


class MessageBus:
    """Basic event/command/query Bus"""

    # event_handlers: dict[type[Event], list[EventHandler]]
    # command_handlers: dict[type[Command], CommandHandler]
    # query_handlers: dict[type[Query], QueryHandler]
    event_handlers: dict[type[Message], list[Callable]]
    command_handlers: dict[type[Message], Callable]
    query_handlers: dict[type[Message], Callable]

    queue: list[Message]
    uow: UnitOfWork

    def __init__(
        self,
        uow: UnitOfWork,
        event_handlers,
        command_handlers,
        query_handlers,
    ) -> None:
        self.event_handlers = event_handlers
        self.command_handlers = command_handlers
        self.query_handlers = query_handlers
        self.queue = []

        self.uow = uow

    def _handle_event(self, msg: Event):
        """multiple handlers, no return value, failure ok"""
        for handler in self.event_handlers[type(msg)]:
            try:
                log.debug("handling event %s with handler %s", msg, handler)
                handler(msg)
                self.queue.extend(self.uow.collect_events())
            except Exception:
                log.exception("Exception handling event %s", msg)
                continue

    def _handle_command(self, msg: Command):
        # single handler, no return value, failure not ok
        try:
            handler = self.command_handlers[type(msg)]
            log.debug(f"handling command {msg!s} with handler {handler!s}")
            # rich_inspect(handler)
            handler(msg)
            self.queue.extend(self.uow.collect_events())
        except Exception:
            log.exception("Exception handling command %s", msg)
            raise

    def _handle_query(
        self,
        msg: Query,
        results: list,
    ):
        # single handler, return value(s), failure ok
        try:
            handler = self.query_handlers[type(msg)]
            log.debug("handling query %s with handler %s", msg, handler)
            result = handler(msg)
            results.append(result)
            self.queue.extend(self.uow.collect_events())
        except Exception:
            log.exception("Exception handling query %s", msg)

    def _handle(
        self,
        msg: Message,
    ):
        results: list[QueryResult] = []
        self.queue = [msg]
        while self.queue:
            msg = self.queue.pop(0)
            # use singledispatch instead?
            match msg:
                case Event():
                    self._handle_event(msg)

                case Command():
                    self._handle_command(msg)

                case Query():
                    self._handle_query(msg, results)

                case _:
                    raise NotImplementedError(
                        f"Messages of type {type(msg)} not supported"
                    )
        return results

    def __call__(self, msg: Message):
        self._handle(msg)


# Message -> Handler Maps


# Dependency Injection


def inject(handler, dependencies):
    params = inspect.signature(handler).parameters
    deps = {name: dep for name, dep in dependencies.items() if name in params}
    log.debug("Injecting %s into %s", deps, handler)

    if issubclass(handler, MessageHandler):
        return handler(**deps)

    if inspect.isfunction(handler):
        # return lambda message: handler(message, **deps)

        def injected_handler(msg):
            return handler(msg, **deps)

        return injected_handler

    raise NotImplementedError(f"Can't inject dependencies into {handler}")


def bus_factory(
    dependencies: dict[str, Any],
    # event_handlers: dict[type[Event], list[EventHandler]],
    # command_handlers: dict[type[Command], CommandHandler],
    # query_handlers: dict[type[Query], QueryHandler],
    event_handlers: dict[type[Event], list[Callable]],
    command_handlers: dict[type[Command], Callable],
    query_handlers: dict[type[Query], Callable],
) -> MessageBus:
    eh = {
        event_type: [inject(handler, dependencies) for handler in event_type_handlers]
        for event_type, event_type_handlers in event_handlers.items()
    }
    ch = {
        command_type: inject(handler, dependencies)
        for command_type, handler in command_handlers.items()
    }
    qh = {
        query_type: inject(handler, dependencies)
        for query_type, handler in query_handlers.items()
    }

    return MessageBus(
        uow=dependencies["uow"],
        event_handlers=eh,
        command_handlers=ch,
        query_handlers=qh,
    )
