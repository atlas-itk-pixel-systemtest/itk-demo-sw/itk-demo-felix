import logging

from rich.logging import RichHandler
from rich.traceback import install

install(
    # show_locals=False,
)

FORMAT = "%(funcName)s() %(message)s"
logging.basicConfig(
    level="NOTSET",
    format=FORMAT,
    datefmt="[%X]",
    handlers=[
        RichHandler(
            markup=True,
            locals_max_length=1,
            locals_max_string=8,
        ),
    ],
)

log = logging.getLogger("rich")
logging.getLogger("asyncio").setLevel(logging.WARNING)

logging = logging
