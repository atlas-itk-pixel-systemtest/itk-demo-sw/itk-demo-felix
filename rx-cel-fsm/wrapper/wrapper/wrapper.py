import copy
import datetime
import json
import shlex
import subprocess
import time
from dataclasses import asdict, dataclass, field
from typing import ClassVar

# import rich
# import rich.repr
# from rich.scope import render_scope
from wrapper.messages import Command, Event

# from wrapper.wrapper_bus import WrapperCommand
from ._logging import log


# @rich.repr.auto
@dataclass
class WrapperSettings:
    """Wrapper Settings"""

    cmd: str = ""  # Base command
    root: bool = False  # Run command as sudo
    debug: bool = False  # Don't run command but instead print the command
    is_eager: bool = True  # Return command or run immediately

    divider: ClassVar[str] = "-"  # String to replace "_" with in commands
    class_divider: ClassVar[str] = " "  # String to place in between classes
    flag_divider: ClassVar[str] = "-"  # String to replace "_" with in flags
    double_dash: ClassVar[bool] = True  # Use -- instead of - for multi-character flags
    # output_parser: str = ""  # Output parser (yaml, json, splitlines, auto)
    cwd: ClassVar[str | None] = None  # Current working directory
    env: ClassVar[dict | None] = None  # Env for environment variables

    # def __rich__(self):
    #     return render_scope(asdict(self), title=self.__class__.__name__)


# @dataclass(frozen=True)
# class DebugMessage:
#     command: str


class Wrapper:
    settings: WrapperSettings

    def __init__(
        self,
        cmd: str,
        settings: WrapperSettings | None = None,
        **kwargs,
    ) -> None:
        if settings:
            self.settings = settings
        else:
            self.settings = WrapperSettings()
        self.settings.cmd = cmd.replace("_", self.settings.divider)

    @property
    def __doc__(self):
        return self(help=True)

    def __getattr__(self, attr):
        """Handles the creation of (sub)classes"""

        # skip Rich introspection searching for Renderables
        # if attr == "awehoi234_wdfjwljet234_234wdfoijsdfmmnxpi492":
        #     raise AttributeError
        # if attr == "aihwerij235234ljsdnp34ksodfipwoe234234jlskjdf":
        #     raise AttributeError
        # if attr.startswith("__rich"):
        #     raise AttributeError

        subcmd = (
            f"{self.settings.cmd}{WrapperSettings.class_divider}"
            f"{attr.replace('_', WrapperSettings.divider)}"
        )
        log.debug("Creating subclass %s for %s", attr, subcmd)
        subclass = Wrapper(
            subcmd,
            settings=copy.copy(self.settings),
        )
        return subclass

    def __call__(self, *args: int | str, **kwargs: int | str):
        """Receives the users commands and builds shell call

        :param args: collection of non-keyword arguments for the shell call
        :param kwargs: collection of keyword arguments for the shell call, can
        either be `key = value` for `--key value` or `key = True` for `--key`
        :returns: shell call
        """

        command = self.settings.cmd.split(" ")
        # self.settings._reset_command()
        # for key in self.settings._incidentals:
        #     setattr(self, f"_{key}", getattr(self.settings, key))

        command.extend(self._generate_command(*args, **kwargs))
        # command = self._input_modifier(command)

        if self.settings.root:
            command = ["sudo"] + command

        cmd = shlex.split(" ".join(command), posix=False)

        if self.settings.debug:
            log.info(f"Command: {cmd}")

        if self.settings.is_eager:
            return self._run_command(cmd)
        else:
            return cmd

    def _generate_command(
        self,
        *args: int | str,
        **kwargs: int | str,
    ) -> list[str]:
        """Transforms the args and kwargs to bash arguments and flags

        :param args: collection of non-keyword arguments for the shell call
        :param kwargs: collection of keyword arguments for the shell call
        :returns: Shell call
        """
        command = []
        self.settings.root = False

        for arg in args:
            if " " in str(arg):
                arg = f"'{arg}'"
            command.append(str(arg))

        for key, values in kwargs.items():
            if key.startswith("_"):
                log.debug(f"Ignoring {key} {values}")
            else:
                if not isinstance(values, list):
                    values = [values]

                for value in values:
                    if value is False:
                        self._flags_to_remove.append(self._add_dashes(key))
                    else:
                        command.append(self._add_dashes(key))
                        command[-1] += f" {value}" * (value is not True)

        return command

    def _add_dashes(self, flag: str) -> str:
        """Adds the right number of dashes for the bash flags based on the
        convention that single lettered flags get a single dash and multi-
        lettered flags get a double dash

        :param flag: flag to add dashes to
        :returns: flag with dashes
        """
        if len(flag) > 1 and WrapperSettings.double_dash:
            return f"--{flag.replace('_', self.settings.flag_divider)}"
        else:
            return f"-{flag}"

    def _run_command(self, cmd: list[str]) -> dict:
        log.info(f"{cmd}")
        try:
            tic = time.perf_counter()
            result: subprocess.CompletedProcess = subprocess.run(
                cmd,
                capture_output=True,
                cwd=self.settings.cwd,
                env=self.settings.env,
                # shell=True,
                text=True,
                check=True,
                timeout=1.0,
            )
            toc = time.perf_counter()
        except subprocess.CalledProcessError as error:
            return {"result": error}
        except Exception as exc:
            return {"result": exc}
        else:
            parsed = self._parse_output(result.stdout)
            return {"result": result, "parsed": parsed, "duration": toc - tic}

    def _parse_output(self, output: str):
        parsed = {}

        try:
            parsed["json"] = json.loads(output)
            return parsed
        except json.decoder.JSONDecodeError as json_error:
            parsed["json"] = json_error

        # try:
        #     parsed_yaml = yaml.safe_load(output)
        #     if isinstance(parsed, list) or isinstance(parsed, dict):
        #         parsed["yaml"] = parsed_yaml
        # except yaml.YAMLError as yaml_error:
        #     parsed["yaml"] = yaml_error

        parsed["splitlines"] = output.splitlines()
        return parsed

    def __repr__(self):
        return f"{self.__class__.__qualname__}({self.settings!r})"
