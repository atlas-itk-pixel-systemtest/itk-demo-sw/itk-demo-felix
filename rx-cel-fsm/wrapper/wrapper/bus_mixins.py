from functools import singledispatchmethod

from wrapper.messages import (
    Command,
    CommandHandler,
    Event,
    EventHandler,
    Message,
    MessageHandler,
    Query,
    QueryHandler,
)
from wrapper.wrapper_bus import SubprocessUnitOfWork


class DynamicMessageBus(SubprocessUnitOfWork):
    """Message Bus with on() and off() functionality to dynamically add/remove handlers"""

    @singledispatchmethod
    def on(self, msg: Message, handler: MessageHandler):
        """Set/add handler to call on message msg"""
        raise NotImplementedError(f"Messages of type {type(msg)} not supported")

    @on.register
    def _(self, msg: Event, handler: type[EventHandler]):
        self.event_handlers[type(msg)].append(handler)

    @on.register
    def _(self, msg: Command, handler: type[CommandHandler]):
        self.command_handlers[type(msg)] = handler

    @on.register
    def _(self, msg: Query, handler: type[QueryHandler]):
        self.query_handlers[type(msg)] = handler

    def off(self, msg: Message):
        """Remove handler(s) for message type msg"""
        match msg:
            case Event():
                del self.event_handlers[type(msg)]
            case Command():
                # n.b. removes all handlers for message type
                del self.command_handlers[type(msg)]
            case Query():
                del self.query_handlers[type(msg)]
            case _:
                raise NotImplementedError(
                    f"Messages of type {type(msg)} not supported",
                )
