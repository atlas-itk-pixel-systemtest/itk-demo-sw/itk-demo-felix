from dataclasses import asdict, dataclass
from re import T

from wrapper import analysis_load_yarr_scans

analysis_load_yarr_scans.settings.debug = True


@dataclass(kw_only=True)
class MinimumHealthTestParams:
    digital_scan: str
    analog_scan: str
    threshold_scan_hr: str
    moduleSN: str

    _analysis_uuid: str = ""
    _write_results_to_db: bool = True


if __name__ == "__main__":
    mht = MinimumHealthTestParams(
        digital_scan="20UPGM22110471/Jig1/010463",
        analog_scan="20UPGM22110471/Jig1/010464",
        threshold_scan_hr="20UPGM22110471/Jig1/010461",
        moduleSN="20UPGM22110471",
        # "private" attributes (starting with '_') are ignored by Wrapper
        _analysis_uuid="999999999999999",
        _write_results_to_db=False,
    )

    alys = analysis_load_yarr_scans(**asdict(mht))
    print(" ".join(alys))

    # json.dumps(mht)

    # mht = MinimumHealthTestParams(**json.loads(request.data))

    mht = MinimumHealthTestParams(**data.model_dump())
