# Subprocess Wrapper

## Wrapper

This module provides convenient "pythonic" access to subprocess calls with automatic generation of command lines, output handling (including errors and exceptions), logging and pretty printing.

Wrappers for specific subprocess calls are automatically created using the module-level attribute (PEP 562) feature:

```python
from wrapper import docker

docker.info(f='json')
```

will call:

```shell
docker info -f json
```

by converting the call into a list passed to `subprocess.run`.

The following translations rules apply:

- underscore goes to dash (configurable)
- dot operator goes to space (configurable)
- boolean parameter goes to flag
- arg goes to string
- arg with one letter goes to flag (`-*`)
- arg with more than one letter goes to long-flag (`--*`)
- arguments starting with underscore are ignored

The output, whether success, error or exception, is captured in a `WrapperEvent` event and sent to a global message bus, which acts as event log.

- In the case of success, a parsed (to dict) version of the output is added to `event.parsed`. For now JSON parsing is supported, with plain text parsing as fallback
- In the case of error (returncode not 0), stderr is parsed
- in the case of an exception, message is parsed (if present) or the repr of the exception used as fallback.7

### Settings

Settings are kept in an instance of `WrapperSettings` which comprises global settings (on class variables) and per-command settings. Some notable settings:

**debug mode** in order to just see the generated subprocess calls, use

```python
cmd.settings.debug = True
```

This will only generate `DebugEvent` objects which can be inspected with the cmdlog.

**root mode** in order to prepend the commands with `sudo` do

```python
cmd.settings.root = True
```

## Cmdlog

The gloabl Event Log cmdlog captures all event results.
It can be rich-printed:

```python
from wrapper.cmdlog import cmdlog

print(cmdlog)
```

e.g. to access the last event:

```python
last_event = cmdlog.events[-1]

if isinstance(last_event, CompletedProcess):
    print(last_event.parsed)
```
