import typer

from wrapper import (
    docker,
    # felix_info,
)

app = typer.Typer()


@app.command()
def cmd(verbose: bool = False):
    from rich import print

    # from wrapper.wrapper import
    from wrapper.wrapper_bus import (
        SubprocessUnitOfWork,
        WrapperCommand,
        WrapperCommandHandler,
        WrapperEvent,
        WrapperEventHandler,
        WrapperQuery,
        WrapperQueryHandler,
        bus_factory,
    )

    bus = bus_factory(
        dependencies={
            "uow": SubprocessUnitOfWork(),
        },
        command_handlers={WrapperCommand: WrapperCommandHandler},
        event_handlers={WrapperEvent: [WrapperEventHandler]},
        query_handlers={WrapperQuery: WrapperQueryHandler},
    )

    # uow.verbose = verbose

    # cmd = felix_info(device="/dev/flx0")
    # wcmd = WrapperCommand(cmd=cmd, wrapper=felix_info)
    # bus.handle(wcmd)

    cmd = docker.info(f="json")
    print(cmd)
    bus(WrapperCommand(cmd=cmd, wrapper=docker))
    print(bus.uow)
    # bus(WrapperQuery(docker.info))

    print(docker.info)


if __name__ == "__main__":
    app()
