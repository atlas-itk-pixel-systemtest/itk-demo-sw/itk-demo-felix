from dataclasses import asdict, dataclass

import typer

from wrapper import (
    cat,
    docker,
    felix_info,
    ls,
)

app = typer.Typer()


@app.command()
def cmd(verbose: bool = False):
    from rich import print

    # from wrapper.wrapper_bus import uow
    # uow.verbose = verbose

    felix_info()
    felix_info.settings.debug = True
    felix_info()

    ls(long=True, t=True, r=True, human_readable=True)
    cat("../example/docker/config/felix_config.json")
    docker.info(f="json")

    # print(uow[-1])

    # dump entire log
    # cmdlog.verbose = True
    # print(uow)

    # for e in cmdlog.list():
    #     if isinstance(e.result, Wrapper):
    #         print(e.result.settings)

    # dump last entry
    # print(cmdlog[-1])

    # for e in cmdlog.events:
    # print(felix_info)


if __name__ == "__main__":
    app()
