from dataclasses import dataclass

# from rich import inspect as rich_inspect
# from rich import print
from wrapper.messages import (
    Command,
    CommandHandler,
    Event,
    EventHandler,
    Query,
    QueryHandler,
    QueryResult,
    UnitOfWork,
    bus_factory,
)


# Test Messages
@dataclass(frozen=True, kw_only=True)
class TestCommand(Command):
    name: str = ""


@dataclass(frozen=True, kw_only=True)
class TestEvent(Event):
    name: str = ""

    def __str__(self) -> str:
        return self.name


@dataclass(frozen=True, kw_only=True)
class TestQuery(Query):
    name: str = ""


# Test Handlers
class TestCommandHandler(CommandHandler):
    def __call__(self, command: Command) -> None:
        with self.uow:
            # cmd: TestCommand = command
            print(command)
            self.uow.add(TestEvent(name="test event"))


class TestEventHandler(EventHandler):
    def __call__(self, event: Event) -> None:
        with self.uow:
            print(event)


class TestQueryHandler(QueryHandler):
    def __call__(self, query: Query) -> QueryResult:
        with self.uow:
            print(query)
        return QueryResult(data={})


if __name__ == "__main__":
    bus = bus_factory(
        dependencies={
            "uow": UnitOfWork(),
        },
        event_handlers={TestEvent: [TestEventHandler]},
        command_handlers={TestCommand: TestCommandHandler},
        query_handlers={TestQuery: TestQueryHandler},
    )
    # rich_inspect(bus)
    # rich_inspect(bus.event_handlers.values())
    # rich_inspect(bus.command_handlers.values())
    # bus.on(TestCommand, test_command_handler)

    # bus.command_handlers.update({TestCommand: TestCommandHandler})
    # bus.event_handlers.update({TestEvent: [TestEventHandler]})
    # bus.query_handlers.update({TestQuery: TestQueryHandler})

    bus(TestCommand(name="test command"))
