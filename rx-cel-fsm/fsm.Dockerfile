FROM almalinux:9.3

RUN dnf -y install python3 python3-devel \
    gcc make \
    && dnf -y clean all
ENV PYTHONUNBUFFERED=1 PYTHONDONTWRITEBYTECODE=1

RUN dnf -y install dnf-plugins-core \
    && dnf config-manager --set-enabled crb
RUN dnf -y install epel-release
RUN dnf -y install graphviz-devel

WORKDIR /app
COPY . .
WORKDIR fsm

RUN python3 -m ensurepip \
    && pip3 install --no-cache-dir --upgrade pip wheel setuptools \
    && pip3 install --no-cache-dir .

RUN set -eux

CMD ["python3", "server.py"]
