#from pyfelix.felix import Init, FelixConfig, FelixInfo, FelixToHost
from pyfelix.felix import Felix
from pyfelix.models import GeneralOptions

# set up FELIX context

genopt = GeneralOptions()


with Felix(genopt) as felix:

    print(felix)
    felix.probe()
    felix.init()
    felix.config()
    felix.info()

    # FELIX readout applications

    # felix.tohost()




