# Test controller to test commands

from rich import print

from commands.commands import Configure, Info, Probe, Reset
from commands.invoker import CommandFailed, CommandInvoker
from commands.receiver import DummyFelixReceiver
from pyfelix._logging import logging

log = logging.getLogger(__name__)


def run_batch():
    ch.register(Probe(rcv))
    ch.register(Reset(rcv))
    ch.register(Info(rcv))
    ch.register(Configure(rcv))
    try:
        ch()
    except CommandFailed as exc:
        log.exception(exc)
        return False
    return True


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    ch = CommandInvoker()
    rcv = DummyFelixReceiver()

    run_batch()
    print(ch)
