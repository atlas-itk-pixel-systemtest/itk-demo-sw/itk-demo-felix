
import sys
from pathlib import Path
import logging

log = logging.getLogger(__name__)


def probe(cmem: Path = "/proc/cmem_rcc",
          felix: Path = "/proc/flx",
          ):
    
    def _probe(path: Path):
        """Probe for presence of cmem_rcc Ringbuffer and FELIX driver
        """
        try:
            with open(path) as flx:
                flx_lines = flx.readlines()
                print(flx_lines)
                return True

        except IOError as exc:

            log.error("FELIX Probe failed - can't open %s (%s)", path, exc)
            return False

    return _probe(cmem) and _probe(felix)

if __name__ == "__main__":

    sys.exit(not probe())
