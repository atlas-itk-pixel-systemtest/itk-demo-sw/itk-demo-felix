from typing import Annotated

from pydantic import BaseModel, Field


class FelixGeneralOptions(BaseModel):
    """General Options"""

    bus_dir: Annotated[
        str | None, "Write felix-bus information to this directory. [default: ./bus]"
    ] = "./bus"
    bus_groupname: str | None = Field(
        default="FELIX", description="Use this groupname for the bus. [default: FELIX]"
    )
    cid: int | None = Field(
        default=0,
        description="""# CID (Connector Id) to set in FID (Felix ID), incompatible with --co. [default: device | None
    # co: int()                                  # CO (Connector Offset) to offset FID (Felix ID), incompatible with --did and --cid.
    # did: int(0)                                # DID (Detector Id) to set in FID (Felix ID), incompatible with --co. [default: 0]""",
    )

    device: int | None = Field(
        default=0,
        description="Use FLX device DEVICE. [default: 0]",
    )
    error_out: bool | None = Field(
        default=True,
        description="Write error information to a UNIX FIFO",
    )
    free_cmem: bool | None = Field(
        default=True,
        description="Free previously booked cmem segment by name-<device>-<dma>",
    )
    # iface: int()                               # Send data to the interface. [calculated: use --ip value]
    ip: str | None = Field(
        default="127.0.0.1",
        description=" Publish data on the ip address IP. [default: libfabric:127.0.0.1]",
    )
    netio_pagesize: int | None = Field(
        default=65536,
        description="NetIO page size in Byte. [default: 65536]",
    )
    netio_pages: int | None = Field(
        default=256,
        description=" Number of NetIO pages. [default: 256]",
    )
    stats_out: bool | None = Field(
        default=True,
        description="Write periodic statistics data to a UNIX FIFO",
    )
    stats_period: int | None = Field(
        default=1000,
        description="Period in milliseconds for statistics dumps. [default: 1000]",
    )
    # help: int()                               # Give' this help list
    verbose: bool | None = Field(
        default=False,
        description=" Produce' verbose output",
    )
    version: bool | None = Field(
        default=False,
        description="Print' program version",
    )
    # vid: int=1                                 # VID (Version Id) to set in FID (Felix ID), incompatible with --co. [default: 1]
    netio_watermark: int | None = Field(
        default=57344,
        description=" NetIO watermark in Byte. [default: 57344]",
    )
