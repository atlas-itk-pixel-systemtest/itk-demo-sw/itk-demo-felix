from enum import Enum
import logging
import subprocess

logger = logging.getLogger(__name__)

class RunMode(Enum):
    RUN = 0
    DRYRUN = 1
    EMULATE = 2


class Command:
    """command runner or emulator
    """

    def __init__(self, cmd: list, mode = RunMode.RUN):
        self.cmd = cmd
        self.mode = RunMode.RUN

    def to_cmdline(self):
        str_kwargs = [
            f"--{k.replace('_','-')}={v}" for k, v in self.model_dump().items()
        ]
        return self.cmd + " ".join(str_kwargs)

    def run(self):

        cmdline = self.to_cmdline()

        if self.mode == RunMode.DRYRUN:
            r = f"DRYRUN: would call {cmdline}"
            logger.info(r)
            return r

        elif self.mode == RunMode.EMULATE:

            # r = f"EMULATE: would call {cmdline}"
            try:
                with open(f"./emulator/{self.cmd}.txt") as femu:
                    r = femu.readlines()
                    logger.info(r)
            except IOError as exc:
                logger.error("no emulator txt for %s", self.cmd)
                
            return r


        logger.info(f"run_flx: Running {cmd}")

        try:
            r = subprocess.run(
                cmd,
                capture_output=True,
                text=True,
            )
        except Exception as exc:
            logger.exception(exc)

        logger.info(f"{cmd!r} exited with {r.returncode}")

        if r.stdout:
            logger.info(f"[stdout] {r.stdout}")
        if r.stderr:
            logger.info(f"[stderr] {r.stderr}")
            return r.stdout + r.stderr, False

        return r.stdout + r.stderr
