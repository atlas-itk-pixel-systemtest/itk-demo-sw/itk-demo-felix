
from typing import Optional

from pydantic import BaseModel
from rich import print

from models import GeneralOptions

class FelixToHostOptions(BaseModel):
    """felix-tohost - FELIX central data acquisition application
        Python interface
                                    # General Options
    options = [
(        '','--bus-dir'            ), # Write felix-bus information to this directory. [default: ./bus]
(        '','--bus-groupname'      ),  # Use this groupname for the bus. [default: FELIX]
(        '','--cid'                ),     #' CID (Connector Id) to set in FID (Felix ID), incompatible with --co. [default: device]
(        '','--co'                 ),    #' CO (Connector Offset) to offset FID (Felix ID), incompatible with --did and --cid.
(        '','--did'                ),     #' DID (Detector Id) to set in FID (Felix ID), incompatible with --co. [default: 0]
(    '-d', '--device'              ), #' Use FLX device DEVICE. [default: 0]
(    '','--error-out'              ), # Write error information to a UNIX FIFO
(    '','--free-cmem'              ),   #' Free previously booked cmem segment by name-<device>-<dma>
(    '','--iface'                  ), #' Send data to the interface. [calculated: use --ip value]
(    '-i', '--ip'                  ),   #' Publish data on the ip address IP. [default: libfabric:127.0.0.1]
(    '-b', '--netio-pagesize'      ), # NetIO page size in Byte. [default: 65536]
(    '-B', '--netio-pages'         ), # Number of NetIO pages. [default: 256]
(    '','--stats-out'              ), # Write periodic statistics data to a UNIX FIFO
(    '','--stats-period'           ),# Period in milliseconds for statistics dumps. [default: 1000]
(    '-?', '--help'                ),      # Give' this help list
(    '-v', '--verbose'             ),      # Produce' verbose output
(    '-V', '--version'             ),      # Print' program version
(    '','--vid'                    ), #' VID (Version Id) to set in FID (Felix ID), incompatible with --co. [default: 1]
(    '-w', '--netio-watermark'     ), # NetIO watermark in Byte. [default: 57344]

(                                  ),    # ToHost Options':

(    '',    '--buffered'           ),       # Enable' buffered mode (FULL-mode only)
(    '-c', '--cmem'                ), #' CMEM buffer size in MB. [default: 1024]
(        '','--daq-tcp'            ),       #' Use TCP/IP fo DAQ traffic.
(        '','--dcs-iface'          ), # Use TCP/IP for DCS on this network interface. [calculated: use libfabric on main iface]
(        '','--dcs-pages'          ),  # DCS Number of NetIO pages. [default: 512]
(        '','--dcs-pagesize'       ),  # DCS NetIO page size in Byte. [default: 1024]
(        '','--dcs-rate-limit'     ), # Drop DCS messages when elink message is above the given rate in KHz. [default: 0]
(    '',    '--dcs-size-limit'     ), # Drop DCS messages when message size is 0 B or above this threshold in Byte. [default: 20]
(    '',    '--dcs-timeout'        ),  # DCS NetIO timeout in ms. [default: 1]
(    '',    '--dcs-unbuffered'     ),       #' DCS unbuffered mode.
(    '',    '--dcs-watermark'      ),  # DCS NetIO watermark in Byte. [default: 972]
(    '-D', '--dma'                 ),   #' Use DMA descriptor ID. [default: 0]
(    '',    '--elink-offset'       ),     # Offset for elinks as they are output and published from felix-star (compatibility option). [default: 0]
(    '-l', '--l1id-check'          ),     # Check L1ID sequence. Formats: 1=TTC2H only, 2=LATOME, 3=FMEMU, 4=FELIG, 5=NSWVMM, 6=NSWTP.
(                                  ),     # Slow for unbuffered' DAQ. [default: 0]
(    '',    '--max-chunk-size-buffered' ),    # Maximum chunk size supported for buffered traffic. Larger chunks are truncated. [calculated: MAX_BUF_CHUNK_SIZE]
(    '-p', '--port'                ),      # Publish data on port PORT. [calculated: 53100 + 10*device + dma]
(    '-P', '--poll-period'         ),      # Polling instead of interrupt-driven readout with the given poll period in microseconds [default: 0]
(    '-R', '--priority'            ),      # Enables' round-robin scheduling for main thread (priority value 2).
(    '-s', '--dcsport'             ),      # Publish DCS data on port PORT. [calculated: 53500 + 10*device + dma]
(    '',    '--ttc-netio-pages'    ),       # TTC Number of NetIO pages. [default: 512]
(    '',    '--ttc-netio-pagesize' ),       # TTC NetIO page size in Byte. [default: 1536]
(    '',    '--ttc-netio-timeout'  ),       # TTC NetIO timeout in ms. Not necessary as TTC2H buffers are flushed at end-of-block. [default: 0]
(    '',    '--ttc-netio-watermark'),       # TTC NetIO watermark in Byte. [default: 1248]
(    '-t', '--ttcport'             ),      # Publish TTC2H data on port PORT. [calculated: 53300 + 10*device + dma]
(    '-T', '--netio-timeout'       ),      # NetIO timeout in ms. [default: 2]
(    '', '--warn-bcr-period'       ),    # Print warning message when non-periodic BCR is detected.
    ]
"""

 
    # ToHost Options

    buffered: Optional[bool]=Field(default=False)           # Enable' buffered mode (FULL-mode only)
    cmem: Optional[int]=Field(default=1024)                 # CMEM buffer size in MB. [default: 1024]
    daq_tcp: Optional[bool]=Field(default=True)             # Use TCP/IP fo DAQ traffic.
    # dcs_iface:                                            # Use TCP/IP for DCS on this network interface. [calculated: use libfabric on main iface]
    dcs_pages: Optional[int]=Field(default=512)             # DCS Number of NetIO pages. [default: 512]
    dcs_pagesize: Optional[int]=Field(default=1024)         # DCS NetIO page size in Byte. [default: 1024]
    dcs_rate_limit: Optional[int]=Field(default=0)          # Drop DCS messages when elink message is above the given rate in KHz. [default: 0]
    dcs_size_limit: Optional[int]=Field(default=20)         # Drop DCS messages when message size is 0 B or above this threshold in Byte. [default: 20]
    dcs_timeout: Optional[int]=Field(default=1)             # DCS NetIO timeout in ms. [default: 1]
    dcs_unbuffered: Optional[bool]=Field(default=False)     # DCS unbuffered mode.
    dcs_watermark: Optional[int]=Field(default=972)         # DCS NetIO watermark in Byte. [default: 972]
    dma: Optional[int]=Field(default=0)                     # Use DMA descriptor ID. [default: 0]
    elink_offset: Optional[int]=Field(default=0)            # Offset for elinks as they are output and published from felix-star (compatibility option). [default: 0]
    l1id_check: Optional[int] =Field(default=0)             # Check L1ID sequence. Formats: 1=TTC2H only, 2=LATOME, 3=FMEMU, 4=FELIG, 5=NSWVMM, 6=NSWTP.  Slow for unbuffered' DAQ. [default: 0]
    # port: int=53100                                       # Publish data on port PORT. [calculated: 53100 + 10*device + dma]
    # max_chunk_size_buffered:                              # Maximum chunk size supported for buffered traffic. Larger chunks are truncated. [calculated: MAX_BUF_CHUNK_SIZE]
    poll_period: Optional[int]=Field(default=0)             # Polling instead of interrupt-driven readout with the given poll period in microseconds [default: 0]
    priority: Optional[int]=Field(default=2)                # Enables' round-robin scheduling for main thread (priority value 2).
    # dcsport:                                              # Publish DCS data on port PORT. [calculated: 53500 + 10*device + dma]
    ttc_netio_pages: Optional[int]=Field(default=512)       # TTC Number of NetIO pages. [default: 512]
    ttc_netio_pagesize: Optional[int]=Field(default=1536)   # TTC NetIO page size in Byte. [default: 1536]
    ttc_netio_timeout: Optional[int]=Field(default=0)       # TTC NetIO timeout in ms. Not necessary as TTC2H buffers are flushed at end-of-block. [default: 0]
    ttc_netio_watermark: Optional[int]=Field(default=1248)  # TTC NetIO watermark in Byte. [default: 1248]
    # ttcport:                                              # Publish TTC2H data on port PORT. [calculated: 53300 + 10*device + dma]
    netio_timeout: Optional[int]=Field(default=2)           # NetIO timeout in ms. [default: 2]
    warn_bcr_period: Optional[bool]=Field(default=True)     # Print warning message when non-periodic BCR is detected.

class FelixToFlxOptions(GeneralOptions):
    """felix-toflx - FELIX central data acquisition application
        Python interface

    ToFlx Options:
        '',   '--netio-buffersize', #=SIZE  # NetIO receive buffer size in byte; maximum size for a single message. [default: 65536]
        '',   '--netio-buffers', #=SIZE     # Number of NetIO receive buffers. [default: 256]
        '-c', '--cmem', #=SIZE              # CMEM buffer size in MB. [default: 20]
        '-D', '--dma,' #=ID                 # Use DMA descriptor ID. [calculated: highest dma]
        '-m', '--cdma',                     # Use circular DMA buffer instead of one-shot
        '-p', '--port', #=PORT              # Send data to port PORT. [calculated: 53200 + 10*device + dma]
        '-r', '--rawtcp',                   # Use raw tcp not libfabric (can be used with --unbuffered)
        '',   '--stats-stdout',             # Prints stats to stdout
        '-u', '--unbuffered',               # Use unbuffered mode
"""

    # ToFlx Options:
    netio_buffersize: Optional[int]=Field(default=65536)    # NetIO receive buffer size in byte; maximum size for a single message. [default: 65536]
    netio_buffers: Optional[int]=Field(default=256)         # Number of NetIO receive buffers. [default: 256]
    cmem: Optional[int]=Field(default=20)                   # CMEM buffer size in MB. [default: 20]
    dma: Optional[int]=Field(default=0)                     # Use DMA descriptor ID. [calculated: highest dma]
    cdma: Optional[bool]=Field(default=False)               # Use circular DMA buffer instead of one-shot
    # port: #=PORT                                          # Send data to port PORT. [calculated: 53200 + 10*device + dma]
    rawtcp: Optional[bool]=Field(default=False)             # Use raw tcp not libfabric (can be used with --unbuffered)
    stats_stdout: Optional[bool]=Field(default=False)       # Prints stats to stdout
    unbuffered: Optional[bool]=Field(default=False)         # Use unbuffered mode


class FelixToHost(Command, FelixToFlxOptions):

    def __init__(self, opt) -> None:
        super().__init__()

genopt = GeneralOptions(
    bus_dir='/bus',
)

flx_tohost = FelixToHost(opt=genopt)
# flx_tohost.

# print(flx_tohost.model_dump_json())

print(flx_tohost.model_dump(exclude_defaults=True)) #to_cmdline())



