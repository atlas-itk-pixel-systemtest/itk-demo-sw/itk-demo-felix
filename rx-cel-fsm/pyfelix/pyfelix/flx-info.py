
import subprocess
import logging
from pyfelix.models import FelixCard

log=logging.getLogger(__name__)

card_data = {
    'number': 1,    
}

card = FelixCard(**card_data)

print(card) 

props = [
  'COUNT',      # Display the number of FELIX devices installed in the host.
  'CARDS',      # Display the number of FELIX cards installed in the host.
  'FPGA',       # Display the status of the FPGA.
  'POWER',      # Display the status of the power monitoring devices (LTC2991 or IN226/TMP435).
  'POD',        # Display the overall status of the MiniPODs.
  'PODTEMP',    # Display temperature and voltage (VCC) readings of the MiniPODs.
  'PODVCC',     # Display temperature and voltage (VCC) readings of the MiniPODs.
  'PODPOWER',   # Display optical power readings of the MiniPODs.
  'PODPOWERRX', # Display optical power readings of the RX MiniPODs only.
  'TTC',        # Display info from TTC related registers.
  'LTI',        # Display info from LTI related registers.
  'FREQ',       # Display the RXUSRCLK frequency.
  'LINK',       # Display the channel alignment status (plus PLL LOL and FEC error counters).
  'GBT',        # Display the channel alignment status (plus PLL LOL and FEC error counters).
  'ELINK',      # Display the E-link alignment status (plus 'PATH' error counters).
  'LINKRESET',  # Display the channel auto-reset counters.
  'ADN2814',    # Display the ADN2814 register 0x4.
  'CXP',        # Display the temperature and voltage from CXP1 and CXP2.
  'SFP',        # Display info from the Small Form Factor Pluggable transceivers.
  'DDR3',       # Display the values from DDR3 RAM memory.
  'SI5324',     # Display the SI5324 status (on FLX-709 only).
  'SI5345',     # Display the SI5345 status.
  'LMK03200',   # Display the LMK03200 status.
  'ICS8N4Q',    # Display the ICS8N4Q status (on FLX-710 only).
  'FFLY',       # Display status of the FireFly modules (FLX-182 only).
  'FIREFLY',    # Display status of the FireFly modules (FLX-182 only).
  'EGROUP',     # Display the values from EGROUP registe
  # [<ch>] [RAW]
                # if no channel number <ch> is specified, display all availab
                # using hexadecimal notation if RAW is specified.
  'LSPCI',      # Output of 'lspci | grep -e Xil -e CERN'.
  'ALL',        # Display all.
]

class FlxInfoError(Exception):
  pass

class FlxInfo:
    # def __init__(self) -> None:
    #     for p in props:
    #         setattr(FlxInfo, p, p)
  def __getattr__(self, attr):
    def run_cmd(*args, **kwargs):
      str_kwargs = ['--{}={}'.format(k, v) for k, v in kwargs.items()]
      cmd = ["flx-info", attr]+str_kwargs+list(args)
      log.info(cmd)
      if attr.upper() not in props:
        log.error("Command %s not available", attr.upper())
        raise FlxInfoError
      # return subprocess.run(
      #   cmd,
      #   # cwd=
      #   capture_output=True,
      #   text=True,
      # )
    return run_cmd

flx_info = FlxInfo()

# try:
#   flx_info.test('z')
# except FlxInfoError:
#   pass

try:
  flx_info.pod('z')
except FlxInfoError:
  pass



