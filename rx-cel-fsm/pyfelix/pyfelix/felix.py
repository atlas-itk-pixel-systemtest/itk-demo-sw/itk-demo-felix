import json

from pydantic import BaseModel
from rich.pretty import pprint

from pyfelix._logging import logging
from pyfelix.models import FelixGeneralOptions
from pyfelix.probe import probe
from pyfelix.wrapper.wrapper import Wrapper

log = logging.getLogger(__name__)


class FelixException(Exception):
    pass


class FelixCommand(BaseModel):
    cmd: str
    settings: dict | None = None
    options: dict | None = None


class Felix:
    """FELIX Software CLI tools wrapper for Python"""

    def __init__(
        self,
        settings: dict | None = None,
        general_options: dict | None = None,
        # **kwargs,
    ):
        self.settings = settings

        if not general_options:
            self.general_options = FelixGeneralOptions()
        else:
            self.general_options = FelixGeneralOptions(**general_options)
            log.debug(
                "Changed general options: ",
                self.general_options.model_dump(
                    exclude_defaults=True,
                ),
            )

        """allowed FELIX commands and utility methods"""
        self.commands = {
            "info": FelixCommand(
                cmd="flx-info",
                settings=self.settings,
                options=self.general_options.model_dump(),
            ),
            "cat": FelixCommand(
                cmd="cat",
                settings=dict(
                    settings=self.settings,
                    output_parser="jc:proc",
                ),
            ),
            # "init"      : "flx-init",  # Initialize FELIX card
            # "config"    : "flx-config",
            # "to_host"   : "felix-tohost",
            # "to_felix"  : "felix-toflx",
        }

    def __enter__(self):
        log.debug("Welcome to your FELIX context.")

        if not probe():
            raise FelixException

        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type:
            log.exception("exc")
        else:
            log.debug("Bye from your FELIX context.")

    def __repr__(self) -> str:
        return json.dumps(
            {
                "Wrapper Settings": self.settings,  # .model_dump(),
                "FELIX General Options": self.general_options.model_dump(),
                # "Commands": {cmd: repr(getattr(self, cmd)) for cmd in self.commands},
            }
        )

    def __getattr__(self, attr):
        """Create wrapper if command valid"""
        if attr in self.commands.keys():
            command = self.commands[attr]
            log.debug(command)
            return Wrapper(**command.model_dump())
            # return Wrapper( c, settings= self.settings, **self.general_options.model_dump() )
        else:
            return super.__getattr__(attr)


def main():
    logging.basicConfig(level=logging.INFO)
    log.info("Welcome to the FELIX Python wrapper")

    # try:
    #     with Felix() as felix:
    #         pprint(vars(felix))

    # except FelixException as e:
    #     log.error("Could not enter FELIX context.")


if __name__ == "__main__":
    main()
