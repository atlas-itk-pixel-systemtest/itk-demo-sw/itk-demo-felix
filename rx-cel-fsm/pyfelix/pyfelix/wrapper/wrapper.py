# pyfelix wrapper
#
# 2024, G. Brandt <gbrandt@cern.ch>
#
# modified from Universal Wrapper
# https://github.com/Basdbruijne/Wrapper
#
# Copyright 2022 by Bas de Bruijne
# All rights reserved.
# Universal Wrapper comes with ABSOLUTELY NO WARRANTY, the writer can not be
# held responsible for any problems caused by the use of this module.

import copy
import json

# import logging
import shlex
import subprocess
from dataclasses import asdict, dataclass, field
from typing import ByteString

# import autothread
import jc
import yaml

# from pydantic import BaseModel
from pyfelix._logging import logging

log = logging.getLogger(__name__)


@dataclass
class WrapperSettings:
    """This class provides variable tracking for the Wrapper class. These
    variables define the behavior in which the wrapper converts calls to subprocess
    commands.
    """

    # Global settings

    cmd: str = ""  # Base command
    divider: str = "-"  # String to replace "_" with in commands
    class_divider: str = " "  # String to place in between classes
    flag_divider: str = "-"  # String to replace "_" with in flags
    input_add: dict[str, int] = field(
        default_factory=dict
    )  # {extra command, index where to add it}
    input_move: dict[str, int] = field(
        default_factory=dict
    )  # {extra command, index where to move it}
    input_custom: list[str] = field(
        default_factory=list
    )  # custom command: e.g. "command.reverse()"
    output_custom: list[str] = field(
        default_factory=list
    )  # custom command: e.g. "output.reverse()"

    # Local or global settings

    root: bool = False  # Run commands as sudo, same as `input_add={0: "sudo"}`
    debug: bool = False  # Don't run commands but instead print the command
    emulate: bool = False  # Don't run, print output from toy emulator library
    double_dash: bool = True  # Use -- instead of - for multi-character flags
    output_yaml: bool = False  # Parse yaml from output
    output_json: bool = False  # Parse json from output
    enable_async: bool = False  # Globally enable asyncio
    return_stderr: bool = False  # Forward stderr output to the return values
    output_splitlines: bool = False  # Split lines of output
    output_decode: bool = True  # Decode output to str
    output_parser: str = ""  # Output parser (yaml, json, splitlines, auto)
    warn_stderr: bool = False  # Forward stderr output to warnings
    cwd: str | None = None  # Current working directory
    env: dict | None = None  # Env for environment variables
    run_async: bool = False  # run subprocess in background with async
    run_parallel: bool = False  # run subprocess in background, but without async

    def _reset_command(self, divider: str | None = None) -> None:
        """Resets cmd_chain to its original value

        When a command is chained, its chain history is stored in cmd_chain. After that
        command has been called the chain needs to be reset to the original command.
        :param divider: The new divider, only applicable if the divider has changed
        """
        if divider:
            self.cmd = self.cmd.replace(self.divider, divider)


class SubprocessError(subprocess.CalledProcessError):
    """Error class derived from subprocess.CalledProcessError to make the error message
    more intuitive by including the commands output
    """

    def __str__(self) -> str:
        """Compiles variables from self to a coherent error message

        :returns: Error message
        """
        msg = f"{super().__str__()[:-1]}:\n"
        for err in ("stdout", "stderr"):
            std = getattr(self, err)
            if std:
                std = std.decode().strip().replace("\n", "\n| ")
                msg += f"{err}:\n| {std}\n"
        return msg


class Wrapper:
    def __init__(
        self, cmd: str, settings: WrapperSettings | None = None, **kwargs
    ) -> None:
        self.settings = WrapperSettings()
        if settings:
            self.settings = settings
            log.debug(
                "Settings: %s",
                asdict(self.settings),
                # self.settings.model_dump_json(exclude_defaults=True),
            )

        self.settings.cmd = cmd.replace("_", self.settings.divider)
        self.settings._reset_command()
        self._flags_to_remove: list = []

        # log.debug("Wrapper for %s", self.settings.cmd)

    # @property
    # def __doc__(self):
    #     return self(help=True)

    # def __repr__(self):
    #     return f"Wrapper('{self.settings.cmd}', {vars(self.settings)}"

    # def __str__(self):
    #     return str(f"<Wrapper '{self.settings.cmd}'>")

    def __call__(self, *args: int | str, **kwargs: int | str) -> str:
        """Receives the users commands and directs them to the right functions

        :param args: collection of non-keyword arguments for the shell call
        :param kwargs: collection of keyword arguments for the shell call, can
        either be `key = value` for `--key value` or `key = True` for `--key`
        :returns: Response of the shell call
        """
        command = self.settings.cmd.split(" ")
        # log.debug("Call (%s)", command)
        self.settings._reset_command()

        # for key in self.settings._incidentals:
        #     setattr(self, f"_{key}", getattr(self.settings, key))
        command.extend(self._generate_command(*args, **kwargs))
        command = self._input_modifier(command)

        if self._root:
            command = ["sudo"] + command
        cmd = shlex.split(" ".join(command), posix=False)

        if self.settings.debug:
            return f"Generated command: {' '.join(cmd)}"

        if self.settings.emulate:
            log.debug(f"Emulate command: {' '.join(cmd)}")

            try:
                with open("emulator/" + cmd[0] + ".txt") as f:
                    return f.read()
            except Exception as exc:
                log.exception(exc)
                return f"--- can't emulate {cmd} --- ({exc})"

        # elif self.settings.run_async:
        # return self._async_run_cmd(cmd)
        elif self.settings.run_parallel:
            return self._run_cmd_parallel(cmd)
        else:
            return self._run_cmd(cmd)

    def _generate_command(self, *args: int | str, **kwargs: int | str) -> list[str]:
        """Transforms the args and kwargs to bash arguments and flags

        :param args: collection of non-keyword arguments for the shell call
        :param kwargs: collection of keyword arguments for the shell call
        :returns: Shell call
        """
        command = []
        self._root = False
        for string in args:
            if " " in str(string):
                string = f"'{string}'"
            command.append(str(string))
        for key, values in kwargs.items():
            # if key.startswith("_") and key[1:] in self.settings._incidentals:
            #     setattr(self, key, values)
            # else:
            if not isinstance(values, list):
                values_ = [values]
            else:
                values_ = values
            for value in values_:
                if value is False:
                    self._flags_to_remove.append(self._add_dashes(key))
                else:
                    command.append(self._add_dashes(key))
                    command[-1] += f" {value}" * (value is not True)
        return command

    def _add_dashes(self, flag: str) -> str:
        """Adds the right number of dashes for the bash flags based on the
        convention that single lettered flags get a single dash and multi-
        lettered flags get a double dash

        :param flag: flag to add dashes to
        :returns: flag with dashes
        """
        if len(str(flag)) > 1 and self._double_dash:
            return f"--{flag.replace('_', self.settings.flag_divider)}"
        else:
            return f"-{flag}"

    def _input_modifier(self, command: list[str]) -> list[str]:
        """Handles the input modifiers, e.g. adding and moving commands

        :param command: list of initial commands
        :returns: Modified list of commands based on settings input
        modifiers
        """
        for input_command, index in self.settings.input_add.items():
            if input_command.split(" ")[0] not in self._flags_to_remove:
                command = self._insert_command(command, input_command, index)
        self._flags_to_remove = []
        for move_command, index in self.settings.input_move.items():
            cmd = [cmd.split(" ")[0] for cmd in command]
            if move_command in cmd:
                command_index = cmd.index(move_command)
                popped_command = command.pop(command_index)
                command = self._insert_command(command, popped_command, index)
        for cmd in self.settings.input_custom:
            exec(cmd)
        return command

    def _insert_command(
        self, command: list[str], input_command: str, index: int
    ) -> list[str]:
        """Combines list.append and list.inset to a continues insert function

        :param command: Initial command to add items to
        :param input_command: Item to add to command
        :param index: Index to add command, must be in range(0, len(command))
        or -1
        :returns: Modified command
        """
        if index == -1:
            command.append(input_command)
            return command
        elif index < 0:
            index += 1
        command.insert(index, input_command)
        return command

    def _run_cmd(self, cmd: list[str]) -> str:
        """Forwards the generated command to subprocess

        :param: list of string which combined make the shell command
        :returns: Output of shell command
        """
        log.debug(" %s", cmd)
        try:
            proc = subprocess.Popen(
                cmd,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                cwd=self.settings.cwd,
                env=self.settings.env,
            )
        except Exception as exc:
            log.exception(exc)
            return str(exc)

        stdout, stderr = proc.communicate()
        return self._raise_or_return(stdout, stderr, proc.returncode, cmd)

    def _raise_or_return(
        self, stdout: ByteString, stderr: ByteString, return_code: int, cmd: list[str]
    ) -> str:
        """Handles the error displaying for the subprocesses

        :param stdout: subprocess output
        :param stderr: subprocess error output
        :param return_code: return code of the process
        :param cmd: original command, used for error message
        :returns: Output of shell command
        """
        if return_code == 0:
            if stderr and self.settings.warn_stderr:
                # warnings.warn("\n" + stderr.decode(), UserWarning, stacklevel=4)
                log.warn("stderr: %s", stderr)
            if self.settings.return_stderr:
                stdout = stderr + b"\n" + stdout
            if self.settings.output_parser:
                return self._parse_output(stdout)
            else:
                return self._output_modifier(stdout)
        raise SubprocessError(return_code, cmd, stdout, stderr)

    def _parse_output(self, output: str):
        output = output.decode()
        options = ["yaml", "json", "splitlines", "auto", "jc"]
        if self.settings.output_parser.split(":")[0] not in options:
            raise ValueError(
                f"{self.settings.output_parser} is not a valid parser, choose from {options}"
            )

        if self.settings.output_parser == "yaml":
            return yaml.safe_load(output)
        elif self.settings.output_parser == "json":
            return json.loads(output)
        elif self.settings.output_parser == "splitlines":
            return output.splitlines()
        elif self.settings.output_parser == "auto":
            try:
                return json.loads(output)
            except json.decoder.JSONDecodeError:
                pass
            try:
                parsed = yaml.safe_load(output)
                if isinstance(parsed, list) or isinstance(parsed, dict):
                    return parsed
            except yaml.YAMLError:
                pass
            return output
        elif self.settings.output_parser.startswith("jc"):
            parser = self.settings.output_parser.split(":")[1]
            # print(output)
            log.debug("Parsing with jc %s", parser)
            return jc.parse(parser, output)
            # return json.loads(output)

    def _output_modifier(self, output: str) -> str:
        """Modifies the subprocess' output according to settings

        :param output: string to modify, e.g. parse
        :returns: modified output
        """
        if self.settings.output_decode:
            output = output.decode()

        if self.settings.output_yaml:
            output = yaml.safe_load(output)

        if self.settings.output_json:
            output = json.loads(output)

        if self.settings.output_splitlines:
            output = output.splitlines()

        for cmd in self.settings.output_custom:
            exec(cmd)

        return output

    def __getattr__(self, cmd):
        """Handles the creation of (sub)classes"""
        log.debug("Creating subclass %s", cmd)
        subclass = Wrapper(
            f"{self.settings.cmd}{self.settings.class_divider}"
            f"{cmd.replace('_', self.settings.divider)}",
            settings=copy.copy(self.settings),
        )
        return subclass


def __getattr__(name: str):
    """Redirects all traffic to Wrapper"""
    log.debug("Wrapping %s", name)
    return Wrapper(name)
