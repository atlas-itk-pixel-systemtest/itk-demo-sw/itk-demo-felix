

/!\ DEPRECATED
---

# FELIX Python Wrapper

The missing Pythonic wrapper around the FELIX CLI commands.

## Low Level Tools

As described in the FELIX manual Section 6.2 on [Low Level Tools](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-doc/felix-user-manual/5.x/6_basic_tools.html#6-2-low-level-tools)

### Toolstable from FELIX Manual

List of all recommended user tools.

Low Level Commands
| Implemented | Command            | Description                                                                                    |
| ----------- | ------------------ | ---------------------------------------------------------------------------------------------- |
| (✅)         | `flx-info`         | View various FELIX hardware and firmware information and status.                               |
| ❌           | `flx-config`       | View and modify low-level firmware parameters by reading and writing FELIX firmware registers. |
| ❌           | `flx-init`         | Initialise FELIX, as well set as low level GBT and clock/jitter cleaning parameters.           |
| ❌           | `flx-reset`        | Reset FELIX or a specific component.                                                           |
| ❌           | `flx-pod`          | Display or enable/disable individual MiniPOD channels.                                         |
| ❌           | `fcap`             | View FELIX firmware E-link configuration capabilities and some other firmware properties.      |
| ❌           | `felix-cmem-free`  | Manually deallocate memory in CMEM buffer.                                                     |
| ❌           | `flx-busy-mon`     | Monitor a FELIX card's BUSY signal.                                                            |
| ❌           | `flx-dma-stat`     | Display the status of a FELIX device's DMA controllers (expert tool).                          |
| ❌           | `flx-irq-counters` | Inspect or reset FELIX card interrupt counters (expert tool).                                  |



| ❌         | Dataflow Tools                                                                            |     |
| --------- | ----------------------------------------------------------------------------------------- | --- |
| `fdaq`    | Receive data from a single FELIX device and save to files or perform sanity checks.       |
| `fdaqm`   | Receive data from multiple FELIX devices and save to files or perform sanity checks.      |
| `fupload` | Upload specific data patterns or data from file to a front-end E-link via a FELIX device. |

| ❌             | FELIX Configuration Tools                                                                           |     |
| ------------- | --------------------------------------------------------------------------------------------------- | --- |
| `elinkconfig` | GUI for link and data generator configuration and configuration file creation.                      |
| `felink`      | Calculate E-link IDs given inputs with differing formats.                                           |
| `fereverse`   | Reverse the endianness of data passing through an E-link.                                           |
| `fgpolarity`  | Switch 0/1 polarity of all data coming or going through a specific GBT link.                        |
| `feconf`      | Upload link and/or data generator configuration to FELIX from the command line.                     |
| `femu`        | Status and control of the FELIX data generators.                                                    |
| `ffmemu`      | Status and control of the data generator of the FMEMU firmware.                                     |
| `fttcemu`     | Status and control of the FELIX TTC data generator.                                                 |
| `fttcbusy`    | Status and configuration of FELIX E-link TTC-BUSY settings, as well as other BUSY-related settings. |
| `fexoff`      | Enable/disable the XOFF feature of FULL mode links.                                                 |
| `fexofftx`    | Generate an XOFF or XON on FULL mode links.                                                         |
| `feto`        | Status and configuration of FELIX timeouts (global, TTC and link data, a.k.a _instant timeout_).    |
| `febrc`       | Configure E-links FromHost broadcast settings.                                                      |
| `fflash`      | Load a firmware image from a FLX-712 card's flash memory into the card's FPGA.                      |
| `fflashprog`  | Program or verify firmware images in FLX-712 onboard flash memory.                                  |

| ❌        | General Debugging Tools                                                                                          |     |
| -------- | ---------------------------------------------------------------------------------------------------------------- | --- |
| `fcheck` | Run configurable sanity checks on data from a file from *fdaq* or dump selected data chunks or blocks to screen. |
| `fedump` | Dump data blocks directly from a FELIX device (or selected E-link) to screen.                                    |

| ❌             | GBTX and lpGBT Configuration Tools                                                               |     |
| ------------- | ------------------------------------------------------------------------------------------------ | --- |
| `fice`        | Read or write GBTX or lpGBT chip registers via the (lp)GBT-link IC channel.                      |
| `flpgbtconf`  | Read or write lpGBT chip registers or register bit fields by name or address via the IC channel. |
| `fgbtxconf`   | Read or write GBTX chip registers or register bit fields by name or address via the IC channel.  |
| `fscai2cgbtx` | Read or write GBTX registers via a GBT-SCA I2C channel, a-la *fice*.                             |

| ❌            | GBT-SCA Tools                                                                   |     |
| ------------ | ------------------------------------------------------------------------------- | --- |
| `fec`        | Demo control and communication with a GBT-SCA chip (GPIO, ADC, DAC and/or I2C). |
| `fscaid`     | Read and display a GBT-SCA chip ID.                                             |
| `fscaio`     | Read and write GBT-SCA GPIO lines.                                              |
| `fscaadc`    | Read out GBT-SCA ADC input channels.                                            |
| `fscadac`    | Read and write GBT-SCA DAC output channels.                                     |
| `fscai2c`    | Read and write to I2C devices connected to GBT-SCA I2C channels.                |
| `fscads24`   | Read out a 1-Wire 64-bit ID chip connected to a GBT-SCA GPIO pin (demo).        |
| `fscajtag`   | Program a bit-file into a Xilinx FPGA connected to a GBT-SCA JTAG port.         |
| `fxvcserver` | Interface Vivado to a GBT-SCA chip's JTAG port and connected Xilinx FPGA(s).    |
| `fscareply`  | Parse and display a GBT-SCA reply given as a sequence of raw bytes.             |

| ❌            | Tools for lpGBT Control and Monitoring Channels                         |     |
| ------------ | ----------------------------------------------------------------------- | --- |
| `flpgbtio`   | Read and write lpGBT GPIO lines via the lpGBT IC channel.               |
| `flpgbti2c`  | Execute I2C operations on an lpGBT I2C Master via the lpGBT IC channel. |
| `flpgbtds24` | Read out a 1-Wire 64-bit ID chip connected to an lpGBT GPIO pin (demo). |

## Felix-STAR readout applications

As described in the FELIX manual Section 7.3 on [Felix Star executables](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-doc/felix-user-manual/5.x/7_felix_star.html#sec:felixstarcommands)


| Implemented | Tool               |
| ----------- | ------------------ |
| ❌           | `felix-tohost`     |
| ❌           | `felix-toflx`      |
| ❌           | `felix-register`   |
| ❌           | `felix-to-atlas`   |
| ❌           | `felix-fifo2elink` |
| ❌           | `felix-elink2file` |
| ❌           | `felix-file2host`  |
| ❌           | `felix-fid`        |
