import logging
import random
from subprocess import CompletedProcess
from typing import Protocol

log = logging.getLogger(__name__)


class FelixReceiver(Protocol):
    def probe(self): ...
    def reset(self): ...
    def init(self): ...
    def configure(self): ...
    def start(self): ...
    def stop(self): ...
    def info(self): ...


class DummyFelixReceiver:
    probability: float = 0.9
    name: str = "--dummy--"

    @property
    def result(self):
        return CompletedProcess(
            args=[],
            returncode=int(random.random() > self.probability),
        )

    def probe(self):
        log.info(self.name)
        return self.result

    def reset(self):
        log.info(self.name)
        return self.result

    def init(self):
        log.info(self.name)
        return self.result

    def configure(self):
        log.info(self.name)
        return self.result

    def start(self):
        log.info(self.name)
        return self.result

    def stop(self):
        log.info(self.name)
        return self.result

    def info(self):
        log.info(self.name)
        return self.result

    def __rich_repr__(self):
        yield self.__class__.__name__
        yield self.name
        yield self.probability

    __rich_repr__.angular = True
