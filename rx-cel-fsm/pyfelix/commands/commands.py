from dataclasses import dataclass, field
from enum import Enum

# from subprocess import CompletedProcess
from typing import Protocol

from commands.receiver import FelixReceiver


class FELIX_APPLICATION(Enum):
    to_host = 0
    to_flx = 1
    register = 2


class Command(Protocol):
    receiver: FelixReceiver

    # result: CompletedProcess = field(default_factory=CompletedProcess)
    def __call__(self) -> None: ...


@dataclass
class BaseCommand:
    """Base class"""

    receiver: FelixReceiver

    def __call__(self) -> None:
        raise NotImplementedError


@dataclass
class Probe(BaseCommand):
    def __call__(self) -> None:
        self.result = self.receiver.probe()


@dataclass
class Reset(BaseCommand):
    def __call__(self) -> None:
        self.result = self.receiver.reset()


@dataclass
class Info(BaseCommand):
    def __call__(self) -> None:
        self.result = self.receiver.info()

@dataclass
class Init(BaseCommand):
    def __call__(self) -> None:
        self.result = self.receiver.init()

@dataclass
class Configure(BaseCommand):
    device: int = 0

    def __call__(self) -> None:
        self.result = self.receiver.configure()  # self.device)


@dataclass
class StartStop:
    # device: int = 0
    # start: bool = True
    # app: FELIX_APPLICATION = FELIX_APPLICATION.register

    def __call__(self) -> None:
        self.result = self.backend.start()  # self.device)


# def define_status(self, event):
#   #Called as finalize_event callback after each transition to set the status of the current state
#   felix_output = self.felix_response["head"]
#   #define status, not implemented yet
#   event.state.set_status("ok")
