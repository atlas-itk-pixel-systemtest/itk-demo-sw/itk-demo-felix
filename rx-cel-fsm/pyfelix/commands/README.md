
# Command Pattern

The command pattern is used to decouple the FSM transition and state callbacks
from the backend implementation.
```mermaid
sequenceDiagram
    Command-->>Protocol: implements
    Client->>Command: uses
    Client->>Invoker: creates
    Command->>Receiver: calls
```

## Receiver

The Receiver is a wrapper around atomic FELIX operations implementing the implicit Receiver protocol. The following concrete implementations are planned:
* dummy receiver, excecutes dummy commands with configurable failure etc. for testing.
* emulator receiver, returns pre-recorded FELIX response text.
* FELIX receiver, executes actual FELIX commands (or manages them via `s6-init`). This receiver can also store general FELIX configuration options in the Receiver instance. 

## Commands

* Commands encapsulate calls on the Receiver operations, along with the Receiver reference, their paramenters and the result object (usually a CompletedProcess instance). 
* A Command can call multiple atomic receive methods where appropriate.
* Following the standard command pattern Command execution (`__call__(self)`) does not return a value. Instead the result is stored in the Command object and can be inspected after the call.
  
## Invoker

* Python protocols for the Command and Receiver classes allow implementation of the Invoker without dependening on the concrete Command and Receiver implementations. Therefore the Invoker can be reused by other microservices.
* The Invoker keeps a history of the Command instances and can be used to inspect current backend state as well as what happened previously, including failures (see result storage in the Command objects above). 
* (Optional: Undo/Redo features can of course not be used in the case of hardware backend, but could be included for use in database-only microservices such as runkey manager or configdb staging area.)
  
## Client

* The client instantiates Command instances by passing a Receiver instance and parameters, and registers them on the Invoker.
* Then it runs the Invoker and inspects the results.
* In the case of the FSM, each callback is a client (potentially using some shared configuration).

 


