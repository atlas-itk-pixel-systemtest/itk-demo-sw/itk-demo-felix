import logging
import re
from dataclasses import dataclass, field

from rich.table import Table

from commands.commands import Command

log = logging.getLogger(__name__)


class CommandFailed(Exception): ...


@dataclass
class CommandInvoker:
    """Command invoker with history"""

    cmds: list = field(default_factory=list)
    head_idx: int = 0
    exec_idx: int = 0

    def register(self, cmd: Command) -> None:
        del self.cmds[self.head_idx :]
        self.cmds.append(cmd)
        self.head_idx += 1
        log.debug(f"{self!r} {type(cmd)!r}")

    def __call__(self) -> None:
        for cmd in self.cmds[self.exec_idx : self.head_idx]:
            log.debug(f"{self!r} {type(cmd)!r}")
            cmd()
            completed_process = cmd.result
            log.debug(completed_process)
            if completed_process.returncode != 0:
                raise CommandFailed
            self.exec_idx += 1

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__} {self.exec_idx!r}/{self.head_idx!r}>"

    def __rich__(self):
        t = Table(title="Command History", show_header=False)
        for n, cmd in enumerate(self.cmds):
            if not hasattr(cmd, "result"):
                res = "o"
            elif cmd.result.returncode == 0:
                res = "✅"
            else:
                res = "❌"
            t.add_row(
                str(n),
                res,
                f"{cmd!r}",
            )
        return t
