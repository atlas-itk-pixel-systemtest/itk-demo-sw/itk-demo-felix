# from .pyfelix.commands.commands import Configure, Info, Probe, StartStop, Init
# from .pyfelix.commands.commands import FELIX_APPLICATION
# from .pyfelix.commands.invoker import CommandFailed, CommandInvoker
# from .pyfelix.commands.receiver import DummyFelixReceiver
# from .pyfelix._logging import logging


class FelixCard():

  def __init__(self):
    pass
    # log = logging.getLogger(__name__)
    # logging.basicConfig(level=logging.DEBUG)
    # ch = CommandInvoker()
    # rcv = DummyFelixReceiver()
    # app = FELIX_APPLICATION()

  def _exec_commands(self):
    # try:
    #     ch()
    # except CommandFailed as exc:
    #     log.exception(exc)
    #     return False
    return True


  def flx_info(self, infotype):
    # info = Info(rcv)
    # #TODO specify what kind of info is required 
    # ch.register(info)
    result = _exec_commands()


  def init_card(self,event):
    # ch.register(Init(rcv))
    result = self._exec_commands()
    return result

  def card_found(self):
    #check if a felix card is present
    # ch.register(Probe(rcv))
    result = self._exec_commands()
    return result

  def configure_device(self,device_name):
    #Configure device dev0/dev1, return false if transition failed
    # configure = Configure(rcv)
    # if device_name == "dev0":
    #   configure.device = 0
    # else:
    #   configure.device = 1
    # ch.register(configure)
    result = self._exec_commands()
    return result
    

  def start_felixstar_tohost(self,device_name):
    #Start felixstar tohost on device dev0/dev1, return false if transition failed
    # start_tohost = StartStop(rcv)
    # start_tohost.start = True
    # start_tohost.app = app.to_host
    # if device_name == "dev0":
    #   start_tohost.device = 0
    # else:
    #   start_tohost.device = 1
    # ch.register(start_tohost)
    result = self._exec_commands()
    return result

  def start_felixstar_toflx(self,device_name):
    #Start felixstar toflx on device dev0/dev1, return false if transition failed
    # start_toflx = StartStop(rcv)
    # start_toflx.start = True
    # start_toflx.app = app.to_flx
    # if device_name == "dev0":
    #   start_tohost.device = 0
    # else:
    #   start_tohost.device = 1
    # ch.register(start_toflx)
    result = self._exec_commands()
    return result

  def stop_felixstar_tohost(self,device_name):
    #Stop felixstar tohost on device dev0/dev1, return false if transition failed
    # stop_tohost = StartStop(rcv)
    # stop_tohost.start = False
    # stop_tohost.app = app.to_host
    # if device_name == "dev0":
    #   stop_tohost.device = 0
    # else:
    #   stop_tohost.device = 1
    # ch.register(stop_tohost)
    result = self._exec_commands()
    return result

  def stop_felixstar_toflx(self,device_name):
    #Stop felixstar toflx on device dev0/dev1, return false if transition failed
    # stop_toflx = StartStop(rcv)
    # stop_toflx.start = False
    # stop_toflx.app = app.to_flx
    # if device_name == "dev0":
    #   stop_toflx.device = 0
    # else:
    #   stop_toflx.device = 1
    # ch.register(stop_toflx)
    result = self._exec_commands()
    return result

  def start_register(self):
    #start the felix-register service for both devices
    # start_register = StartStop(rcv)
    # start_register.start = True
    # start_register.app = app.register
    # ch.register(start_register)
    result = self._exec_commands()
    return result

  def stop_register(self):
    #stop the felix-register service for both devices
    # stop_register = StartStop(rcv)
    # stop_register.start = True
    # stop_register.app = app.register   
    # ch.register(stop_register) 
    result = self._exec_commands()
    return result

  def reset_card(self):
    #Reset card back to initial state 
    # ch.Register(Reset(rcv))
    result = self._exec_commands()
    return result

  def define_status(self, event):
    #Will be implemented as asynchronous thread to check the status continuously
    for state in event.state:
      state.set_status("ok")

