# from transitions.extensions import HierarchicalMachine
from transitions.extensions.diagrams import HierarchicalGraphMachine as HGM
from transitions.extensions.nesting import NestedState
from transitions.extensions.states import add_state_features
from transitions.core import listify
from ruamel.yaml import YAML
from machine_setup import *
NestedState.separator = '.'
#add transfers based on conditions? => transfer only if condition works out, what to do otherwise?
#if not, return error to GUI in finalize_event (save it as model attribute?)

yaml = YAML()

class StateStatus(NestedState):
  def __init__(self, *args, **kwargs):
    self.state_status_dict = kwargs.pop("state_status", {})
    self.state_status = "unkown"
    super(StateStatus, self).__init__(*args, **kwargs)

  #bad idea in case we want to use reflexive transitions
  # def enter(self, event_data):
  #   if event_data.transition.source == self.name and self.state_status_dict:
  #     self.state_status = self.state_status_dict["on_reentry"]
  def set_status(self,status):
    self.state_status = status

  def __getattr__(self, item):
    if(item == "get_state_status"):
      return self.state_status
    if(item == "get_status_name"):
      return self.state_status_dict[self.state_status]["name"]
    if(item == "get_status_message"):
      return self.state_status_dict[self.state_status].get("message")
    return super(StateStatus, self).__getattribute__(item)


@add_state_features(StateStatus)
class StateStatusMachine(HGM):
  pass



graph_conf = {
    "title": "Card/Device",
    "use_pygraphviz": False,
    "show_conditions": False,
    "show_auto_transitions": False,
    "show_state_attributes": False,
}

machine_conf = {
    "auto_transitions": True,
    'ignore_invalid_triggers':True,
}


def create_machine(*argv, **kwargs):
    device_names = ["dev0", "dev1"]
    felixstars = ["A"]
    default_state_status = {
                            "unkown": {
                                  "name":"UNKOWN",
                                  "message": "Status is updated after each transition, this should not happen"
                                },
                            "ok": {"name":"OK"},
                            "warning": {
                                        "name":"WARNING",
                                        "message": "add what to do in case state XY gets this status"
                                      },
                            "error": {
                                        "name":"ERROR",
                                        "message": "add what to do in case state XY gets this status"
                                      },
                            }
    m = StateStatusMachine(
        *argv,
        **kwargs,
        **card_conf(device_names,felixstars,default_state_status),
        **graph_conf,
        **machine_conf,
    )

    return m

def draw_graph(m):
    g = m.get_graph(
        # show_roi=True,  # region of interest = (previous state, active state and all reachable states)
    )
    g.draw("./public/graph.png", prog="dot")


