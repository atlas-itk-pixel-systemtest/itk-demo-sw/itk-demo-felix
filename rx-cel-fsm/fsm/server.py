#!/usr/bin/env python

import json
import yaml
import logging

import logging_tree
from rich import inspect
from rich.logging import RichHandler

from transitions import MachineError

from microdot.asgi import Microdot, send_file
# from microdot.cors import CORS

from model import FelixCard
from machine import create_machine, draw_graph


log = logging.getLogger("rich")
log.info("Booting FSM server ...")

model = FelixCard()

m = create_machine(model=model)
print(m.get_triggers("startFSM"))
# m.probe()

draw_graph(m)

app = Microdot()

# cors = CORS(
#     app,
#     allowed_origins=["*"],
#     allow_credentials=True,
# )

if log.isEnabledFor(logging.DEBUG):
    logging_tree.printout()

def build_response(response_keys):
  return_dict = {}
  for key in response_keys:
    return_dict[key] = {}
    print("model state:")
    print(m.model.state)
    print("status name:")
    print(m.get_state(m.model.state[0]).get_status_name)
    print("status message:")
    print(m.get_state(m.model.state[0]).get_status_message)
    return_dict[key]["state"] = m.model.state
    return_dict[key]["status"] = m.get_state(m.model.state[0]).get_status_name
    return_dict[key]["status_message"] = m.get_state(m.model.state[0]).get_status_message
  return return_dict

# Microdot Routes


@app.get("/")
async def index(request):
    return send_file("public/index.html", max_age=0)


@app.get("/public/<path:path>")
async def static(request, path):
    if ".." in path:
        # directory traversal is not allowed
        return "Not found", 404
    return send_file("public/" + path, max_age=0)


@app.get("/health")
async def health(request):
    return "Ok", 200

@app.get("/state")
async def state(request):
    return m.model.state


@app.get("/state/<machinename>")
async def states(request, machinename):
    return m.model[machinename].states


@app.get("/markup")
async def markup(request):
    response = m.markup
    # print(response)
    return response


@app.get("/markup/graph")
async def graph(request):
    return str(m.get_graph())


@app.get("/markup/json")
async def markup_json(request):
    return json.dumps(m.markup, indent=4)


@app.get("/markup/yaml")
async def markup_yaml(request):
    return yaml.dump(m.markup)

@app.post("/initialize")
#TODO device 0 needs to be initialised before device 1
async def init_felix(request):
  model.initialize() 
  response = build_response(["response_initialize"])
  return response

@app.post("/configure")
async def configure_felix(request):
  response_keys = []
  for device in request.form["devices"]:
    model.trigger("configure_"+device, device) 
    response_keys += "response_configure"+device
  response = build_response(response_keys)
  return response

@app.post("/start_felixstar_tohost")
async def start_felixstar_tohost(request):
  payload = request.json
  for device, felixstar in payload["instances"].items():
    model.trigger("start_tohost_"+device+"_"+felixstar,device,felixstar) 
  #TODO return output
  return "felixstar started"

@app.post("/stop_felixstar_tohost")
async def stop_felixstar_tohost(request):
  payload = request.json
  for device, felixstar in payload["instances"].items():
     model.trigger("stop_tohost_"+device+"_"+felixstar,device,felixstar) 
  #TODO return output
  return "felixstar stopped"

@app.post("/start_felixstar_toflx")
async def start_felixstar_toflx(request):
  payload = request.json
  for device, felixstar in payload["instances"].items():
     model.trigger("start_toflx_"+device+"_"+felixstar,device,felixstar) 
  #TODO return output
  return "felixstar started"

@app.post("/stop_felixstar_toflx")
async def stop_felixstar_toflx(request):
  payload = request.json
  for device, felixstar in payload["instances"].items():
     model.trigger("stop_toflx_"+device+"_"+felixstar,device,felixstar) 
  #TODO return output
  return "felixstar stopped"

@app.post("/trigger/<triggername>")
async def trigger(request, triggername):
    global m
    global model
    try:
        res = m.get_triggers(model.state)
    except MachineError as mexc:
        log.exception(f"trigger failed {mexc}")
    draw_graph(m)
    # resend index with updated graph
    return res
    # send_file("public/index.html")


@app.post("/shutdown")
async def shutdown(request):
    msg = "The server is shutting down..."
    print(msg)
    request.app.shutdown()
    return msg


# @app.errorhandler(500)
# async def internal_error(request):
#     print(dir(request))
#     return{'error':'internal error'}, 500

if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format="%(message)s",
        datefmt="[%X]",
        handlers=[RichHandler()],
    )
    # inspect(m, methods=True)
    # for method, pattern, function in app.url_map:
    #     print(method, pattern.url_pattern, function.__name__)

    app.run(port=8002, debug=True)
