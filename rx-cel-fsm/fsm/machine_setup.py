
def get_felixstar_conf(felixstar,default_state_status,device_name):
  identifier = device_name+"_"+felixstar
  return {
      "name": felixstar,
      "states": [ 
                  {"name": "configured", "state_status": default_state_status},
                  {"name": "register", "state_status": default_state_status},
                  {"name": "tohost", "state_status": default_state_status},
                  {"name": "tohost-register", "state_status": default_state_status},
                  {"name": "toflx", "state_status": default_state_status},
                  {"name": "toflx-register", "state_status": default_state_status},
                  {"name": "tohost-toflx", "state_status": default_state_status},
                  {"name": "running", "state_status": default_state_status},
                ],
      "transitions": [        
        # Transitions for starting the tohost/toflx connection
        {"trigger": "start_tohost_"+identifier, "source": "configured", "dest": "tohost", "conditions": "start_felixstar_tohost"},
        {"trigger": "start_tohost_"+identifier, "source": "toflx", "dest": "tohost-toflx", "conditions": "start_felixstar_tohost"},
        {"trigger": "start_tohost_"+identifier, "source": "register", "dest": "tohost-register", "conditions": "start_felixstar_tohost"},
        {"trigger": "start_tohost_"+identifier, "source": "toflx-register", "dest": "running", "conditions": "start_felixstar_tohost"},
        {"trigger": "start_toflx_"+identifier, "source": "configured", "dest": "toflx", "conditions": "start_felixstar_toflx"},
        {"trigger": "start_toflx_"+identifier, "source": "tohost", "dest": "tohost-toflx", "conditions": "start_felixstar_toflx"},
        {"trigger": "start_toflx_"+identifier, "source": "register", "dest": "toflx-register", "conditions": "start_felixstar_toflx"},
        {"trigger": "start_toflx_"+identifier, "source": "tohost-register", "dest": "running", "conditions": "start_felixstar_toflx"},
        #transitions to stop the tohost/toflx connection
        {"trigger": "stop_tohost_"+identifier, "source": "tohost", "dest": "configured", "conditions": "stop_felixstar_tohost"},
        {"trigger": "stop_tohost_"+identifier, "source": "tohost-toflx", "dest": "toflx", "conditions": "stop_felixstar_tohost"},
        {"trigger": "stop_toflx_"+identifier, "source": "toflx", "dest": "configured", "conditions": "stop_felixstar_toflx"},
        {"trigger": "stop_toflx_"+identifier, "source": "tohost-toflx", "dest": "tohost", "conditions": "stop_felixstar_toflx"},
        #transitions to start the register
        {"trigger": "start_register", "source": "configured", "dest": "register", "conditions": "start_register"},
        {"trigger": "start_register", "source": "tohost", "dest": "tohost-register", "conditions": "start_register"},
        {"trigger": "start_register", "source": "toflx", "dest": "toflx-register", "conditions": "start_register"},
        {"trigger": "start_register", "source": "tohost-toflx", "dest": "running", "conditions": "start_register"},
        #transitions to stop the register
        {"trigger": "stop_register", "source": "register", "dest": "configured", "conditions": "stop_register"},
        {"trigger": "stop_register", "source": "tohost-register", "dest": "tohost", "conditions": "stop_register"},
        {"trigger": "stop_register", "source": "toflx-register", "dest": "toflx", "conditions": "stop_register"},
        {"trigger": "stop_register", "source": "running", "dest": "tohost-toflx", "conditions": "stop_register"},
      ],
      "initial": "configured",
  }

def get_device_conf(name,felixstars,default_state_status):
    return {
        "name": name,
        "states": [
                    {"name": "initialized", "state_status": default_state_status},
                    {
                        "name": "felixstar",
                        "parallel": [
                          *[get_felixstar_conf(star,default_state_status,name) for star in felixstars]
                        ],
                    }
                  ],
        "transitions": [
          {"trigger":"configure_"+name, "source": "initialized", "dest": "felixstar", "conditions": "configure_device"},
        ],
        "initial": "initialized",
    }

def card_conf(names,felixstars,default_state_status):
    return {
        "name": "card",
        "states": [
                    {"name": "initial", "state_status": default_state_status},
                    {
                        "name": "devices",
                        "parallel": [
                          *[get_device_conf(name, felixstars,default_state_status) for name in names]
                        ],
                    },
                    {"name": "undetected", "state_status": default_state_status}
                  ],
        "transitions": [
            {"trigger": "probe", "source": "undetected", "dest": "initial", "conditions": "card_found"},
            {"trigger": "reset", "source": "devices", "dest": "initial", "conditions": "reset_card"},
            {"trigger": "initialize", "source": "initial", "dest": "devices", "conditions": "init_card"},
            #internal transitions that do not change the state
            #used for fuctions that return information about the setup
            {"trigger": "info", "source": ["initial", "devices"], "dest": None, "after": "flx_info"},
            {"trigger": "get_elink_config", "source": "devices", "dest": None},

        ],
        # "initial": "undetected", #check on startup if a felix card is there, otherwise stay in unknown
        "initial": "initial", 
        "send_event": True,
        "queued": True,
        #--- machine callbacks ------------------------
        # 'on_exception': ["return_error"],
        'finalize_event': ["define_status"],
    }
