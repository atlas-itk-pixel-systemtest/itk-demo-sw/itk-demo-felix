import reflex as rx


class ReflexserverConfig(rx.Config):
    pass


config = ReflexserverConfig(
    app_name="felix_ui",
    # api_url="http://localhost:8000",
    # env=rx.Env.PROD,
    cors_allowed_origins=["*"],
    # telemetry_enabled=False,
    # db_url="sqlite:///reflex.db",
)
