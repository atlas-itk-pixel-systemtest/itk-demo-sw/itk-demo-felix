# FELIX Software UI/API based on Reflex

## Users

### Start frontend and backend Docker images

```shell
../bootstrap            # check prerequisites met
mkdir configurations    # local configurations storage
docker compose up
```

## Developers

### Build Docker images

```shell
docker compose build
```

### Local Installation

```shell
poetry install
```
