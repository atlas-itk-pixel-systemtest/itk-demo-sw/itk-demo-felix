from typing import Callable

import reflex as rx

from .components.navbar import navbar


def template(page: Callable[[], rx.Component]) -> rx.Component:
    return rx.flex(
        rx.heading("FELIX UI - itk-demo-felix", size="7", align="center"),
        rx.divider(),  # border_color="black"),
        navbar(),
        rx.divider(),  # border_color="black"),
        rx.flex(
            # menu(),
            rx.container(page()),
            direction="row",
            align="center"
        ),
        width="100%",
        direction="column",
        align="center"
    )
