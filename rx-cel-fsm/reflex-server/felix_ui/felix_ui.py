"""
It is the file where the content and layout of
the page at http://localhost:3000/ are determined.

Ali Can Canbay - acanbay@cern.ch    |   9 Aug 2023

Last edit: 14 Aug 2023 by acanbay
"""

import logging

import reflex as rx

# import felix_ui.pages.advanced_pod as apodfnc
import felix_ui.base as bs
# import felix_ui.pages.configuration_functions as cfgfunc
# import felix_ui.pages.index_functions as idxfunc
# import felix_ui.pages.monitoring_functions as mntfunc
import felix_ui.state as stfunc
# from felix_ui.pages import manual

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


bs.reset_config_params()

State = stfunc.State

app = rx.App()


# def index():
#     # http://localhost:3000
#     return idxfunc.main(State)


# def configuration_page():
#     # http://localhost:3000/configuration
#     return cfgfunc.main(State)


# def monitoring_page():
#     # http://localhost:3000/monitoring
#     return mntfunc.main(State)


# def advanced_POD():
#     # http://localhost:3000/opower
#     return apodfnc.main(State)


# def add_gui(app):
#     # GUI
#     app.add_page(index, title="FELIX")
#     # app.add_page(manual.main(State), "/manual", title="Manual Operation")
#     app.add_page(monitoring_page, "/monitoring", title="Monitoring")
#     app.add_page(configuration_page, "/configuration", title="Configurations")
#     app.add_page(advanced_POD, "/opower", title="POD")


# def add_api(app):
#     import felix_ui.pages.API_modules as amod

#     # API
#     app.api.add_api_route("/health", amod.health)
#     app.api.add_api_route("/info", amod.info)
#     app.api.add_api_route("/infoconfig", amod.info_configurations)
#     app.api.add_api_route("/getconfigs", amod.get_config_files)
#     app.api.add_api_route("/getconfig/{fname}", amod.inspect_config_file)
#     app.api.add_api_route("/setdefault/{fname}", amod.change_default_config)
#     app.api.add_api_route("/deleteconfig/{fname}", amod.delete_config_file)

#     app.api.add_api_route("/infofelix", amod.info_FELIX)
#     app.api.add_api_route("/initfelix", amod.init_FELIX)
#     app.api.add_api_route("/configurefelix", amod.configure_FELIX)
#     app.api.add_api_route("/startfelix", amod.start_FELIX)
#     app.api.add_api_route("/stopfelix", amod.stop_FELIX)

#     # app.api.add_api_route("/flxconfigstore", amod.flx_config_store)
#     app.api.add_api_route("/flxconfigget/{dev}/{reg}", amod.flx_config_get)
#     app.api.add_api_route("/flxconfigset/{dev}/{reg}/{value}", amod.flx_config_set)
#     app.api.add_api_route("/gbtreset/{dev}/{value}", amod.gbt_reset)

#     app.api.add_api_route("/readoptoboard/{settings}/{reg}", amod.opto_reg_read)
#     app.api.add_api_route(
#         "/writeoptoboard/{settings}/{reg}/{value}", amod.opto_reg_write
#     )

#     app.api.add_api_route("/getelink", amod.get_ELINK)
#     app.api.add_api_route("/getlink", amod.get_OLINK)
#     app.api.add_api_route("/getpod", amod.get_OPower)

#     app.api.add_api_route(
#         "/dashboardSideBarContent/{identifier}", amod.dashboardSideBarContent
#     )


# add_gui(app)
# # add_api(app)
