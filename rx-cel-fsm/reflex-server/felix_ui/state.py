"""
It is the file where the content and layout of
the page at http://localhost:3000/ are determined.

Ali Can Canbay - acanbay@cern.ch    |   9 Aug 2023

Last edit: 27 Aug 2023 by acanbay
"""

import asyncio
import filecmp
import json
import os
from os import listdir
from os.path import isfile, join
from typing import Dict, List

import reflex as rx

import felix_ui.base as bs

configurations_path = "configurations/"


class State(rx.State):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.FELIX_state_name = "Unknown"
        self.felix_state_status = "unknown"
        self.output = ""
        self.refresh()

    FELIX_state_name: str
    felix_state_status: str
    output: str
    configuration_Files: dict
    selected_config: str
    is_uploading: bool

    def set_felix_state(self, stateName):
      #TODO some preliminary string manipulation for nicely labeled states in the gui, needs improvement
      prettyName = ""
      if len(stateName)>1:
        for name in stateName:
          name = name.split(".",1)[1]
          name = name.replace("dev", "Device ")
          prettyName += name + ", "
      self.FELIX_state_name = prettyName

    target_status = "None"
    target_config: str = "New_Configuration.json"

    target_config_Device_IDs: List[int] = [0, 1]
    target_config_Device_IDs_bool: List[bool] = [False, False]
    target_config_Device_IDs_tag: List[str] = ["", ""]

    target_config_Links: List[List[int]] = [
        list(range(12)) for _ in range(len(target_config_Device_IDs))
    ]

    target_config_Links_bool: List[List[bool]] = [
        [False] * 12 for _ in range(len(target_config_Device_IDs))
    ]

    target_config_Links_comment: List[List[str]] = [
        [""] * 12 for _ in range(len(target_config_Device_IDs))
    ]

    target_config_ICEC: List[List[bool]] = [[False] * 12, [False] * 12]
    target_config_Polarity: List[List[bool]] = [[False] * 12, [False] * 12]

    target_config_possible_TXs: List[List[List[int]]] = [
        [
            [
                num
                for num in range(link * 64, (link + 1) * 64)
                if num // 64 == link and num % 2 == 0
            ][:8]
            for link in links
        ]
        for links in target_config_Links
    ]

    target_config_possible_TXs_bool: List[List[List[bool]]] = [
        [[False] * 8 for link in links] for links in target_config_Links
    ]

    target_config_possible_RXs: List[List[List[int]]] = [
        [
            [
                num
                for num in range(link * 64, (link + 1) * 64)
                if num // 64 == link and num % 4 == 0
            ][:6]
            for link in links
        ]
        for links in target_config_Links
    ]

    target_config_possible_RXs_bool: List[List[List[bool]]] = [
        [[False] * 6 for link in links] for links in target_config_Links
    ]


    def update_config_params(self):
        data = bs.get_config_params()
        if not data:
            return
        if not (default_idx := data.get("Default")):
            return

        config_files = {}
        for file in data["Configs"].values():
            if default_idx != -1:
                if file == data["Configs"][default_idx]:
                    file += " (default)"
                    self.selected_config = file
            config_files[file.name] = file
        self.configuration_Files = config_files

    @rx.var
    def FELIX_button_text(self) -> str:
        if self.FELIX_state_name == "FelixRunning":
            FELIX_button_text = "Stop FELIX"
        elif self.FELIX_state_name == "FelixTransitioning":
            FELIX_button_text = "Wait"
        else:
            FELIX_button_text = "Start FELIX"
        return FELIX_button_text

    @rx.var
    def FELIX_state_status(self) -> str:
        return self.felix_state_status

    @rx.var
    def FELIX_current_status(self) -> str:
        FELIX_current_status = self.FELIX_state_name
        return FELIX_current_status

    @rx.var
    def Output(self) -> str:
        Output = self.output
        return Output

    # http://localhost:3000/
    def refresh(self):
        self.update_config_params()

    # http://localhost:3000/ and http://localhost:3000/configurations
    def set_default(self):
        bs.change_default_config(self.selected_config)
        self.update_config_params()

    # http://localhost:3000/ and http://localhost:3000/configurations
    def delete_selected_config(self):
        if "default" in self.selected_config:
            fname = self.selected_config[:-10]
        else:
            fname = self.selected_config

        bs.delete_config(fname)
        self.output = f"{fname} is deleted"
        self.update_config_params()

    # http://localhost:3000/ and http://localhost:3000/configurations
    def confirm_delete(self):
        self.delete_selected_config()

    # http://localhost:3000/
    async def init_FELIX(self):
        output = await bs.init_FELIX(self.set_felix_state)
        #TODO replace with output from felix command
        self.output = output["status_message"]
        self.felix_state_status = output["status"]

    # http://localhost:3000/
    async def configure_FELIX(self):
        output = await bs.configure_FELIX(self.set_felix_state)
        self.output = output

    # http://localhost:3000/configurations
    def add_Files(self):
        Files = [
            f
            for f in listdir(".web/public/")
            if isfile(join(".web/public/", f)) and "." != f[0]
        ]
        for File in Files:
            if File == "favicon.ico":
                continue
            os.system(f"mv .web/public/{File} configurations")
        self.update_config_params()

    # http://localhost:3000/configurations
    async def handle_upload(self, files: List[rx.UploadFile]):
        self.is_uploading = True

        for file in files:
            upload_data = await file.read()
            outfile = rx.get_asset_path(file.filename)

            with open(outfile, "wb") as file_object:
                file_object.write(upload_data)

        return State.stop_upload

    # http://localhost:3000/configurations
    async def stop_upload(self):
        await asyncio.sleep(1)
        self.is_uploading = False
        self.add_Files()

    # http://localhost:3000/configurations
    def reset_target_config(self, target_config="New_Configuration.json"):
        self.target_config = target_config

        for dev in range(len(self.target_config_Device_IDs_bool)):
            self.target_config_Device_IDs_bool[dev] = False
            self.target_config_Device_IDs_tag[dev] = ""

            for link in range(len(self.target_config_Links_bool[dev])):
                self.target_config_Links_bool[dev][link] = False
                self.target_config_ICEC[dev][link] = False
                self.target_config_Polarity[dev][link] = False
                self.target_config_Links_comment[dev][link] = ""

                for num in range(len(self.target_config_possible_TXs_bool[dev][link])):
                    self.target_config_possible_TXs_bool[dev][link][num] = False

                for num in range(len(self.target_config_possible_RXs_bool[dev][link])):
                    self.target_config_possible_RXs_bool[dev][link][num] = False

    # http://localhost:3000/configurations
    def get_target_config_param(self):
        file = configurations_path + self.target_config
        with open(file) as json_str:
            data = json.load(json_str)

        for dev in range(len(self.target_config_Device_IDs)):
            if str(self.target_config_Device_IDs[dev]) in data["Devices"].keys():
                self.target_config_Device_IDs_bool[dev] = True

                device_id = str(self.target_config_Device_IDs[dev])
                self.target_config_Device_IDs_tag[dev] = data["Devices"][device_id][
                    "Tag"
                ]
            else:
                continue

            for link in range(len(self.target_config_Links[dev])):
                if (
                    str(self.target_config_Links[dev][link])
                    in data["Devices"][device_id]["Links"].keys()
                ):
                    self.target_config_Links_bool[dev][link] = True
                else:
                    continue

                link_id = str(self.target_config_Links[dev][link])

                if data["Devices"][device_id]["Links"][link_id]["ICEC"] == 1:
                    self.target_config_ICEC[dev][link] = True

                if data["Devices"][device_id]["Links"][link_id]["Polarity"] == 1:
                    self.target_config_Polarity[dev][link] = True

                self.target_config_Links_comment[dev][link] = data["Devices"][
                    device_id
                ]["Links"][link_id]["Comment"]

                for num in range(len(self.target_config_possible_TXs[dev][link])):
                    if (
                        self.target_config_possible_TXs[dev][link][num]
                        in data["Devices"][device_id]["Links"][link_id]["TX"]
                    ):
                        self.target_config_possible_TXs_bool[dev][link][num] = True
                    else:
                        continue

                for num in range(len(self.target_config_possible_RXs[dev][link])):
                    if (
                        self.target_config_possible_RXs[dev][link][num]
                        in data["Devices"][device_id]["Links"][link_id]["RX"]
                    ):
                        self.target_config_possible_RXs_bool[dev][link][num] = True
                    else:
                        continue

    # http://localhost:3000/configurations
    def change_view_status(self, Status):
        if Status == "Create":
            self.target_status = "Create"
            self.reset_target_config()

        elif Status in ["Inspect", "Edit"]:
            self.target_status = Status

            if " (default)" in self.selected_config:
                self.target_config = self.selected_config[:-10]
            else:
                self.target_config = self.selected_config
            self.reset_target_config(self.target_config)

            self.get_target_config_param()

        elif Status == "None":
            self.target_status = Status

    # http://localhost:3000/configurations
    def edit_target_config_param(self, param, dev, link=None, num=None, text: str = ""):
        if param == "Dev":
            self.target_config_Device_IDs_bool[
                dev
            ] = not self.target_config_Device_IDs_bool[dev]

        elif param == "Tag":
            self.target_config_Device_IDs_tag[dev] = text

        elif param == "Link":
            self.target_config_Links_bool[dev][
                link
            ] = not self.target_config_Links_bool[dev][link]

        elif param == "ICEC":
            self.target_config_ICEC[dev][link] = not self.target_config_ICEC[dev][link]

        elif param == "Polarity":
            self.target_config_Polarity[dev][link] = not self.target_config_Polarity[
                dev
            ][link]

        elif param == "Comment":
            self.target_config_Links_comment[dev][link] = text

        elif param == "TX":
            self.target_config_possible_TXs_bool[dev][link][
                num
            ] = not self.target_config_possible_TXs_bool[dev][link][num]

        elif param == "RX":
            self.target_config_possible_RXs_bool[dev][link][
                num
            ] = not self.target_config_possible_RXs_bool[dev][link][num]

    # http://localhost:3000/configurations
    def change_File_Name(self, text: str):
        self.target_config = text

    # http://localhost:3000/configurations
    def change_device_tag(self, form_data: dict):
        dev_str = list(form_data.keys())[0]
        tag = form_data[dev_str]
        self.edit_target_config_param("Tag", int(dev_str), text=tag)

    # http://localhost:3000/configurations
    def change_link_comment(self, form_data: dict):
        key = list(form_data.keys())[0]
        comment = form_data[key]
        data = key.split("_")
        dev = data[0]
        link = data[1]

        self.edit_target_config_param("Comment", int(dev), int(link), text=comment)

    # http://localhost:3000/configurations
    def write_config(self, form_data: dict):
        if form_data["fname"] != "":
            self.target_config = form_data["fname"]

        data = {"Devices": {}}

        for dev in range(len(self.target_config_Device_IDs)):
            if self.target_config_Device_IDs_bool[dev] == False:
                continue
            device_id = str(dev)
            data["Devices"] = {device_id: {}}
            data["Devices"][device_id]["Tag"] = self.target_config_Device_IDs_tag[dev]
            data["Devices"][device_id]["Links"] = {}

            for link in range(len(self.target_config_Links[dev])):
                if self.target_config_Links_bool[dev][link] == False:
                    continue

                link_id = str(link)

                data["Devices"][device_id]["Links"][link_id] = {}

                if self.target_config_ICEC[dev][link] == True:
                    data["Devices"][device_id]["Links"][link_id]["ICEC"] = 1
                else:
                    data["Devices"][device_id]["Links"][link_id]["ICEC"] = 0

                if self.target_config_Polarity[dev][link] == True:
                    data["Devices"][device_id]["Links"][link_id]["Polarity"] = 1
                else:
                    data["Devices"][device_id]["Links"][link_id]["Polarity"] = 0

                data["Devices"][device_id]["Links"][link_id]["Comment"] = (
                    self.target_config_Links_comment[dev][link]
                )

                data["Devices"][device_id]["Links"][link_id]["TX"] = []
                data["Devices"][device_id]["Links"][link_id]["RX"] = []

                for num in range(len(self.target_config_possible_TXs[dev][link])):
                    if self.target_config_possible_TXs_bool[dev][link][num] == False:
                        continue
                    data["Devices"][device_id]["Links"][link_id]["TX"].append(
                        self.target_config_possible_TXs[dev][link][num]
                    )

                for num in range(len(self.target_config_possible_RXs[dev][link])):
                    if self.target_config_possible_RXs_bool[dev][link][num] == False:
                        continue
                    data["Devices"][device_id]["Links"][link_id]["RX"].append(
                        self.target_config_possible_RXs[dev][link][num]
                    )

        is_default = False
        if os.path.exists(configurations_path + self.target_config):
            if filecmp.cmp(
                configurations_path + self.target_config,
                configurations_path + "flx.cfg",
            ):
                is_default = True

        with open(configurations_path + self.target_config, "w") as outfile:
            json.dump(data, outfile, indent=2)

        if is_default:
            os.system(
                f"cp {configurations_path}{self.target_config} {configurations_path}/flx.cfg"
            )

        self.update_config_params()
        self.target_status = "None"


