from pyregistry.exceptions import SRResponseError, SRConnectionError
from pyregistry.registry import ServiceRegistry
import config_checker as cc
from pydantic import validator
from typing import Union
import os

FILE_DIR = "/config"  # os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
STACK = " "  # os.environ['STACK']
USE_REGISTRY = False  # os.environ['USE_REGISTRY']


class ConfigSupport(cc.BaseConfig):
    runkey: str = "my_felix_runkey"
    search_dict: str = '{"serial":123}'
    configdb_key: str = "demi/default/itk-demo-configdb/api/url"
    fsm_url_key: str = "demi/" + STACK + "itk-demo-felix/fsm/url"
    felixbase_url_key: str = "demi/" + STACK + "itk-demo-felix/felixbase/url"
    sr_url: str = "http://localhost:5111/api"

    CONFIG_SOURCES = cc.EnvSource(allow_all=True)


class FelixSettings(cc.BaseConfig):
    felix_app: int = 0
    felix_card_number: int = 0
    felix_device0_number: int = 0
    felix_device1_number: int = 1
    felix_device0_serial: str = None
    felix_device1_serial: str = None
    felix_initialize: int = 0
    felix_configure: int = 0
    dryrun: int = 0
    noflx: int = 0
    felix_config_file: str = "felix_config.json"
    felix_data_interface: str = "eth0"
    serial: Union[int, str] = 0
    sr_url: str = None

    @validator("noflx", "felix_initialize", "felix_configure", "dryrun", "noflx")
    def must_be_zero_or_one(cls, v):
        if v not in (0, 1):
            raise ValueError("must be 0 or 1")
        return v


class Links(cc.BaseConfig):
    polarity: int
    icec: int
    rx: list[int]
    tx: list[int]


class Devices(cc.BaseConfig):
    Links: dict[str, Links]


class FelixConfig(cc.BaseConfig):
    Card: int
    Devices: dict[str, Devices]


def _get_url_from_registry(key, url, service_name):
    try:
        url = sr.get(key)
    except SRConnectionError as e:
        log.error(f"Service registry not found\nError: {e}")
        raise e
    except SRResponseError as e:
        log.error(f"{service_name} url not found\nError: {e}")
        raise e


felix_settings = FelixSettings(config_sources=[cc.EnvSource(allow_all=True)])

config = ConfigSupport()
fsm_url = "http://localhost:8002/"
felixbase_url = "http://host.docker.internal:8001/"

if USE_REGISTRY:
    sr = ServiceRegistry(config.sr_url)
    _get_url_from_registry(fsm_url, config.fsm_url_key, "FSM")
    _get_url_from_registry(felixbase_url, config.felixbase_url - key, "Felixbase")
