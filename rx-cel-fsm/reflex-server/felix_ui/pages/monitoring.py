"""
It is the file where the content and layout of
the Monitoring panel are determined.

Ali Can Canbay - acanbay@cern.ch    |   9 Aug 2023
Maren Stratmann - maren.stratmann@cern.ch

"""
from typing import Dict, List

import reflex as rx

from felix_ui.components.navbar import navbar
from felix_ui.template import template
import felix_ui.state as stfunc


class MonitoringState(rx.State):

  Optical_power_bool: Dict[str, List[bool]] = {"TX": [False] * 24, "RX": [False] * 24}

  Optical_link_bool: List[bool] = [False] * 24
  Electrical_link_bool: Dict[int, List[bool]] = {
      0: [False] * 24,
      4: [False] * 24,
      8: [False] * 24,
      12: [False] * 24,
      16: [False] * 24,
      20: [False] * 24,
      28: [False] * 24,
      29: [False] * 24,
  }

  def read_pod(self):
      data = bs.get_opower()

      for link in range(24):
          self.Optical_power_bool["TX"][link] = False
          if float(data["TX"][link]) > 50.0:
              self.Optical_power_bool["TX"][link] = True

          self.Optical_power_bool["RX"][link] = False
          if float(data["RX"][link]) > 50.0:
              self.Optical_power_bool["RX"][link] = True

  def read_link(self):
    data = bs.get_olink_configurations()
    for link in range(24):
        self.Optical_link_bool[link] = float(data[link])

  def read_elink(self):
      data = bs.get_elink_configurations()
      for link in range(24):
          self.Electrical_link_bool[0][link] = False
          if 0 in data[link]:
              self.Electrical_link_bool[0][link] = True
          self.Electrical_link_bool[4][link] = False
          if 4 in data[link]:
              self.Electrical_link_bool[4][link] = True
          self.Electrical_link_bool[8][link] = False
          if 8 in data[link]:
              self.Electrical_link_bool[8][link] = True
          self.Electrical_link_bool[12][link] = False
          if 12 in data[link]:
              self.Electrical_link_bool[12][link] = True
          self.Electrical_link_bool[16][link] = False
          if 16 in data[link]:
              self.Electrical_link_bool[16][link] = True
          self.Electrical_link_bool[20][link] = False
          if 20 in data[link]:
              self.Electrical_link_bool[20][link] = True
          self.Electrical_link_bool[28][link] = False
          if 28 in data[link]:
              self.Electrical_link_bool[28][link] = True
          self.Electrical_link_bool[29][link] = False
          if 29 in data[link]:
              self.Electrical_link_bool[29][link] = True

# Optical power Part
def pod():
    return rx.flex(
            rx.heading("Optical Power", font_size="1.3em"),
            rx.flex(
                rx.button(
                    "Read",
                    on_click=MonitoringState.read_pod,
                    size="1"
                ),
                rx.link(
                    rx.button(
                        "Advanced",
                        on_click=MonitoringState.read_pod,
                        variant="outline",
                        size="1"
                    ),
                    href="/opower",
                    button=True,
                ),
            ),
            direction="column",
            align="center"
          )

# Optical link alignment part
def link():
    return rx.flex(
        rx.heading("Optical link alignment", font_size="1.3em"),
        rx.button(
            "Read",
            on_click=MonitoringState.read_link,
            size="1"
        ),
        direction="column",
        align="center"
    )


# Electrical link alignment part
def elink_header():
    return rx.flex(
            rx.heading("Electrical link alignment", font_size="1.3em"),
            rx.button(
                "Read",
                on_click=MonitoringState.read_elink,
                size="1"
            ),
          direction="column",
          align="center"
        )

def elinks():
    elinks = [0, 4, 8, 12, 16, 20, 28, 29]
    return [
        rx.heading(
            str(i) + " :",
            font_size="1.15em",
        )
        for i in elinks
    ]


def get_table_rows():
    return [
              [
                rx.heading(str(i), font_size="1.2em"),
                rx.flex(
                    rx.cond(
                      MonitoringState.Optical_power_bool["TX"][i],
                      rx.icon("check", size=20, color="green"),
                      rx.icon("x", size=20, color="grey"),
                    ),
                    direction="column",
                    align="center"
                ),
                rx.flex(
                    rx.cond(
                      MonitoringState.Optical_power_bool["RX"][i],
                      rx.icon("check", size=20, color="green"),
                      rx.icon("x", size=20, color="grey"),
                    ),
                    direction="column",
                    align="center"
                ),
                rx.flex(
                    rx.cond(
                      MonitoringState.Optical_link_bool[i],
                      rx.icon("check", size=20, color="green"),
                      rx.icon("x", size=20, color="grey"),
                    ),
                    direction="column",
                    align="center"  
                ),
                rx.cond(
                  MonitoringState.Electrical_link_bool["0"][i],
                  rx.icon("check", size=20, color="green"),
                  rx.icon("x", size=20, color="grey"),
                ),
                rx.cond(
                  MonitoringState.Electrical_link_bool["4"][i],
                  rx.icon("check", size=20, color="green"),
                  rx.icon("x", size=20, color="grey"),
                ),
                rx.cond(
                  MonitoringState.Electrical_link_bool["8"][i],
                  rx.icon("check", size=20, color="green"),
                  rx.icon("x", size=20, color="grey"),
                ),
                rx.cond(
                  MonitoringState.Electrical_link_bool["12"][i],
                  rx.icon("check", size=20, color="green"),
                  rx.icon("x", size=20, color="grey"),
                ),
                rx.cond(
                  MonitoringState.Electrical_link_bool["16"][i],
                  rx.icon("check", size=20, color="green"),
                  rx.icon("x", size=20, color="grey"),
                ),
                rx.cond(
                  MonitoringState.Electrical_link_bool["20"][i],
                  rx.icon("check", size=20, color="green"),
                  rx.icon("x", size=20, color="grey"),
                ),
                rx.cond(
                  MonitoringState.Electrical_link_bool["28"][i],
                  rx.icon("check", size=20, color="green"),
                  rx.icon("x", size=20, color="grey"),
                ),
                rx.cond(
                  MonitoringState.Electrical_link_bool["29"][i],
                  rx.icon("check", size=20, color="green"),
                  rx.icon("x", size=20, color="grey"),
                ),
              ]
              for i in range(0, 24)
            ]

def table():
    table_rows = get_table_rows()
    elink_subheaders = elinks()
    return rx.table.root(
            rx.table.header(
              rx.table.row(
                rx.table.column_header_cell(rx.heading("Link", font_size="1.3em")),
                rx.table.column_header_cell(pod(), col_span=2),
                rx.table.column_header_cell(link()),
                rx.table.column_header_cell(elink_header(), col_span=len(elink_subheaders)),
                align="center",
              ),
            ),
            rx.table.body(
                rx.table.row(
                  rx.table.cell(" "),
                  rx.table.cell(rx.heading("TX",font_size="1.15em")),
                  rx.table.cell(rx.heading("RX",font_size="1.15em")),
                  rx.table.cell(" "),
                  *[rx.table.cell(subhead) for subhead in elink_subheaders],
                  align="center",
                ),
                *[rx.table.row(*[rx.table.cell(col) for col in row]) for row in table_rows],
                align="center",
            ),
            size="1",
            width="100%"
    )


# Main module combining parts
@rx.page(route="/monitoring", title="Monitoring")
@template
def monitoring():
    return rx.flex(
        table(),
        direction="column",
        align="center"
    )
