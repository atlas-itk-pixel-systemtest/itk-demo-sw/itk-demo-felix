import json
import re

from fastapi import HTTPException, Request, Response
from rcf_response import Error

import felix_ui.base as bs
from felix_ui.felix_ui import State


async def health():
    return Response(status_code=200)


async def info():
    dic = bs.get_FELIX_params()
    dic["status"] = 200
    return dic


async def dashboardSideBarContent(request: Request, identifier: str):
    url = request.base_url
    sidebar = {"buttons": [], "content": ""}

    if identifier == "dev0" or identifier == "dev1":
        device = identifier[-1]
        try:
            raw_config = bs.get_config()
            print(raw_config)
            config = json.dumps(json.loads(raw_config)["Devices"][device], indent=2)

            sidebar = {
                "buttons": [],
                "content": f"Config:\n{config}",
            }
        except KeyError:
            pass
    elif identifier == "card":
        sidebar = {
            "buttons": [
                {
                    "label": "Init ",
                    "endpoint": f"{url}initfelix/",
                    "body": None,
                },
                {
                    "label": "Config",
                    "endpoint": f"{url}configurefelix/",
                    "body": None,
                },
                {
                    "label": "Start",
                    "endpoint": f"{url}startfelix/",
                    "body": None,
                },
                {
                    "label": "Stop",
                    "endpoint": f"{url}stopfelix/",
                    "body": None,
                },
            ],
            "content": f"Settings:\n{bs.get_settings()}",
        }
    else:
        error = Error(
            "404", "Identifier not found", f"Identifier {identifier} not found."
        )
        return error.to_conn_response()
    return sidebar


## Configuration Part
async def info_configurations():
    return bs.get_config_params()


async def get_config_files():
    return bs.get_configs()


async def inspect_config_file(fname: str):
    data = bs.get_config_params()

    if fname in data["Configs"].values():
        file = "/config/" + fname
        with open(file) as json_str:
            return json.load(json_str)
    else:
        return {"message": f"{fname} is not in the configuration directory"}


async def change_default_config(fname: str):
    data = bs.get_config_params()
    default_idx = data["Default"]

    if default_idx != -1:
        if fname == data["Configs"][str(default_idx)]:
            return {"message": f"{fname} is already the default"}

    if fname in data["Configs"].values():
        bs.change_default_config(fname)
        return {"message": f"Default config file changed with {fname}"}
    else:
        return {"message": f"{fname} is not in the configuration directory"}


async def delete_config_file(fname: str):
    # data = bs.get_config_params()

    # if fname in data["Configs"].values():
    #     bs.delete_config(fname)
    #     return {"message": f"{fname} is deleted"}
    # else:
    return {"message": f"Configuration file named {fname} not found"}


##########################

## Felix Part
async def info_FELIX():
    return bs.get_FELIX_params()


async def init_FELIX():
    output, success = await bs.init_FELIX(State.set_felix_state)
    if not success:
        raise HTTPException(status_code=500, detail=output)
    return {"message": output}


async def configure_FELIX():
    output, success = await bs.configure_FELIX(State.set_felix_state)
    if not success:
        raise HTTPException(status_code=500, detail=output)
    return {"message": output}


async def start_FELIX():
    output, success = await bs.start_FELIX(State.set_felix_state)
    if not success:
        raise HTTPException(status_code=500, detail=output)
    return {"message": output}


async def stop_FELIX():
    output, success = await bs.stop_FELIX(State.set_felix_state)
    if not success:
        raise HTTPException(status_code=500, detail=output)
    return {"message": output}


# async def flx_config_store():
#    output = bs.flx_config_store()
#    return {"result": output}
#
#
async def flx_config_get(dev: int, reg: str):
    output, success = bs.flx_config_get(dev, reg)
    if not success:
        raise HTTPException(status_code=500, detail=output)
    return {"result": output}


async def flx_config_set(dev: int, reg: str, value: str, enforce: bool = False):
    output, success = bs.flx_config_set(dev, reg, value, enforce)
    if not success:
        raise HTTPException(status_code=500, detail=output)
    return {"result": output}


async def gbt_reset(dev: int, value: str, tx: bool = False):
    output, success = bs.gbt_reset(dev, value, tx)
    if not success:
        raise HTTPException(status_code=500, detail=output)
    return {"result": output}


async def opto_reg_read(settings: str, reg: str):
    output, success = bs.opto_reg_read(settings, reg)
    if not success:
        raise HTTPException(status_code=500, detail=output)
    return {"result": output}


async def opto_reg_write(settings: str, reg: str, value: int):
    output, success = bs.opto_reg_write(settings, reg, value)
    if not success:
        raise HTTPException(status_code=500, detail=output)
    return {"result": output}


async def get_ELINK():
    # data = bs.get_elink_configurations()

    # output = {}
    # lines = data.split('\n')

    # for i in range(len(lines)):
    #     output[i]=lines[i]

    return bs.get_elink_configurations()


async def get_OLINK():
    return bs.get_olink_configurations()


async def get_OPower():
    data = bs.get_opower()

    # output = {}
    # lines = data.split('\n')

    # for i in range(len(lines)):
    #     output[i]=lines[i]

    return bs.get_opower()
