"""
Manual FELIX operation page

Gerhard Brandt <gbrandt@cern.ch>
"""

import re
from dataclasses import Field, dataclass, fields

import reflex as rx
from rich import print
from sqlmodel import delete, select, table
import felix_ui.base as bs


from felix_ui.template import template

#service_name: bool: needs start/stop buttons
S6_SVCS = {
    "flx-info": False,
    "flx-init": False,
    "flx-config": False,
    "flx-reset": False,
    "felix-toflx": True,
    "felix-tohost": True,
    "felix-register": False,
}

#notes: toflx/tohost has to be called for both devices simultaneously (eins nach dem anderen klappt nicht)
#toDo: output fenster größer, 


# felix-tohost and felix-fromhost are handled via s6 
#-> add buttons with "start" and "stop" for these commands

@dataclass
class FelixGeneralOptions:
    """CLI Wrapper for FELIX central data acquisition application"""

    #     General Options:
    bus_dir: str = "./bus"
    """ Write felix-bus information to this directory. [default: ./bus]"""

    bus_groupname: str = "FELIX"  # Use this groupname for the bus. [default: FELIX]
    cid: int = 0  # CID (Connector Id) to set in FID (Felix ID), incompatible with --co. [default: device]
    # co=N                      # CO (Connector Offset) to offset FID (Felix ID), incompatible with --did and --cid.
    did: int = 0  # DID (Detector Id) to set in FID (Felix ID), incompatible with --co. [default: 0]
    device: int = 0  # Use FLX device DEVICE. [default: 0]
    error_out: str = ""  # Write error information to a UNIX FIFO
    free_cmem: bool = False
    """ Free previously booked cmem segment by name-<device>-<dma>"""

    iface: str = ""  # Send data to the interface. [calculated: use --ip value]
    ip: str = "libfabric:127.0.0.1"  # Publish data on the ip address IP. [default: libfabric:127.0.0.1]
    netio_pagesize: int = 65536  # NetIO page size in Byte. [default: 65536]
    netio_pages: int = 256  # Number of NetIO pages. [default: 256]
    stats_out: str = ""  # Write periodic statistics data to a UNIX FIFO
    stats_period: int = (
        1000  # Period in milliseconds for statistics dumps. [default: 1000]
    )
    help: bool = False  # Give this help list
    verbose: bool = False  # Produce verbose output
    version: bool = False  # Print program version
    vid: int = 1  # VID (Version Id) to set in FID (Felix ID), incompatible with --co. [default: 1]
    netio_watermark: int = 57344  # NetIO watermark in Byte. [default: 57344]


@dataclass
class FelixToFelixOptions:
    #     ToFlx Options:
    # netio_buffersize=SIZE    # Obsolete, use --netio-pagesize. NetIO receive buffer size in byte; maximum size for a single message. [default: netio-pagesize]
    # netio_buffers=SIZE       # Obsolete, use --netio-pages. Number of NetIO receive buffers. [default: netio-pages]
    cmem: int = 20  # CMEM buffer size in MB. [default: 20]
    # dma=ID                   # Use DMA descriptor ID. [calculated: highest dma]
    cdma: bool = False  # Use circular DMA buffer instead of one-shot
    # port=PORT                # Send data to port PORT. [calculated: 53200 + 10*device + dma]
    rawtcp: bool = False  # Use raw tcp not libfabric (can be used with --unbuffered)
    stats_stdout: bool = False  # Prints stats to stdout
    unbuffered: bool = True  # Use unbuffered mode


class CommonState(rx.Model, table=True):
    service: str
    status: bool

    # def __init__(self):


def common_state_row(cs: CommonState):
    return rx.table.row(
        rx.table.cell(cs.service),
        rx.table.cell(cs.status),
    )


class State(rx.State):
    options: str = "" #put all optional arguments here
    exec_result: str = "" #return value of the executed command

    common_states: list[CommonState] = []
    # colors: dict[str, str] = {}

    def get_common_states(self):
        with rx.session() as session:
            self.common_states = session.exec(CommonState.select()).all()
            # if len(self.common_states):
            for cs in self.common_states:
                print(cs)

    def get_common_state(self, service: str):
        with rx.session() as session:
            statement = select(CommonState).where(CommonState.service == service)
            cs = session.exec(statement).first()
            return cs

    def update_common_state(self, service: str, status: bool):
        with rx.session() as session:
            cs = self.get_common_state(service=service)
            cs.status = status
            print(cs)
            session.add(cs)
            session.commit()
            session.refresh(cs)

    def reset_states(self):
        with rx.session() as session:
            session.exec(delete(CommonState))
            session.commit()
            for service in list(S6_SVCS.keys()):
                cs = CommonState(service=service, status=False)
                session.add(cs)
            session.commit()

    def operate_manually(self, svc: str):
        #the database should be filled at start time, but I did not manage to do that
        #this is a workaround
        first_command = False
        with rx.session() as session:
          states = select(CommonState).filter(CommonState.service.like('%x-%'))
          results = session.exec(states)
          len_res = sum(1 for result in results)
          if len_res==0:
            first_command = True
        if first_command:
          self.reset_states()
        service = svc
        if "_" in svc:
          service = svc.split("_")[1]
        cs = self.get_common_state(service=service)
        self.update_common_state(service, not cs.status)
        self.exec_result = bs.run_flx_expert_mode(svc, {"options":self.options})


    @rx.var
    def colors(self, service: str = ""):
        print(service)
        # service = self.common_states
        # if not cs.status:
        return "tomato"
        # else:
        # return "green"

    #     # with rx.session() as session:
    #     #     statement = select(CommonState).where(CommonState.service == svc)
    #     #     cs = session.exec(statement).first()
    #     # if not cs:
    #     #     return "gray"

    #     coldic: dict[str, str] = {}
    #     for cs in self.common_states:
    #         if not cs.status:
    #             coldic[cs.service] = "tomato"
    #         else:
    #             coldic[cs.service] = "green"
    #     return coldic


def render_field(f: Field):
    # print(f)
    field_name = f.name
    if f.type is str:
        return rx.input(placeholder=f.name, name=f.name)
    elif f.type is int:
        return rx.input(placeholder=f.name, name=f.name)
    elif f.type is bool:
        return rx.checkbox(f.name)
    else:
        return rx.box(rx.text(field_name))


@rx.page(route="/manual")
@template
def manual():

    # def render_button(svc: str):
    # svc = CommonState.service
    # status = CommonState.status
    # State.colors(btn)

    # return rx.button(
    # svc,
    # on_click=State.operate_manually(svc),
    # color_scheme=State.colors(svc),
    # )

    return rx.flex(
        # dev0
        # rx.button("Reset", on_click=State.reset_states),
        # rx.button("Dump", on_click=State.get_common_states),
        rx.list(
            # rx.foreach(State.common_states, render_button),
            *[
                rx.button(
                    svc,
                    on_click=State.operate_manually(svc),
                    # color_scheme=State.colors(svc),
                )
                for svc in list(S6_SVCS.keys()) if S6_SVCS[svc] == False
            ],
            columns=str(lambda x=0: x+1 for svc in list(S6_SVCS.keys()) if S6_SVCS[svc] == False),
            spacing="4",
            width="100%",
        ),
        # rx.grid(
        #     rx.form(rx.vstack(*[render_field(f) for f in fields(FelixGeneralOptions)])),
        #     rx.form(rx.vstack(*[render_field(f) for f in fields(FelixToFelixOptions)])),
            
        #     columns="2",
        #     spacing="4",
        #     # width="100%",
        # ),
        rx.list(
            *[
              rx.flex(
                rx.button(
                  "Start " + svc,
                  on_click=State.operate_manually("start_"+svc),
                ),
                rx.button(
                    "Stop " + svc,
                    on_click=State.operate_manually("stop_"+svc),
                ),
                columns = "2",
              )
                for svc in list(S6_SVCS.keys()) if S6_SVCS[svc] == True
            ],
            columns=str(lambda x=0: x+1 for svc in list(S6_SVCS.keys()) if S6_SVCS[svc] == True),
            spacing="4",
            width="100%",
        ),
        rx.flex(
          rx.input(
            placeholder="Put additional [OPTIONS] [COMMANDS] here",
            value=State.options,
            on_change=State.set_options,
            size="3",
          ),
          rx.scroll_area(
            rx.flex(
              rx.text_area(
                placeholder="Output will be shown here",
                value=State.exec_result,
                read_only=True,
                size="3",
              ),
              direction="column",
              align="stretch"
            )
          ),
          direction="row",
          spacing="4",
          width="100%",
        ),
        direction="column",
        align="center",
        spacing="3",
        flex_wrap="wrap",
        width="100%",
    )
