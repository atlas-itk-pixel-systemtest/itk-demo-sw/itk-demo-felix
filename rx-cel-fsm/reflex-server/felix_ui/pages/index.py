"""
It is the file where the content and layout of
the page at http://localhost:3000/ are determined.

Ali Can Canbay - acanbay@cern.ch    |   9 Aug 2023

Last edit: 15 Aug 2023 by acanbay
"""

import reflex as rx
import felix_ui.state as stfunc
from felix_ui.template import template


button1 = {
    "width": "100px",
    "height": "40px",
    "justify-content": "center",
    "align-items": "center",
    "overflow": "hidden",
}

button2 = {
    "width": "130px",
    "height": "40px",
    "justify-content": "center",
    "align-items": "center",
    "overflow": "hidden",
}


class ModalSettingsState(rx.State):
    show: bool = False

    def change(self):
        self.show = not (self.show)


class ModalConfigState(rx.State):
    show: bool = False

    def change(self):
        self.show = not (self.show)


# The part associated with the configuration files
def configuration_part(State):
    return rx.vstack(
        rx.hstack(
            rx.box(
                rx.button("Show Settings", on_click=ModalSettingsState.change),
                # rx.modal(
                #     rx.modal_overlay(
                #         rx.modal_content(
                #             rx.modal_header("FELIX Settings"),
                #             rx.modal_body(
                #                 bs.get_settings(), style={"white-space": "pre-line"}
                #             ),
                #             rx.modal_footer(
                #                 rx.button("Close", on_click=ModalSettingsState.change)
                #             ),
                #         )
                #     ),
                #     is_open=ModalSettingsState.show,
                # ),
            ),
            rx.box(
                rx.button("Show Config", on_click=ModalConfigState.change),
                # rx.modal(
                #     rx.modal_overlay(
                #         rx.modal_content(
                #             rx.modal_header("FELIX Config"),
                #             rx.modal_body(
                #                 bs.get_config(), style={"white-space": "pre-line"}
                #             ),
                #             rx.modal_footer(
                #                 rx.button("Close", on_click=ModalConfigState.change)
                #             ),
                #         )
                #     ),
                #     is_open=ModalConfigState.show,
                # ),
            ),
            position="fixed",
            left="20px",
            top="170px",
        )
    )
  

# The part associated with FELIX commands
def felix_part(State):
    return rx.hstack(
        rx.button(
            "Init FELIX",
            on_click=State.init_FELIX,
            style=button1,
        ),
        rx.button(
            "Configure FELIX",
            style=button2,
            on_click=State.configure_FELIX,
        ),
        rx.cond(
            State.FELIX_button_text == "Wait",
            rx.button(
                State.FELIX_button_text,
                bg="gray",
                color="black",
                is_loading=True,
                style=button1,
            ),
            rx.button(
                State.FELIX_button_text,
                # on_click=State.change_Felix_Status,
                style=button1,
            ),
        ),
        rx.text(width="680px"),
        rx.button(
            "Refresh page",
            on_click=State.refresh,
            style=button2,
        ),
        position="fixed",
        left="20px",
        top="280px",
    )


# Output part
def output_part(State):
    return rx.vstack(
        rx.heading(
            "Outputs:",
            font_size="1.2em",
            position="fixed",
            left="20px",
            top="370px",
        ),
        rx.hstack(
            rx.text(State.Output),
            text_align="top",
            border="1px dotted black",
            padding="0.1em",
            shadow="lg",
            border_radius="lg",
            position="fixed",
            left="20px",
            top="390px",
        ),
    )


# Main module combining parts
@rx.page(route="/",title="FELIX")
@template
def index():
    return rx.flex(
        configuration_part(stfunc.State),
        rx.divider(
            position="fixed",
            top="250px",
        ),
        felix_part(stfunc.State),
        rx.divider(
            position="fixed",
            top="350px",
        ),
        output_part(stfunc.State),
        direction="column",
        align="center"
    )
