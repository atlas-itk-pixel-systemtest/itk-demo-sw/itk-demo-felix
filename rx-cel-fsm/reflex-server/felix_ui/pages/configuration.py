"""
It is the file where the content and layout of
the configuration panel are determined.

Ali Can Canbay - acanbay@cern.ch    |   9 Aug 2023
Maren Stratmann - maren.stratmann@cern.ch

"""

import reflex as rx

from felix_ui.components.navbar import navbar
from felix_ui.template import template
import felix_ui.state as stfunc


def new_configuration_part():
    return rx.flex(
        rx.flex(
            rx.flex(
                rx.upload(
                    rx.button(
                        "Chose New Configuration(s)",
                        height="50px",
                        width="250px",
                        color="rgb(107,99,246)",
                        bg="white",
                        border="1px solid rgb(107,99,246)",
                    ),
                    padding="0.5em",
                ),
                rx.button(
                    "Upload and add to DB",
                    on_click=stfunc.State.handle_upload(rx.upload_files()),
                ),
              direction="row",
            ),
            rx.cond(
                stfunc.State.is_uploading,
                rx.progress(is_indeterminate=True, color="blue", width="100%"),
                # rx.progress(value=0, width="100%"),
            ),
          direction="column",
        ),
        rx.text("  |  ",white_space="pre"),
        rx.button(
            "Create new configuration file",
            on_click=stfunc.State.change_view_status("Create"),
        ),
        direction="row",
        # position="fixed",
        # left="20px",
        # top="160px",
    )


def configuration_part():
    return rx.flex(
        rx.heading(
            "Configurations Files",
            font_size="1.2em",
            position="fixed",
            left="20px",
            top="280px",
        ),
        rx.flex(
            rx.flex(
                rx.select.root(
                  rx.select.content(
                    rx.foreach(
                      stfunc.State.configuration_Files.keys(),
                      lambda filename: rx.select.item(filename, value=filename)
                    ),
                  ),
                    height="50px",
                    width="350px",
                    # value=stfunc.State.selected_config,
                    # on_change=stfunc.State.set_selected_config,
                ),
                direction="row"
            ),
            rx.button(
                "Set Default",
                on_click=stfunc.State.set_default(),
            ),
            rx.cond(
                stfunc.State.target_status != "Edit",
                rx.button(
                    "Inspect",
                    on_click=stfunc.State.change_view_status("Inspect"),
                ),
                rx.button(
                    "Edit",
                    on_click=stfunc.State.change_view_status("Edit"),
                ),
            ),
            rx.text(" | ",white_space="pre"),
            rx.box(
              rx.alert_dialog.root(
                rx.alert_dialog.trigger(
                  rx.button("Delete"),
                ),
                rx.alert_dialog.content(
                  rx.alert_dialog.title("Confirm"),
                  rx.alert_dialog.description(
                        "Are you sure you want to delete "
                        + stfunc.State.selected_config
                        + "?"
                  ),
                  rx.flex(
                    rx.alert_dialog.cancel(
                      rx.button("Cancel"),
                    ),
                    rx.alert_dialog.action(
                      rx.button("Delete", on_click=stfunc.State.confirm_delete),
                    ),
                    spacing="3"
                  ),
                ),
              ),
            ),
            position="fixed",
            left="20px",
            top="310px",
        ),
        direction="row",
    )


def single_TXRX(dev, link, num, channel, disabled):
    return rx.vstack(
        rx.cond(
            channel == "TX",
            rx.vstack(
                rx.checkbox(
                    color_scheme="green",
                    size="lg",
                    is_disabled=disabled,
                    is_checked=stfunc.State.target_config_possible_TXs_bool[dev][link][num],
                    on_change=stfunc.State.edit_target_config_param("TX", dev, link, num),
                ),
                rx.box(
                    stfunc.State.target_config_possible_TXs[dev][link][num], font_size="0.8em"
                ),
            ),
            rx.vstack(
                rx.checkbox(
                    color_scheme="green",
                    size="lg",
                    is_disabled=disabled,
                    is_checked=stfunc.State.target_config_possible_RXs_bool[dev][link][num],
                    on_change=stfunc.State.edit_target_config_param("RX", dev, link, num),
                ),
                rx.box(
                    stfunc.State.target_config_possible_RXs[dev][link][num], font_size="0.8em"
                ),
            ),
        )
    )


def multi_TXRX(dev, link, disabled):
    return rx.hstack(
        rx.vstack(
            rx.heading(
                "TX",
                font_size="1em",
            ),
            rx.hstack(
                *[single_TXRX(dev, link, i, "TX", disabled) for i in range(0, 8)]
            ),
        ),
        rx.text(width="20px"),
        rx.vstack(
            rx.heading(
                "RX",
                font_size="1em",
            ),
            rx.hstack(
                *[single_TXRX( dev, link, i, "RX", disabled) for i in range(0, 6)]
            ),
        ),
        rx.text(width="20px"),
        rx.vstack(
            rx.heading(
                "Comment",
                font_size="1em",
            ),
            rx.cond(
                disabled,
                rx.center(
                    rx.cond(
                        stfunc.State.target_config_Links_comment[dev][link] == "",
                        "None",
                        stfunc.State.target_config_Links_comment[dev][link],
                    ),
                    width="300px",
                    height="30px",
                    padding_x=1,
                ),
                rx.form(
                    rx.hstack(
                        rx.input(
                            placeholder=stfunc.State.target_config_Links_comment[dev][link],
                            id=f"{dev}_{link}",
                            width="250px",
                            height="30px",
                            border_width="medium",
                            padding_x=1,
                        ),
                        rx.button("Submit", type_="submit", height="30px"),
                    ),
                    on_submit=stfunc.State.change_link_comment,
                ),
            ),
        ),
    )


def link_content(dev, link, disabled):
    return rx.vstack(
        rx.hstack(
            rx.center(
                rx.heading(
                    f"Dev. {dev}",
                    font_size="1em",
                ),
                rx.text(width="20px"),
                rx.vstack(
                    rx.heading(
                        f"Link {stfunc.State.target_config_Links[dev][link]}",
                        font_size="1em",
                    ),
                    rx.cond(
                        not disabled,
                        rx.checkbox(
                            color_scheme="green",
                            size="lg",
                            is_disabled=disabled,
                            is_checked=stfunc.State.target_config_Links_bool[dev][link],
                            on_change=stfunc.State.edit_target_config_param("Link", dev, link),
                        ),
                    ),
                ),
                width="150px",
            ),
            rx.text(width="15px"),
            rx.vstack(
                rx.heading(
                    "IC/EC",
                    font_size="1em",
                ),
                rx.checkbox(
                    color_scheme="green",
                    size="lg",
                    is_disabled=disabled,
                    is_checked=stfunc.State.target_config_ICEC[dev][link],
                    on_change=stfunc.State.edit_target_config_param("ICEC", dev, link),
                ),
            ),
            rx.text(width="20px"),
            rx.vstack(
                rx.heading(
                    "Inv. Pol.",
                    font_size="1em",
                ),
                rx.checkbox(
                    color_scheme="green",
                    size="lg",
                    is_disabled=disabled,
                    is_checked=stfunc.State.target_config_Polarity[dev][link],
                    on_change=stfunc.State.edit_target_config_param("Polarity", dev, link),
                ),
            ),
            rx.text(width="20px"),
            multi_TXRX(dev, link, disabled),
        ),
        rx.divider(border_color="black"),
    )


# Device content
def Device(dev, disabled):
    return rx.vstack(
        rx.text(height="5px"),
        rx.hstack(
            rx.heading("Device:", font_size="1em"),
            rx.text(str(dev), font_size="1em"),
            rx.cond(
                stfunc.State.target_status != "Inspect",
                rx.checkbox(
                    color_scheme="green",
                    size="lg",
                    is_disabled=disabled,
                    is_checked=stfunc.State.target_config_Device_IDs_bool[dev],
                    on_change=stfunc.State.edit_target_config_param("Dev", dev),
                ),
            ),
            rx.text(width="20px"),
            rx.heading("Tag: ", font_size="1em"),
            rx.cond(
                disabled,
                rx.text(
                    "'" + stfunc.State.target_config_Device_IDs_tag[dev] + "'", font_size="1em"
                ),
                rx.form(
                    rx.hstack(
                        rx.input(
                            placeholder=stfunc.State.target_config_Device_IDs_tag[dev],
                            id=f"{dev}",
                            width="300px",
                            height="30px",
                            border_width="medium",
                        ),
                        rx.button("Submit", type_="submit", height="30px"),
                    ),
                    on_submit=stfunc.State.change_device_tag,
                ),
            ),
        ),
        rx.divider(border_color="black"),
        rx.cond(
            stfunc.State.target_status == "Inspect",
            rx.vstack(
                *[
                    rx.cond(
                        stfunc.State.target_config_Links_bool[dev][i],
                        link_content(dev, i, disabled),
                    )
                    for i in range(0, 12)
                ]
            ),
            rx.cond(
                stfunc.State.target_config_Device_IDs_bool[dev],
                rx.vstack(
                    *[link_content(dev, i, disabled) for i in range(0, 12)]
                ),
            ),
        ),
        rx.divider(border_color="black"),
    )


# The part where the content of the existing config file can be inspect, changed
# or a new config file can be created
def info_panel():
    return rx.vstack(
        rx.divider(
            border_color="black",
            position="fixed",
            top="388px",
            width="10000px",
            border_width="medium",
        ),
        rx.vstack(
            rx.hstack(
                rx.cond(
                    stfunc.State.target_status == "Inspect",
                    rx.hstack(
                        stfunc.State.target_status == "Inspect",
                        rx.heading(stfunc.State.target_config, font_size="1em"),
                        rx.text(width="50px"),
                        rx.button(
                            "Edit",
                            height="30px",
                            on_click=stfunc.State.change_view_status("Edit"),
                        ),
                    ),
                    rx.hstack(
                        rx.form(
                            rx.hstack(
                                rx.input(
                                    placeholder=stfunc.State.target_config,
                                    id="fname",
                                    width="250px",
                                    height="30px",
                                    border_width="medium",
                                    padding_x=1,
                                ),
                                rx.button("Save", type_="submit", height="30px"),
                            ),
                            on_submit=stfunc.State.write_config,
                        ),
                    ),
                ),
                rx.text(width="50px"),
                rx.button(
                    "Close",
                    height="30px",
                    on_click=stfunc.State.change_view_status("None"),
                    position="fixed",
                    left="1140px",
                ),
                position="fixed",
                left="20px",
                top="415px",
            ),
            rx.box(
                rx.cond(
                    stfunc.State.target_status == "Inspect",
                    rx.vstack(
                        rx.cond(
                            stfunc.State.target_config_Device_IDs_bool[0],
                            Device( 0, True),
                        ),
                        rx.cond(
                            stfunc.State.target_config_Device_IDs_bool[1],
                            Device(1, True),
                        ),
                    ),
                    rx.vstack(
                        Device(0, False),
                        Device(1, False),
                    ),
                ),
                style={
                    "overflow": "scroll",
                    "max-height": "500px",
                    "border": "1px solid black",
                },
                position="fixed",
                left="20px",
                top="440px",
                width="1200px",
            ),
        ),
        position="fixed",
        left="20px",
        top="380px",
    )


# Main module combining parts
@rx.page("/configuration", title="Configurations")
@template
def configuration():
    return rx.flex(
        new_configuration_part(),
        rx.divider(
            position="fixed",
            top="260px",
        ),
        configuration_part(),
        rx.divider(
            position="fixed",
            top="380px",
        ),
        # rx.cond(
        #     State.target_status != "None",
        #     info_panel(State),
        # ),
        direction="column",
        align="center"
    )


# # Inspect mode
# def inspect(State):
#     return rx.vstack(
#         rx.divider(
#             border_color="black",
#             position="fixed",
#             top="388px",
#             width="10000px",
#             border_width="medium",
#         ),

#         rx.vstack(
#             rx.hstack(
#                 rx.heading(State.target_config, font_size="1em"),
#                 rx.text(width="50px"),
#                 rx.button(
#                     "Edit",
#                     height = "30px",
#                     on_click = State.change_view_status("Edit"),
#                 ),
#                 rx.button(
#                     "Close",
#                     height = "30px",
#                     on_click = State.change_view_status("None"),
#                     position="fixed",
#                     left="1140px",
#                 ),
#                 position="fixed",
#                 left="20px",
#                 top="415px"
#             ),

#             rx.box(
#                 rx.vstack(
#                     rx.cond(
#                         State.target_config_Device_IDs_bool[0],
#                         Device_with_cond(State,0,True)
#                     ),
#                     rx.cond(
#                         State.target_config_Device_IDs_bool[1],
#                         Device_with_cond(State,1,True)
#                     ),
#                 ),

#                 style={ "overflow": "scroll",
#                         "max-height": "500px",
#                         "border": "1px solid black"},
#                 position="fixed",
#                 left="20px",
#                 top="440px",
#                 width="1200px",
#             ),

#         ),
#         position="fixed",
#         left="20px",
#         top="380px"
#     )

# Edit and Cretate modes content
# def Device(State,dev,disabled):
#     return rx.vstack(
#         rx.text(height="5px"),
#         rx.hstack(
#             rx.heading("Device:", font_size="1em"),
#             rx.text(str(dev), font_size="1em"),

#             rx.text(width="50px"),

#             rx.heading(f"Tag: ", font_size="1em"),
#             rx.text("'"+State.target_config_Device_IDs_tag[dev]+"'", font_size="1em"),
#         ),
#         rx.divider(border_color="black"),

#         link_content(State,dev,1,disabled),
#         link_content(State,dev,0,disabled),
#         link_content(State,dev,2,disabled),
#         link_content(State,dev,3,disabled),
#         link_content(State,dev,4,disabled),
#         link_content(State,dev,5,disabled),
#         link_content(State,dev,6,disabled),
#         link_content(State,dev,7,disabled),
#         link_content(State,dev,8,disabled),
#         link_content(State,dev,9,disabled),
#         link_content(State,dev,10,disabled),
#         link_content(State,dev,11,disabled),

#         rx.divider(border_color="black"),
#     )

# def edit(State):
#     return rx.vstack(
#         rx.divider(
#             border_color="black",
#             position="fixed",
#             top="388px",
#             width="10000px",
#             border_width="medium",
#         ),

#         rx.vstack(
#             rx.hstack(
#                 rx.heading(State.target_config, font_size="1em"),
#                 rx.text(width="50px"),
#                 rx.button(
#                             "Edit",
#                             height = "30px",
#                             on_click = State.change_view_status("Edit"),
#                           ),
#                 rx.button(
#                             "Close",
#                             height = "30px",
#                             on_click = State.change_view_status("None"),
#                             position="fixed",
#                             left="1140px",
#                           ),
#                 position="fixed",
#                 left="20px",
#                 top="415px"
#             ),

#             rx.box(
#                 rx.vstack(
#                     rx.cond(
#                         State.target_config_Device_IDs_bool[0],
#                         Device(State,0,False)
#                     ),
#                     rx.cond(
#                         State.target_config_Device_IDs_bool[1],
#                         Device(State,1,False)
#                     ),
#                 ),

#                 style={ "overflow": "scroll",
#                         "max-height": "500px",
#                         "border": "1px solid black"},
#                 position="fixed",
#                 left="20px",
#                 top="440px",
#                 width="1200px",
#             ),

#         ),
#         position="fixed",
#         left="20px",
#         top="380px"
#     )


# def edit_mode(State):
#     return rx.box(
#         rx.vstack(
#             rx.divider(border_color="black"),
#             link_content(State,0,False),
#             rx.divider(border_color="black"),
#             link_content(State,1,False),
#             rx.divider(border_color="black"),
#             link_content(State,2,False),
#             rx.divider(border_color="black"),
#             link_content(State,3,False),
#             rx.divider(border_color="black"),
#             link_content(State,4,False),
#             rx.divider(border_color="black"),
#             link_content(State,5,False),
#             rx.divider(border_color="black"),
#             link_content(State,6,False),
#             rx.divider(border_color="black"),
#             link_content(State,7,False),
#             rx.divider(border_color="black"),
#             link_content(State,8,False),
#             rx.divider(border_color="black"),
#             link_content(State,9,False),
#             rx.divider(border_color="black"),
#             link_content(State,10,False),
#             rx.divider(border_color="black"),
#             link_content(State,11,False),
#             rx.divider(border_color="black"),
#         ),

#         style={   "overflow": "scroll",
#                 "max-height": "500px",
#                 "border": "1px solid black"},
#         position="fixed",
#         left="20px",
#         top="440px",
#         width="1200px",
#     )

# def edit(State):
#     return rx.vstack(
#         rx.divider(
#             border_color="black",
#             position="fixed",
#             top="388px",
#             width="10000px",
#             border_width="medium",
#         ),

#         rx.vstack(
#             rx.hstack(

#                 rx.hstack(
#                     rx.heading( "File Name:",
#                                 font_size="1em",

#                                 ),

#                     rx.input(
#                         placeholder=State.config_file,
#                         on_change=State.change_File_Name,
#                         width="300px",
#                         height="30px",
#                         border_width="medium",
#                     ),

#                     position="fixed",
#                     left="20px",
#                 ),

#                 rx.hstack(
#                     rx.heading( "FELIX Card ID:", font_size="1em"),
#                     rx.input(
#                         placeholder=State.FELIX_ID,
#                         on_change=State.change_FELIX_ID,
#                         width="50px",
#                         height="30px",
#                         border_width="medium",
#                     ),
#                     rx.text(width="10px"),
#                     rx.heading( "Device ID:", font_size="1em"),
#                     rx.input(
#                         placeholder=State.Device_ID,
#                         on_change=State.change_Device_ID,
#                         width="50px",
#                         height="30px",
#                         border_width="medium",
#                     ),
#                     position="fixed",
#                     left="420px",
#                 ),

#                 rx.button(
#                     "Save",
#                     height = "30px",
#                     on_click = State.write_config,
#                     position="fixed",
#                     left="1050px",
#                 ),
#                 rx.button(
#                     "Cancel",
#                     height = "30px",
#                     on_click = State.disable_config_output,
#                     position="fixed",
#                     left="1130px",
#                 ),
#                 position="fixed",
#                 left="20px",
#                 top="430px",
#             ),

#         edit_mode(State),

#         ),
#         position="fixed",
#         left="20px",
#         top="380px"
#     )

# def create(State):
#     return rx.vstack(
#         rx.divider(
#             border_color="black",
#             position="fixed",
#             top="388px",
#             width="10000px",
#             border_width="medium",
#         ),

#         rx.vstack(
#             rx.hstack(

#                 rx.hstack(
#                     rx.heading( "File Name:",
#                                 font_size="1em",

#                                 ),

#                     rx.input(
#                         placeholder=State.config_file,
#                         on_change=State.change_File_Name,
#                         width="300px",
#                         height="30px",
#                         border_width="medium",
#                     ),

#                     position="fixed",
#                     left="20px",
#                 ),

#                 rx.hstack(
#                     rx.heading( "FELIX Card ID:", font_size="1em"),
#                     rx.input(
#                         placeholder=State.FELIX_ID,
#                         on_change=State.change_FELIX_ID,
#                         width="50px",
#                         height="30px",
#                         border_width="medium",
#                     ),
#                     rx.text(width="10px"),
#                     rx.heading( "Device ID:", font_size="1em"),
#                     rx.input(
#                         placeholder=State.Device_ID,
#                         on_change=State.change_Device_ID,
#                         width="50px",
#                         height="30px",
#                         border_width="medium",
#                     ),
#                     position="fixed",
#                     left="420px",
#                 ),

#                 rx.button(
#                     "Create",
#                     height = "30px",
#                     on_click = State.write_config,
#                     position="fixed",
#                     left="1040px",
#                 ),
#                 rx.button(
#                     "Cancel",
#                     height = "30px",
#                     on_click = State.disable_config_output,
#                     position="fixed",
#                     left="1130px",
#                 ),
#                 position="fixed",
#                 left="20px",
#                 top="430px",
#             ),

#         edit_mode(State),

#         ),
#         position="fixed",
#         left="20px",
#         top="380px"
#     )
