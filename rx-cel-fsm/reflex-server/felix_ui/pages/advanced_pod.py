"""
It is the file where the content and layout of
the page at /opower (accessible from /monitoring via 'Advanced' button)
is determined determined.

Ali Can Canbay - acanbay@cern.ch    |   9 Aug 2023
Maren Stratmann maren.stratmann@cern.ch
"""

import reflex as rx
from typing import Dict, List
from felix_ui.pages.monitoring import MonitoringState
from felix_ui.template import template
import felix_ui.base as bs



class AdvancedPODState(rx.State):
  Optical_power: Dict[str, List[float]] = {"TX": [0] * 24, "RX": [0] * 24}

  def read_pod(self):
      data = bs.get_opower()
      for link in range(24):
          self.Optical_power["TX"][link] = float(data["TX"][link])
          self.Optical_power["RX"][link] = float(data["RX"][link])



def table_row(link):
    return [
        rx.table.cell(rx.heading(f"{link} :", font_size="1.2em")),
        rx.table.cell(
            rx.flex(
                rx.box(
                    AdvancedPODState.Optical_power["TX"][link],
                    width="100px",
                    border_width="medium",
                    text_align="center",
                ),
                rx.text(" μW",white_space="pre"),
            )
        ),
        rx.table.cell(
            rx.flex(
                rx.box(
                    AdvancedPODState.Optical_power["RX"][link],
                    width="100px",
                    border_width="medium",
                    text_align="center",
                ),
                rx.text(" μW",white_space="pre"),
            )
        ),
    ]


# Construct the table
def datas():
    nlinks = [i for i in range(0, 24)]
    return rx.table.root(
        rx.table.header( 
          rx.table.row(
                    rx.table.column_header_cell(rx.heading("Link", font_size="1.2em")),
                    rx.table.column_header_cell(rx.heading("TX", font_size="1.2em")),
                    rx.table.column_header_cell(rx.heading("RX", font_size="1.2em")),
          ),
        ),
        rx.table.body(
          *[rx.table.row(*table_row(link)) for link in nlinks],
        ),
        size="1",
        width="80%",
    )


# Main module combining parts
@rx.page("/opower", title="POD")
@template
def advanced_pod():
    return rx.flex(
            rx.box(
                rx.vstack(
                    rx.heading(
                        "Optical Power",
                        font_size="1.5em",
                    ),
                    rx.button(
                        "Read",
                        on_click=AdvancedPODState.read_pod,
                        size="2"
                    ),
                ),
                padding=10,
            ),
            rx.spacer(),
            datas(),
            rx.spacer(),
            width="80%",
            align="start",
    )
