import reflex as rx
import felix_ui.state as stfunc

def _felix_status_icon(status):
  return rx.cond(
                  status == "ok",
                  rx.flex(
                    rx.text.strong("  Status: ",white_space="pre"),
                    rx.text (status, color="green"),
                  ),
                  rx.cond(
                    status == "warning",
                    rx.flex(
                      rx.text.strong("  Status: ",white_space="pre"),
                      rx.text (status, color="red"),
                    ),
                    rx.flex(
                      rx.text.strong("  Status: ",white_space="pre"),
                      rx.text (status, color="grey"),
                    ),
                  ),
                )


def navbar():
    return rx.box(
        rx.flex(
            rx.link(
                rx.button("Home", height="30px", variant="outline"),
                href="/",
                button=True,
            ),
            rx.link(
               rx.button("Configuration", height="30px", variant="outline"),
               href="/configuration",
               button=True,
            ),
            rx.link(
                rx.button("Manual", height="30px", variant="outline"),
                href="/manual",
                button=True,
            ),
            rx.link(
                rx.button("Monitoring", height="30px", variant="outline"),
                href="/monitoring",
                button=True,
            ),
            rx.spacer(),
            rx.text.strong("State: ",white_space="pre"),
            rx.text(stfunc.State.FELIX_current_status),
            _felix_status_icon(stfunc.State.FELIX_state_status)
        ),
        width="40%"
    )
