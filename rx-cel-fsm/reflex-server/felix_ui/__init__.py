from .pages import (
  index,
  monitoring,
  advanced_pod,
  manual,
  configuration,
)

__all__ = [
  "index",
  "monitoring",
  "advanced_pod",
  "manual",
  "configuration",
]