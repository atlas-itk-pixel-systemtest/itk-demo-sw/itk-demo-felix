"""
It is the file where the content and layout of
the page at http://localhost:3000/ are determined.

Ali Can Canbay - acanbay@cern.ch    |   9 Aug 2023

Last edit: 18 Mar 2024 by vormwald
"""

import json
import logging
import os
import re
from os import listdir
from os.path import isfile, join

import requests

from felix_ui.settings import FILE_DIR, felix_settings, fsm_url, felixbase_url

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


configurations_path = "/config/"
params_path = "params/"
outputs_path = "outputs/"


def get_settings():
    dic = felix_settings.dict()
    my_string = "\n".join(f"{k}: {v}" for k, v in dic.items())
    return my_string


def get_config():
    with open(os.path.join(FILE_DIR, felix_settings.felix_config_file), "r") as file:
        raw_config = file.read()
    return raw_config


def publish_state(state, serials):
    sr_url = felix_settings.sr_url
    if sr_url:
        dic = {}
        for serial in serials:
            dic[f"demi/dashboard/status/{serial}"] = state

        headers = {
            "accept": "application/json",
            "Content-Type": "application/json",
        }
        requests.post(f"{sr_url}/set_many/", headers=headers, json={"kv_dict": dic})


def debug(func):
    """debugging decorator"""

    def wrapper(*args, **kwargs):
        logger.debug(f"{func.__name__} ({args}, {kwargs})")
        result = func(*args, **kwargs)
        logger.debug(f"{func.__name__}: {result}")
        return result

    return wrapper


# @debug
def run_flx(route, params):
    logger.info(f"run_flx: Calling {route}")

    r = requests.post(fsm_url+"/"+route,data=params)
    r_dict = r.json()
    print(r_dict)
    return r_dict["response_" + route]
    # return ["Got request to call " + route, "ok"]


# Bypass FSM for the advanced user panel
# @debug
def run_flx_expert_mode(route, params):
    logger.info(f"run_flx: Calling {route}")

    r = requests.get(felixbase_url + route, params=params)
    response = json.loads(r.json())
    return response["stdout"]

    # return ["Got request to call " + route + " with options " + params["options"]]


## FastAPI part
## http://localhost:8000
def reset_config_params():
    if not os.path.exists(configurations_path):
        return

    config_files = [
        f
        for f in listdir(configurations_path)
        if isfile(join(configurations_path, f))
        if f.endswith(".json")
    ]
    config_files.sort()

    default_file = felix_settings.felix_config_file

    data = {"Configs": {}, "Default": None}
    default_idx = -1
    for i in range(len(config_files)):
        data["Configs"][i] = config_files[i]
        if config_files[i] == default_file:
            default_idx = i
    data["Default"] = default_idx

    with open(params_path + "/config.params", "w") as outfile:
        json.dump(data, outfile, indent=2)

    return data


def get_config_params():
    return reset_config_params()


def get_configs():
    reset_config_params()
    return get_config_params()["Configs"]


def change_default_config(fname):
    felix_settings.felix_config_file = fname
    reset_config_params()


def delete_config(fname):
    # os.system(f"rm {configurations_path}/{fname}")
    reset_config_params()


def get_FELIX_params():
    with open(params_path + "/felix.params") as json_file:
        data = json.load(json_file)
    return data


def change_FELIX_params(Status):
    data = get_FELIX_params()
    data["FELIX Status"] = Status

    with open(params_path + "/felix.params", "w") as outfile:
        json.dump(data, outfile, indent=2)


@debug
async def init_FELIX(set_felix_state):
    """Initialize FELIX card"""
    try:
        result = run_flx("initialize", {})
    except Exception as e:
        logger.exception(e)

    print(result)
    set_felix_state(result["state"])
    return result


async def configure_FELIX(set_felix_state):
    """Configure FELIX card"""
    # TODO: call run_flx with correct params
    pass


async def start_FELIX(set_felix_state):
    """Start FELIX app"""
    # TODO: call run_flx with correct params
    pass


async def stop_FELIX(set_felix_state):
    """Stop FELIX app"""
    # TODO: call run_flx with correct params
    pass


# def flx_config_store():
#    try:
#        run_flx(
#            [
#                "flx-config",
#                "-d",
#                str(felix_settings.felix_device_number),
#                "-E",
#                "store",
#                "/config/flx-config-store.txt",
#            ]
#        )
#    except Exception as e:
#        logger.exception(e)
#
#    regs = {}
#
#    with open("/config/flx-config-store.txt") as f:
#        lines = f.readlines()
#        for li in lines:
#            try:
#                reg, val = re.split("=", li)
#            except Exception:
#                logger.error(f"Can't parse {li}")
#                continue
#
#            logger.debug(f"{reg} = {val}")
#            regs[reg] = int(val, 0)
#
#    return regs


def flx_config_get(dev: int, reg: str):
    pass
    # try:
    #     r, success = run_flx(["flx-config", "-d", str(dev), "get", reg])
    # except Exception as e:
    #     logger.exception(e)

    # return r.strip(), success


def flx_config_set(dev: int, reg: str, value: str, enforce: bool):
    pass
    # try:
    #     reg_assignment = reg + "=" + str(value)
    #     if enforce:
    #         r, success = run_flx(
    #             ["flx-config", "-d", str(dev), "set", "-E", reg_assignment]
    #         )
    #     else:
    #         r, success = run_flx(["flx-config", "-d", str(dev), "set", reg_assignment])
    # except Exception as e:
    #     logger.exception(e)

    # return r.strip(), success


def gbt_reset(dev: int, value: str, tx: bool):
    if tx:
        reg = "GBT_TX_RESET"
    else:
        reg = "GBT_RX_RESET"
    try:
        r1, success1 = flx_config_set(dev=dev, reg=reg, value=value, enforce=True)
        r2, success1 = flx_config_set(dev=dev, reg=reg, value="0x0", enforce=True)
    except Exception as e:
        logger.exception(e)

    r = r1 + r2
    success = success1 and success1

    return r, success


def opto_reg_read(settings: str, reg: str):
    pass
    # try:
    #     settings = settings.split("-")[1:]
    #     command = ["flpgbtconf"]
    #     for x in settings:
    #         if x[0] == "G":
    #             command.append("-G")
    #             command.append(str(x[1:]))
    #         if x[0] == "I":
    #             command.append("-I")
    #             command.append(str(x[1:]))
    #         if x[0] == "d":
    #             command.append("-d")
    #             command.append(str(x[1:]))
    #     if "1" in settings:
    #         command.append("-1")
    #     command.append(reg)
    #     r, success = run_flx(command)
    # except Exception as e:
    #     logger.exception(e)
    # return r.strip(), success


def opto_reg_write(settings: str, reg: str, value: str):
    pass
    # try:
    #     settings = settings.split("-")[1:]
    #     command = ["flpgbtconf"]
    #     for x in settings:
    #         if x[0] == "G":
    #             command.append("-G")
    #             command.append(str(x[1:]))
    #         if x[0] == "I":
    #             command.append("-I")
    #             command.append(str(x[1:]))
    #         if x[0] == "d":
    #             command.append("-d")
    #             command.append(str(x[1:]))
    #     if "1" in settings:
    #         command.append("-1")
    #     command.append(reg)
    #     command.append(str(value))
    #     r, success = run_flx(command)
    # except Exception as e:
    #     logger.exception(e)
    # return r.strip(), success


def extract_text_between_words(text, word1, word2):
    start = text.find(word1)
    if start == -1:
        return None

    start += len(word1)
    end = text.find(word2, start)
    if end == -1:
        return None

    return text[start:end].strip()


def get_elink_configurations():
    lines = []
    allaligned = []
    for dev in [
        felix_settings.felix_device0_number,
        felix_settings.felix_device1_number,
    ]:
        result = run_flx("config", {"options": "-d " + str(dev) + " list"})
        r = result["status_message"]

        lines = r.split("\n")
        alignkey = "LINK_ALIGNED_"
        aligned = [line for line in lines if re.search(alignkey, line)]
        allaligned.extend(aligned[0:12])

    Output = {}
    i = 0
    for line in allaligned:
        text = extract_text_between_words(line, "0x0000000", " Every")

        Output[i] = [elink for elink in range(10 * 4) if ((int(text, 16) >> elink) & 1)]
        i += 1

    return Output


def get_olink_configurations():
    lines = []
    result = run_flx("info", {"options": "link"})
    r = result["status_message"]
    lines = r.split("\n")

    print(lines)

    Output = {}

    line_device_0 = lines[7]
    line_device_0 = line_device_0.split(" ")
    line_device_0 = line_device_0[2:]
    line_device_0 = [l for l in line_device_0 if l != ""]

    line_device_1 = lines[11]
    line_device_1 = line_device_1.split(" ")
    line_device_1 = line_device_1[2:]
    line_device_1 = [l for l in line_device_1 if l != ""]

    for i in range(len(line_device_0)):
        if line_device_0[i] == "YES":
            Output[i] = True
        else:
            Output[i] = False

    for i in range(len(line_device_1)):
        if line_device_1[i] == "YES":
            Output[i + len(line_device_0)] = True
        else:
            Output[i + len(line_device_0)] = False

    return dict(sorted(Output.items()))


def get_opower():
    lines = []
    result = run_flx("info", {"options": "pod"})
    r = result["status_message"]
    lines = r.split("\n")

    Output = {"TX": {}, "RX": {}}

    # 1st TX
    line_tmp = lines[14].split("|")
    for i in range(1, len(line_tmp) - 1):
        Output["TX"][i - 1] = line_tmp[i].strip()

    # 1st RX
    line_tmp = lines[15].split("|")
    for i in range(1, len(line_tmp) - 1):
        Output["RX"][i - 1] = line_tmp[i].strip()

    # 2st TX
    line_tmp = lines[16].split("|")
    for i in range(1, len(line_tmp) - 1):
        Output["TX"][i + 11] = line_tmp[i].strip()

    # 2st TX
    line_tmp = lines[17].split("|")
    for i in range(1, len(line_tmp) - 1):
        Output["RX"][i + 11] = line_tmp[i].strip()

    return Output
