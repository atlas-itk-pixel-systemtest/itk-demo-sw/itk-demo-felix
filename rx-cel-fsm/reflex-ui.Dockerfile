FROM gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/containers/python/root/3.12:latest AS builder

RUN apt-get update && apt-get install unzip

WORKDIR /app

COPY . .
WORKDIR reflex-server
RUN poetry install

# Deploy templates and prepare app
RUN poetry run reflex init

# # Export static copy of frontend to /app/.web/_static
RUN poetry run reflex export --frontend-only --no-zip


# # Copy static files out of /app to save space in backend image
# RUN mv .web/_static /tmp/_static
# RUN rm -rf .web && mkdir .web
# RUN mv /tmp/_static .web/_static

# CMD ["poetry","run","reflex","run"]

FROM nginx

COPY --from=builder /app/reflex-server/.web/_static /usr/share/nginx/html
COPY --from=builder /app/reflex-server/reflex_ui_nginx.conf /etc/nginx/conf.d/default.conf