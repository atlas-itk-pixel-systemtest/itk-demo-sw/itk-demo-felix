 [demiurl]: https://demi.docs.cern.ch
[appurl]: https://almalinux.org

[![demi](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/defaults/-/raw/a2b52ffa2261b381fb09e7d81b718ae2d1b54035/assets/atlas_itk_logo.png)][demiurl]
## Contact information

| Type | Address/Details |
| :---: | --- |
| Docs | [demi.docs.cern.ch][demiurl] |

# DEPRECEATION NOTICE

please use the centralized [demi-baseimage](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/demi-baseimage) instead.

# Detector Microservices - Docker baseimage

## Overview

A custom base image built with [AlmaLinux][appurl] and [S6 overlay](https://github.com/just-containers/s6-overlay).
This Docker image was adapted from linuxserver.io `docker-baseimage-fedora`.
Several services were removed and scripts simplified for DeMi.

Features:

- Based on `almalinx:9` image
- Add s6-overlay
- Add s6 base skeleton to `/`
  - Create `itk` user which will mimic local user passed to container in `$PUID`/`$PGID` (`init-adduser`)
  - Use `FILE__<SECRET>` to load environment variable `$SECRET` from file `SECRET` (`init-envfile`)
  - use of `-e UMASK=022` supported 
  - Not included for now: Docker Mods (and dynamic installation of RPMs using dnf and Python packages using pip)
  - `init-config`: Barrier for oneshot configuration scripts
  - `init-services`: Barrier for longrun services scripts
- Python (AlmaLinux system Python) venv at `/venv`, rw by `itk` user
- `/config` with permissions for `itk` user
- Local directory mounted to `/config/workspace`


## s6-overlay 

Unchanged from original s6-overlay but we only support s6 v3, see below.


## baseimage init hierarchy

- Add oneshot configure scripts `init-*` between `ìnit-config` and `init-config-end`
- Add longrun service scripts `svc-*` after `ìnit-services`

Note on injecting the environment:
  - in your configuration scripts you can use  
    ```!/usr/bin/with-contenv bash```
    to access the envvars passed in from docker
  - in your productions scripts the environment should be
    set up finally and `with-contenv` not used nor required.
- better to build a custom environment using `s6-envdir` here

```
S6 STAGE1

# magic - set s6 verbosity to 5 to see it happen

S6 STAGE2

S6_STAGE2_HOOK
  [/docker-mods] 
SERVICES_DIR="/custom-services.d"

# to be removed:
# s6rc-oneshot-runner
# fix-attrs
# legacy-cont-init

user:
  [init-envfile]  # <--- inject secrets from $FILE__* ---
  [init-adduser]  # <--- chmod itk user uid/gid and chown /config directory 
      init-os-end
        init-config
          [--- put here your configuration scripts ---]
            init-config-end
              init-mods
                [init-mods-package-install]
                  init-mods-end
                    init-services
                      [--- put here your production scripts ---]

S6 STAGE3

# shut everything down---

```

## Notes

### UNSUPPORTED FROM S6-OVERLAY

We only support s6 v3, the s6-rc.d way to init/run services.
Do not use "legacy" and "deprecated" features:

- `fix-attrs` and `/etc/fix-attrs.d`
- `/etc/cont-init.d` or `/custom-cont-init.d`
- `/etc/services.d` and `S6_SERVICES_*` 
- `/etc/cont-finish.d`
- `user2` bundle and "ci-service-check/dependencies.d/legacy-services"

### REMOVED/CHANGED FROM LSIO

The baseimage is *inspired* by `linuxserver.io` Fedora baseimages but removes various features:

- `abc` (911:911) user changed to `itk` (1111:1111)
- removed `docker_mods` and `init-mods-package-install` for now
  - most mods are for Alpine / Ubuntu and use 'abc' user where neccessary
- removed migrations support 
- removed cron and /defaults support
- Python /lsiopy --> /venv

### Roadmap

- may bring back mods in the future but host on gitlab-registry.cern.ch



