FROM gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/containers/python/root/3.12:latest


RUN apt-get update && apt-get install unzip 

WORKDIR /app

COPY . .
WORKDIR reflex-server
RUN poetry install
RUN poetry lock --no-update

# Deploy templates and prepare app
RUN poetry run reflex init
RUN poetry run reflex db init

ENTRYPOINT ["poetry","run","reflex", "run", "--env", "prod", "--backend-only", "--loglevel", "debug" ]