# FELIX Software Base Docker Container

The `base-image/` sub-repo provides scripts to create a Docker container
for the FELIX Software with a few additional features:
  
- s6-init system to manage the FELIX life-cycle
- user mapping to manage host permissions
- shims to integrate with the host shell.

This image sits in between the [FELIX distribution image](https://gitlab.cern.ch/atlas-tdaq-felix/felix-image)
and the microservices API image `itk-demo-felix-api` (in `../api/`) but can also be used standalone.

Running this image standalon can be interesting if you want to use managed,
containerized FELIX software but do not need the microservice API/GUI but rather want to work from a shell directly.

For this reason *multiple instances* are not supported by this repo directly. Only a single container called `felix` is prepared [^1].

[^1]: If you need more than one container (eg. for multiple devices, users or different FELIX versions) either create corresponding docker scripts / compose files yourself, or use our higher level DeMi facilities, such as the `opto-stack` etc.

Find the Maintainers:

- Gerhard Brandt <gbrandt@cern.ch>

## Starting the FELIX Software Docker Container

### Method 1: Using the `felix` convenience script

```shell
felix
```

### Method 2: Use docker compose

Create a minimal `compose.yaml` file like this

```
---
version: "3"
services:
  felix:
    image:  gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/itk-demo-felix-base:latest
    container_name: felix
    restart: unless-stopped
    environment:
      - TZ=Europe/Paris
      - PUID=1000 # change to your user id!!!
      - PGID=1000 # change to your group id!!!
      - FELIX_DATA_INTERFACE=lo
      - FELIX_INITIALIZE=2 # 2= init and configure FELIX at startup
      - FELIX_APP=1 # 1= run felixcore
    tty: true
    devices:
      - /dev/cmem_rcc:/dev/cmem_rcc
      - /dev/flx0:/dev/flx0
      - /dev/flx1:/dev/flx1
    network_mode: host
```

The environment variables can also be provided from a `.env` file.

Then start using

```shell
docker compose up [-d]
```

If you want to override the baked-in default configuration create the local config directory

```shell
mkdir -p .docker/config
```

and copy your `flx.cfg` there.
Then you need to add the bind mount to the compose file:

```
services:
  felix:
    volumes:
      - ${PWD}/.docker/config:/config
```

### Method 3: Use docker CLI

```shell
docker create \
  --name felix \
  -e TZ=Europe/Paris \
  -e PUID="$(id -u)" \
  -e PGID="$(id -g)" \
  -e DATA_INTERFACE=lo \
  -e FELIX_INITIALIZE=2 \
  -e START_FELIX_APP=1 \
  --device=/dev/cmem_rcc:/dev/cmem_rcc \
  --device=/dev/flx0:/dev/flx0 \
  -v "$PWD":/workspace:cached \
  --workdir /workspace \
  --network host \
  -it \
  --restart unless-stopped \  
  gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/itk-demo-felix-base:latest
docker start felix
```

and to clean up after work

```shell
docker stop felix
docker remove felix
```

## Using FELIX Software in the container

### Method 1: From the container shell

Once the container is up and running, enter the container shell doing

```shell
felix shell
```

or

```shell
docker compose exec felix bash --login
```

or

```shell
docker exec felix bash --login
```

depending on how you started the FELIX container.

### Method 2: Use the shims from the native shell

Install shims for all FELIX commands to your PATH

```shell
. ./setup
```

Use FELIX commands as before

```shell
flx-init
flx-info
flx-config
```

etc.

How it works:

- Shims get created in `./bin/shims` based on the `flx-shim` template from list of supported commands stored in `alias.list`.
- `./bin/shims` gets added to the front of the PATH.
- If a shim is called, it checks for a suitable compose file in the current directory, i.e. a compose file
  - That contains a "felix" service.
  - That is fully configured (i.e. everything is hardcoded or via .env file)
  - If ok, the FELIX command is called in the respective compose project.
- If no such file is available, `docker exec` is used to execute the shim.
- If no running "felix" container is available, the shim tries to run the local command. so, if no FELIX containers are running, you get back whatever you had set up before.

If it's unclear what's happening, prepend the command by `VERBOSITY=2` to see what the shims are doing, eg.:

```shell
VERBOSITY=2 flx-info
```

## Environment variables

General Purpose:

| Envvar | Purpose | Default |
|--|--|--|
|PUID | Host user pid | `id -u` |
|PGID | Host user group | `id -g` |
|VERBOSITY | felix-related messages (0=ERROR, 1=WARNING, 2=INFO) |
| S6_VERBOSITY | s6-related messages | 0 |

(for all s6-overlay environment variables see [s6-overlay manual](https://github.com/just-containers/s6-overlay#customizing-s6-overlay-behaviour))

FELIX specific:

| Envvar | Purpose | Default |
|--|--|--|
|FELIX_DATA_INTERFACE | DATA_INTERFACE | lo |
|FELIX_CARD_NUMBER | Set Felix card (0 or 1) | 0 |
| FELIX_DEVICE_NUMBER | Set Felix device on given card (0 or 1) | 0 |
|FELIX_INITIALIZE | At startup run 0=nothing, 1=flx-init, 2=flx-config from .cfg file, 3=flx-config from .json file | 0 |
|FELIX_CONFIG_FILE | Use custom configuration file | flx.cfg |
|FELIX_APP |  Start FELIX Application: 0=none, 1=felixcore, 2=felix-star | 0 |

## Configuration and mounts in the container

A default initial configuration file in `flx-config store` format is baked into the container at
`/config/flx.cfg`.

Personal configuration files to override this file can be mounted in the `/config` folder of the container by placing them in ```${PWD}/.docker/config```.

By default the local working directory `$PWD` is bind-mounted at `/workspace`.
This is also the default working directory (`--workdir`).

## Update

Synchronize with the CERN GitLab image registry

```shell
docker compose pull
```

Recreate container if docker-compose file or image have changed:

```shell
docker compose up -d
```

## Development

### Build

The build is nothing special. Just a few scripts are copied into felix-image

```
docker compose build
```

### Push

To push an update

```
docker compose push
```
