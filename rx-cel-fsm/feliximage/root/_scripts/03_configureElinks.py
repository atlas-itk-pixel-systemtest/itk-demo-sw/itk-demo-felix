from flxcfg import *

# init_felix()

flx0 = Felix(0)
flx1 = Felix(1)

# =============
flx0.enable_icec(link=3)
flx0.enable_icec(link=4)

# Fibre 3

# KEKQ22_Chip4
flx0.enable_tx(196)
flx0.enable_rx(192)

# KEKQ24_Chip4
flx0.enable_tx(192)
flx0.enable_rx(196)

# KEKQ25_Chip4
flx0.enable_tx(204)
flx0.enable_rx(212)

# Liv8_Chip4
flx0.enable_tx(198)
flx0.enable_rx(208)

# KEKQ19_Chip4
flx0.enable_tx(206)
flx0.enable_rx(204)

# Fibre 4

# Paris6_Chip4
flx0.enable_tx(268)
flx0.enable_rx(260)

# Goe5_Chip4
flx0.enable_tx(260)
flx0.enable_rx(256)

# Paris16_Chip4
flx0.enable_tx(256)
flx0.enable_rx(264)

# Paris11_Chip4
flx0.enable_tx(270)
flx0.enable_rx(276)

# Goe7_Chip4
flx0.enable_tx(262)
flx0.enable_rx(268)

# Paris8_Chip4
flx0.enable_tx(258)
flx0.enable_rx(272)

# =============

flx0.configure()
flx1.configure()
