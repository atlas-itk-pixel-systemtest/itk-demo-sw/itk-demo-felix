#!/usr/bin/env python3
import config_checker as cc
import json, os, logging

logger = logging.getLogger(__name__)

FILE_DIR = "/config"  # os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
# Create the directory if it does not exist
os.makedirs(FILE_DIR, exist_ok=True)
os.makedirs("/run/s6/container_environment", exist_ok=True)


class UseConfigDB(cc.BaseConfig):
    use_configdb: bool = False

    CONFIG_SOURCES = cc.EnvSource(allow_all=True)

class ConfigSupport(cc.BaseConfig):
    runkey: str = "my_felix_runkey"
    search_dict: str = '{"serial":123}'
    configdb_key: str = "demi/default/itk-demo-configdb/api/url"

    CONFIG_SOURCES = cc.EnvSource(allow_all=True)


class FelixSettings(cc.BaseConfig):
    felix_app: int = 0
    felix_card_number: int = 0
    felix_device_number: int = 0
    felix_initialize: int = 0
    dryrun: int = 0
    noflx: int = 0
    felix_config_file: str = "felix_config.json"
    felix_data_interface: str = "lo"
    felix_toflx_ip: cc.IPType = "localhost"
    felix_tohost_ip: cc.IPType = "localhost"
    felix_tohost_dcs_pages: int = 0
    serial: int = 0


class Links(cc.BaseConfig):
    polarity: int
    icec: int
    rx: list[int]
    tx: list[int]


class FelixConfig(cc.BaseConfig):
    FelixID: int
    DeviceID: int
    Links: dict[str, Links]


def main():
    if UseConfigDB().use_configdb:
        #Load configs from configdb and check them against pydantic model
        config_support = ConfigSupport()

        felix_config = FelixConfig(
            config_sources=[
                cc.ConfigDBJsonSource(
                    config_support.configdb_key,
                    config_support.runkey,
                    {
                        "search_dict": json.loads(config_support.search_dict),
                        "object_type": "felix",
                        "config_type": "config",
                    },
                ),
            ]
        )

        felix_settings = FelixSettings(
            config_sources=[
                cc.ConfigDBJsonSource(
                    config_support.configdb_key,
                    config_support.runkey,
                    {
                        "search_dict": json.loads(config_support.search_dict),
                        "object_type": "felix",
                        "config_type": "meta",
                    },
                )
            ]
        )

        with open(f"{FILE_DIR}/{felix_settings.felix_config_file}", "w") as f:
            f.write(json.dumps(felix_config.dict()))

    else:
        #Use config from env vars and check them against pydantic model

        felix_settings = FelixSettings(config_sources=cc.EnvSource(allow_all=True))

        file = f"{FILE_DIR}/{felix_settings.felix_config_file}"
        try:
            with open(file) as f:
                try:
                    json.load(f)
                    felix_config = FelixConfig(
                        config_sources=cc.FileSource(
                            file=f"{FILE_DIR}/{felix_settings.felix_config_file}"
                        )
                    )
                except Exception:
                    logger.warning(
                        f"Config file {file} could not be checked, because it is not in json format."
                    )
        except FileNotFoundError:
            logger.error(f"Config file not found: {file}")

    settings_dict = felix_settings.dict()

    for key, value in settings_dict.items():
        with open(f"/run/s6/container_environment/{key.upper()}", "w") as f:
            f.write(str(value))

        print(key.upper())
        print(value)


if __name__ == "__main__":
    main()
