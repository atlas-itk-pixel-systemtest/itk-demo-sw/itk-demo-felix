import json
import argparse
import os
import subprocess

NUM_DEVICES = 2  # device 0,1
NUM_LINKS = 12  # fiber link = 0..11
NUM_DEC_EGROUPS = 7  # decoder egroup = 0..5, but have to disable egroup #6
NUM_ENC_EGROUPS = 4  # encoder egroup = 0..3

cmds = []


def add_cmd(cmd):
    cmds.append(cmd)


def setup_icec(linkD, ena_icec):
    # ic
    add_cmd(f"MINI_EGROUP_FROMHOST_{linkD}_IC_ENABLE=0x{ena_icec}")
    add_cmd(f"MINI_EGROUP_TOHOST_{linkD}_IC_ENABLE=0x{ena_icec}")
    # ec
    add_cmd(f"MINI_EGROUP_FROMHOST_{linkD}_EC_ENABLE=0x{ena_icec}")
    add_cmd(f"MINI_EGROUP_TOHOST_{linkD}_EC_ENABLE=0x{ena_icec}")


def setup_tx(linkD, elinks):
    if type(elinks) == int:
        elinks = [elinks]
    for egroup in range(NUM_ENC_EGROUPS):
        ena = 0x0
        enc = 0x0
        width = 0x1  # 4b for all Epaths
        elink0 = link * 64 + egroup * 4
        elink2 = elink0 + 2
        if elink0 in elinks:
            ena |= 0x1  # Epath0
            enc |= 0x0004  # 4 - RD53 (4b per Epaths) Epath0
        if elink2 in elinks:
            ena |= 0x4  # Epath2
            enc |= 0x0400  # 4 - RD53 (4b per Epaths) Epath2

        cmdpref = f"ENCODING_LINK{linkD}_EGROUP{egroup}_CTRL_"
        add_cmd(
            cmdpref + f"EPATH_ENA=0x{ena}"
        )  # 0x1 - Epath0, 0x4 - Epath2, 0x5 - Epath0&2
        add_cmd(cmdpref + f"EPATH_WIDTH=0x{width}")  # 0x1 - 4b for all Epaths
        add_cmd(
            cmdpref + f"PATH_ENCODING=0x{enc}"
        )  # 4 - RD53 (4b per Epaths); 0x0004 - Epath0; 0x0400 - Epath2; 0x0404 - Epath0&2


def setup_rx(linkD, elinks):
    if type(elinks) == int:
        elinks = [elinks]
    for egroup in range(NUM_DEC_EGROUPS):
        ena = 0x0
        dec = 0x0
        width = 0x4  # 32b for all Epaths

        elink0 = link * 64 + egroup * 4
        elink1 = elink0 + 1
        if elink0 in elinks:
            ena |= 0x1  # Epath0 (Data Frames)
            dec |= 0x03  # 64b/66b encoding Epath0
        if elink1 in elinks:
            ena |= 0x2  # Epath1 (Register Frames)
            dec |= 0x30  # 64b/66b encoding Epath1

        cmdpref = f"DECODING_LINK{linkD}_EGROUP{egroup}_CTRL_"
        add_cmd(
            cmdpref + f"EPATH_ENA=0x{ena}"
        )  # 0x1 - Epath0, 0x4 - Epath2, 0x5 - Epath0&2
        add_cmd(cmdpref + f"EPATH_WIDTH=0x{width}")  # 0x1 - 4b for all Epaths
        add_cmd(
            cmdpref + f"PATH_ENCODING=0x{dec}"
        )  # 4 - RD53 (4b per Epaths); 0x0004 - Epath0; 0x0400 - Epath2; 0x0404 - Epath0&2


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="Input file (json format)", type=str)
    parser.add_argument("-o", "--output", help="Output file", type=str)
    args = parser.parse_args()

    if args.input is None:
        inputFile = "/config/flx.cfg.json"
    else:
        inputFile = args.input

    if args.output is None:
        outputFile = "/config/flx.cfg"
    else:
        outputFile = args.output

    with open(inputFile) as json_file:
        dataAll = json.load(json_file)

    card = dataAll["FelixID"]
    device = dataAll["DeviceID"]
    devPolarity = ""
    devStr = str(device)

    links = dataAll["Links"].keys()
    for link in range(NUM_LINKS):
        linkD = "%02d" % int(link)

        linkStr = str(link)
        if linkStr in links:
            # devPolarity = str( dataAll["Links"][linkStr]["polarity"] )+devPolarity
            if (
                dataAll["Links"][linkStr]["polarity"] == 0
            ):  # default polarity (0 == default == reset)
                subprocess.run(
                    [
                        "fgpolarity",
                        "-c",
                        str(card),
                        "-G",
                        str(device * NUM_LINKS + link),
                        "-r",
                        "reset",
                    ]
                )
            else:  # reversed polarity (1 == reverse == set)
                subprocess.run(
                    [
                        "fgpolarity",
                        "-c",
                        str(card),
                        "-G",
                        str(device * NUM_LINKS + link),
                        "-r",
                        "set",
                    ]
                )

            # icec
            if dataAll["Links"][linkStr]["icec"] == 1:
                ena_icec = 1
            else:
                ena_icec = 0
            setup_icec(linkD, ena_icec)
            # tx
            txs = dataAll["Links"][linkStr]["tx"]
            setup_tx(linkD, txs)
            # rx
            rxs = dataAll["Links"][linkStr]["rx"]
            setup_rx(linkD, rxs)

        else:
            # devPolarity = "0"+devPolarity
            # icec
            setup_icec(linkD, 0)
            # tx
            setup_tx(linkD, [])
            # rx
            setup_rx(linkD, [])

    # devPolarity=hex( int(devPolarity,2) )
    cmd2 = []
    # cmd2.append( f"DeviceID={device}" )
    cmd2.append(f"LPGBT_FEC=0xFFFFFFFFFFFF")
    cmd2.append(f"GBT_DATA_RXFORMAT2=0xFFFFFFFFFFFF")
    # cmd2.append(f"GBT_RXPOLARITY=0xFFFFFFFFFF") #{devPolarity}")
    cmd2.append(f"DECODING_REVERSE_10B=0x1")
    cmds = cmd2 + cmds

    with open(outputFile, "w") as file:
        for cmd in cmds[:-1]:
            file.write(cmd + "\n")
        file.write(cmds[-1])

# for cmd in cmds[1:]:
#   print(f"flx-config -d {device} set {cmd}")

# os.system( f"mv {jsonFile} {jsonFile}.json" )
# os.system( f"mv {fileName} {jsonFile}" )
