import os

# in the comments the allowed options for the pixel F/W flavors are indicated
NUM_LINK = 12  # fiber link = 0..11
NUM_DEC_EGROUP = 7  # decoder egroup = 0..5, but have to disable egroup #6
NUM_ENC_EGROUP = 4  # encoder egroup = 0..3


def exec(cmd):
    print(cmd)
    # os.system(cmd)


def init_felix():
    print("Init FELIX")
    exec("flx-init")


def set_ic(dev, link, ena):
    # ena: 0x1 - enable, 0x0 - disable
    exec(
        "flx-config -d %d set MINI_EGROUP_FROMHOST_%02d_IC_ENABLE=0x%x"
        % (dev, link, ena)
    )
    exec(
        "flx-config -d %d set MINI_EGROUP_TOHOST_%02d_IC_ENABLE=0x%x" % (dev, link, ena)
    )


def set_ec(dev, link, ena):
    # ena: 0x1 - enable, 0x0 - disable
    exec(
        "flx-config -d %d set MINI_EGROUP_FROMHOST_%02d_EC_ENABLE=0x%x"
        % (dev, link, ena)
    )
    exec(
        "flx-config -d %d set MINI_EGROUP_TOHOST_%02d_EC_ENABLE=0x%x" % (dev, link, ena)
    )


def set_elink_enc(dev, link, egroup, ena, width, enc):
    cmdpref = "flx-config -d %d set ENCODING_LINK%02d_EGROUP%d_CTRL_" % (
        dev,
        link,
        egroup,
    )
    exec(cmdpref + "EPATH_ENA=0x%x" % ena)  # 0x1 - Epath0, 0x4 - Epath2, 0x5 - Epath0&2
    exec(cmdpref + "EPATH_WIDTH=0x%x" % width)  # 0x1 - 4b for all Epaths
    exec(
        cmdpref + "PATH_ENCODING=0x%x" % enc
    )  # 4 - RD53 (4b per Epaths); 0x0004 - Epath0; 0x0400 - Epath2; 0x0404 - Epath0&2


def set_elink_dec(dev, link, egroup, ena, width, dec):
    cmdpref = "flx-config -d %d set DECODING_LINK%02d_EGROUP%d_CTRL_" % (
        dev,
        link,
        egroup,
    )
    exec(
        cmdpref + "EPATH_ENA=0x%x" % ena
    )  # 0x1 - Epath0 (Data Frames), 0x2 - Epath1 (Register Frames), 0x3 - Epath0&1
    exec(cmdpref + "EPATH_WIDTH=0x%x" % width)  # 0x4 - 32b for all Epaths
    exec(
        cmdpref + "PATH_ENCODING=0x%x" % dec
    )  # 3 - 64b/66b; 0x03 - Epath0, 0x30 - EPath1, 0x33 - Epath0&1


def setup_ic(dev, links):
    cfg_links = set()
    for link in range(NUM_LINK):
        ena = 0

        if link in links:
            ena = 1
            cfg_links.add(link)

        set_ic(dev, link, ena)

    if cfg_links != links:
        print("cannot configure ic elinks:", links - cfg_links)


def setup_ec(dev, links):
    cfg_links = set()
    for link in range(NUM_LINK):
        ena = 0

        if link in links:
            ena = 1
            cfg_links.add(link)

        set_ec(dev, link, ena)

    if cfg_links != links:
        print("cannot configure ec elinks:", links - cfg_links)


def setup_tx(dev, elinks):
    cfg_elinks = set()
    for link in range(NUM_LINK):
        for egroup in range(NUM_ENC_EGROUP):
            ena = 0x0
            enc = 0x0
            width = 0x1  # 4b for all Epaths

            elink0 = link * 64 + egroup * 4
            elink2 = elink0 + 2

            if elink0 in elinks:
                ena |= 0x1  # Epath0
                enc |= 0x0004  # 4 - RD53 (4b per Epaths) Epath0
                cfg_elinks.add(elink0)

            if elink2 in elinks:
                ena |= 0x4  # Epath2
                enc |= 0x0400  # 4 - RD53 (4b per Epaths) Epath2
                cfg_elinks.add(elink2)

            set_elink_enc(dev, link, egroup, ena=ena, width=width, enc=enc)

    if cfg_elinks != elinks:
        print("cannot configure tx elinks:", elinks - cfg_elinks)


def setup_rx(dev, elinks):
    cfg_elinks = set()
    for link in range(NUM_LINK):
        for egroup in range(NUM_DEC_EGROUP):
            ena = 0x0
            dec = 0x0
            width = 0x4  # 32b for all Epaths

            elink0 = link * 64 + egroup * 4
            elink1 = elink0 + 1

            if elink0 in elinks:
                ena |= 0x1  # Epath0 (Data Frames)
                dec |= 0x03  # 64b/66b encoding Epath0
                cfg_elinks.add(elink0)
            if elink1 in elinks:
                ena |= 0x2  # Epath1 (Register Frames)
                dec |= 0x30  # 64b/66b encoding Epath1
                cfg_elinks.add(elink1)

            set_elink_dec(dev, link, egroup, ena=ena, width=width, dec=dec)

    if cfg_elinks != elinks:
        print("cannot configure rx elinks:", elinks - cfg_elinks)


class Felix:
    def __init__(self, dev):
        self.dev = dev
        # self.ic_enabled = False
        # self.ec_enabled = False
        self.ic_links = set()
        self.ec_links = set()
        self.tx_elinks = set()
        self.rx_elinks = set()

    def enable_ic(self, link):
        self.ic_links.add(link)

    def enable_ec(self, link):
        self.ec_links.add(link)

    def enable_tx(self, elink):
        self.tx_elinks.add(elink)

    def enable_rx(self, elink):
        self.rx_elinks.add(elink)

    def enable_icec(self, link):
        self.enable_ic(link)
        self.enable_ec(link)

    def configure(self):
        print("Configuring FELIX dev: %d" % self.dev)

        exec("flx-config -d %d set LPGBT_FEC=0xFFF" % self.dev)

        # Change the FEC 5 settings to FEC 12 because optoBoards use FEC 12
        exec("flx-config -d %d set GBT_DATA_RXFORMAT2=0xFF" % self.dev)
        # because OB4 needs polarity swap (was 8 before)
        # exec("flx-config -d %d set GBT_RXPOLARITY=0x0" % self.dev)
        exec("flx-config -d %d set GBT_RXPOLARITY=0xF" % self.dev)

        exec("flx-config -d %d set DECODING_REVERSE_10B=0x1" % self.dev)

        setup_ic(self.dev, self.ic_links)
        setup_ec(self.dev, self.ic_links)
        setup_tx(self.dev, self.tx_elinks)
        setup_rx(self.dev, self.rx_elinks)
