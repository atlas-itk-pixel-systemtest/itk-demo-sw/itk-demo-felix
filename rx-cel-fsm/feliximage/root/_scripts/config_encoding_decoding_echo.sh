#!/usr/bin/env bash
#in the comments the allowed options for the pixel F/W flavors are indicated
LINK_MIN=0
LINK_STOP=11 # total channels / 2 was 2 before
LINK_MAX=11  # total channels / 2
DEC_EGROUP_MIN=0
DEC_EGROUP_MAX=6
ENC_EGROUP_MIN=0
ENC_EGROUP_MAX=4 #was 3 before

echo DECODING_REVERSE_10B=0x1

for ((il = LINK_MIN; il <= LINK_STOP; il++)); do
	# echo "# Decoding Settings for Link: ${il}"
	il2=$(printf '%02d\n' $il)
	echo "MINI_EGROUP_FROMHOST_${il2}_EC_ENABLE=0x1"
	echo "MINI_EGROUP_TOHOST_${il2}_EC_ENABLE=0x1"
	echo "MINI_EGROUP_FROMHOST_${il2}_IC_ENABLE=0x1"
	echo "MINI_EGROUP_TOHOST_${il2}_IC_ENABLE=0x1"

	for ((ig = DEC_EGROUP_MIN; ig <= DEC_EGROUP_MAX; ig++)); do
		# echo "# ***Decoding Egroup ${ig}***"

		echo "DECODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_ENA=0x1"
		echo "DECODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_WIDTH=0x4"
		echo "DECODING_LINK${il2}_EGROUP${ig}_CTRL_PATH_ENCODING=0x33"
	done

	for ((ig = ENC_EGROUP_MIN; ig <= ENC_EGROUP_MAX; ig++)); do
		# echo "# Encoding Egroup ${ig}"
		echo "ENCODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_ENA=0x5"
		echo "ENCODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_WIDTH=0x1"
		echo "ENCODING_LINK${il2}_EGROUP${ig}_CTRL_PATH_ENCODING=0x0404"

	done

done

for ((il = LINK_STOP + 1; il <= LINK_MAX; il++)); do
	# echo "# Disabling Settings for Link: ${il}"
	il2=$(printf '%02d\n' $il)
	echo "MINI_EGROUP_FROMHOST_${il2}_EC_ENABLE=0x0"
	echo "MINI_EGROUP_TOHOST_${il2}_EC_ENABLE=0x0"
	echo "MINI_EGROUP_FROMHOST_${il2}_IC_ENABLE=0x0"
	echo "MINI_EGROUP_TOHOST_${il2}_IC_ENABLE=0x0"

	for ((ig = DEC_EGROUP_MIN; ig <= DEC_EGROUP_MAX; ig++)); do
		# echo "# ***Decoding Egroup ${ig}***"
		echo "DECODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_ENA=0x0"
		echo "DECODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_WIDTH=0x0"
		echo "DECODING_LINK${il2}_EGROUP${ig}_CTRL_PATH_ENCODING=0x0"
	done

	for ((ig = ENC_EGROUP_MIN; ig <= ENC_EGROUP_MAX; ig++)); do
		# echo "# Encoding Egroup ${ig}"
		echo "ENCODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_ENA=0x0"
		echo "ENCODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_WIDTH=0x0"
		echo "ENCODING_LINK${il2}_EGROUP${ig}_CTRL_PATH_ENCODING0=0x0"
	done

done
