"""
It is the file where the content and layout of 
the page at http://localhost:3000/ are determined.

Ali Can Canbay - acanbay@cern.ch    |   9 Aug 2023

Last edit: 15 Aug 2023 by acanbay
"""

import reflex as rx
from GUI_components.navbar import navbar
from modules.settings import felix_settings, FILE_DIR
import modules.base as bs

button1 = {
    "width": "100px",
    "height": "40px",
    "justify-content": "center",
    "align-items": "center",
    "overflow": "hidden",
}

button2 = {
    "width": "130px",
    "height": "40px",
    "justify-content": "center",
    "align-items": "center",
    "overflow": "hidden",
}


class ModalSettingsState(rx.State):
    show: bool = False

    def change(self):
        self.show = not (self.show)


class ModalConfigState(rx.State):
    show: bool = False

    def change(self):
        self.show = not (self.show)


# The part associated with the configuration files
def configuration_part(State):
    return rx.vstack(
        rx.hstack(
            rx.box(
                rx.button("Show Settings", on_click=ModalSettingsState.change),
                rx.modal(
                    rx.modal_overlay(
                        rx.modal_content(
                            rx.modal_header("FELIX Settings"),
                            rx.modal_body(
                                bs.get_settings(), style={"white-space": "pre-line"}
                            ),
                            rx.modal_footer(
                                rx.button("Close", on_click=ModalSettingsState.change)
                            ),
                        )
                    ),
                    is_open=ModalSettingsState.show,
                ),
            ),
            rx.box(
                rx.button("Show Config", on_click=ModalConfigState.change),
                rx.modal(
                    rx.modal_overlay(
                        rx.modal_content(
                            rx.modal_header("FELIX Config"),
                            rx.modal_body(
                               bs.get_config(), style={"white-space": "pre-line"}
                            ),
                            rx.modal_footer(
                                rx.button("Close", on_click=ModalConfigState.change)
                            ),
                        )
                    ),
                    is_open=ModalConfigState.show,
                ),
            ),
            position="fixed",
            left="20px",
            top="170px",
        )
    )

    # rx.heading(
    #     "Configurations Files",
    #     font_size="1.2em",
    #     position="fixed",
    #     left="20px",
    #     top="150px",
    # ),
    # rx.hstack(
    #     rx.hstack(
    #         rx.select(
    #             State.configuration_Files,
    #             height="50px",
    #             width="350px",
    #             value=State.selected_config,
    #             on_change=State.set_selected_config,
    #         ),
    #     ),
    #     rx.button(
    #         "Set Default",
    #         on_click=State.set_default(),
    #     ),
    #     rx.box(
    #         rx.button("Delete", on_click=State.change_delete_popup),
    #         rx.modal(
    #             rx.modal_overlay(
    #                 rx.modal_content(
    #                     rx.modal_header("Confirm"),
    #                     rx.modal_body("Are you sure you want to delete " + State.selected_config + "?"),
    #                     rx.modal_footer(
    #                         rx.button("Delete", on_click=State.confirm_delete),
    #                         rx.text(width="10px"),
    #                         rx.button("Cancel", on_click=State.cancel_delete),
    #                     ),
    #                 )
    #             ),
    #             is_open=State.delete_popup,
    #         ),
    #     ),
    #     position="fixed",
    #     left="20px",
    #     top="170px",
    # ),
    # )


# The part associated with FELIX commands
def felix_part(State):
    return rx.hstack(
        rx.button(
            "Init FELIX",
            on_click=State.init_FELIX,
            style=button1,
        ),
        rx.button(
            "Configure FELIX",
            style=button2,
            on_click=State.configure_FELIX,
        ),
        rx.cond(
            State.FELIX_button_text == "Wait",
            rx.button(
                State.FELIX_button_text,
                bg="gray",
                color="black",
                is_loading=True,
                style=button1,
            ),
            rx.button(
                State.FELIX_button_text,
                on_click=State.change_Felix_Status,
                style=button1,
            ),
        ),
        rx.circle(
            width="1.5em",
            height="1.5em",
            border_width="thin",
            border_color="black",
            background_color=State.FELIX_status_color,
            z_index=1,
        ),
        rx.text(width="680px"),
        rx.button(
            "Refresh page",
            on_click=State.refresh,
            style=button2,
        ),
        position="fixed",
        left="20px",
        top="280px",
    )


# Output part
def output_part(State):
    return rx.vstack(
        rx.heading(
            "Outputs:",
            font_size="1.2em",
            position="fixed",
            left="20px",
            top="370px",
        ),
        rx.hstack(
            rx.text_area(
                default_value=State.Output,
                is_read_only=True,
                height="400px",
                width="1200px",
            ),
            text_align="top",
            border="1px dotted black",
            padding="0.1em",
            shadow="lg",
            border_radius="lg",
            position="fixed",
            left="20px",
            top="390px",
        ),
    )


# Main module combining parts
def main(State):
    return rx.vstack(
        rx.heading("FELIX API - itk-felix-sw", font_size="3em"),
        rx.divider(
            border_color="black",
            # position="fixed",
            # top="60px",
        ),
        # Disable 'Home' as it leads to the currently selected panel
        navbar(State, True, False, False, "fixed"),
        rx.divider(
            border_color="black",
            # position="fixed",
            # top="98px",
        ),
        configuration_part(State),
        rx.divider(
            position="fixed",
            top="250px",
        ),
        felix_part(State),
        rx.divider(
            position="fixed",
            top="350px",
        ),
        output_part(State),
    )
