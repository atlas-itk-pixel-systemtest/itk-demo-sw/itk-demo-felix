"""
It is the file where the content and layout of 
the page at http://localhost:3000/ are determined.

Ali Can Canbay - acanbay@cern.ch    |   9 Aug 2023

Last edit: 18 Mar 2024 by vormwald
"""

import json
import logging
import os
import re
import time
import subprocess
from os import listdir
from os.path import isfile, join
import requests

from modules.settings import felix_settings, FILE_DIR

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


configurations_path = "/config/"
params_path = "params/"
outputs_path = "outputs/"

NOFLX = False
# NOFLX = True


def get_settings():
    dic = felix_settings.dict()
    my_string = "\n".join(f"{k}: {v}" for k, v in dic.items())
    return my_string


def get_config():
    with open(os.path.join(FILE_DIR, felix_settings.felix_config_file), "r") as file:
        raw_config = file.read()
    return raw_config


def publish_state(state, serials):
    sr_url = felix_settings.sr_url
    if sr_url:
        dic = {}
        for serial in serials:
            dic[f"demi/dashboard/status/{serial}"] = state

        headers = {
            "accept": "application/json",
            "Content-Type": "application/json",
        }
        requests.post(f"{sr_url}/set_many/", headers=headers, json={"kv_dict": dic})


def debug(func):
    """debugging decorator"""

    def wrapper(*args, **kwargs):
        logger.debug(f"{func.__name__} ({args}, {kwargs})")
        result = func(*args, **kwargs)
        logger.debug(f"{func.__name__}: {result}")
        return result

    return wrapper


# @debug
def run_flx(cmd):
    if NOFLX:
        r = f"NOFLX: would call {cmd}"
        logger.info(r)
        return r.strip(), True

    logger.info(f"run_flx: Running {cmd}")

    r = subprocess.run(
        cmd,
        capture_output=True,
        text=True,
    )

    logger.info(f"{cmd!r} exited with {r.returncode}")

    if r.stdout:
        logger.info(f"[stdout] {r.stdout}")
    if r.stderr or r.returncode != 0:
        logger.info(f"[stderr] {r.stderr}")
        return r.stdout + r.stderr, False

    return r.stdout + r.stderr, True


## FastAPI part
## http://localhost:8000
def reset_config_params():
    config_files = [f for f in listdir(configurations_path) if isfile(join(configurations_path, f)) if f.endswith(".json")]
    config_files.sort()

    default_file = felix_settings.felix_config_file

    data = {"Configs": {}, "Default": None}
    default_idx = -1
    for i in range(len(config_files)):
        data["Configs"][i] = config_files[i]
        if config_files[i] == default_file:
            default_idx = i
    data["Default"] = default_idx

    with open(params_path + "/config.params", "w") as outfile:
        json.dump(data, outfile, indent=2)

    return data


def get_config_params():
    return reset_config_params()


def get_configs():
    reset_config_params()
    return get_config_params()["Configs"]


def change_default_config(fname):
    felix_settings.felix_config_file = fname
    reset_config_params()


def delete_config(fname):
    # os.system(f"rm {configurations_path}/{fname}")
    reset_config_params()


def get_FELIX_params():
    with open(params_path + "/felix.params") as json_file:
        data = json.load(json_file)
    return data


def change_FELIX_params(Status):
    data = get_FELIX_params()
    data["FELIX Status"] = Status

    with open(params_path + "/felix.params", "w") as outfile:
        json.dump(data, outfile, indent=2)


@debug
async def init_FELIX(set_felix_state):
    """Initialize FELIX card"""
    try:
        r, success = run_flx("flx-init")
    except Exception as e:
        logger.exception(e)

    if success:
        set_felix_state("FelixInitialized")
        r = r + "\n" + "FELIX is initialized"
        publish_state(
            "warning",
            [
                felix_settings.felix_device0_serial,
                felix_settings.felix_device1_serial,
                felix_settings.serial,
            ],
        )
    else:
        r = r + "\n" + "Something went wrong, FELIX is not initialized"
        publish_state(
            "danger",
            [
                felix_settings.felix_device0_serial,
                felix_settings.felix_device1_serial,
                felix_settings.serial,
            ],
        )
    return r.strip(), success


async def load_FW_trigger_sequence(sequenceID):
    """Configure FELIX HW trigger sequence"""

    try:
        r1, success1 = run_flx(["/bin/bash", "/scripts/config_CalTrigSeq.sh", str(sequenceID), str(felix_settings.felix_device0_number)])
    except Exception as e:
        logger.exception(e)
        return r1.strip(), False

    try:
        r2, success2 = run_flx(["/bin/bash", "/scripts/config_CalTrigSeq.sh", str(sequenceID), str(felix_settings.felix_device1_number)])
    except Exception as e:
        logger.exception(e)
        return r2.strip(), False

    return "Success", True

async def configure_FELIX(set_felix_state):
    """Configure FELIX card"""

    try:
        r, success = run_flx(
            [
                "python3",
                "/scripts/convertJSON.py",
                "-i",
                configurations_path + felix_settings.felix_config_file,
                "-o",
                configurations_path,
            ]
        )
    except Exception as e:
        logger.exception(e)
        return r, False
    configfilebasename = felix_settings.felix_config_file.split(".")[0]
    config_file_dev0 = configfilebasename + "-dev" + str(felix_settings.felix_device0_number) + ".cfg"
    config_file_dev1 = configfilebasename + "-dev" + str(felix_settings.felix_device1_number) + ".cfg"

    try:
        r, success0 = run_flx(
            [
                "flx-config",
                "-d",
                str(felix_settings.felix_device0_number),
                "load",
                os.path.join(configurations_path, config_file_dev0),
            ]
        )
    except Exception as e:
        logger.exception(e)
        return r.strip(), False

    if success0:
        set_felix_state("FelixConfigured")
        r = r + "\n" + "FELIX device " + str(felix_settings.felix_device0_number) + " is configured"
        publish_state("warning", [felix_settings.felix_device0_serial])
    else:
        r = r + "\n" + "Something went wrong, FELIX device " + str(felix_settings.felix_device0_number) + " is not configured"
        publish_state("danger", [felix_settings.felix_device0_serial])
    try:
        r, success1 = run_flx(
            [
                "flx-config",
                "-d",
                str(felix_settings.felix_device1_number),
                "load",
                os.path.join(configurations_path, config_file_dev1),
            ]
        )
    except Exception as e:
        logger.exception(e)
        return r.strip(), False

    if success1:
        set_felix_state("FelixConfigured")
        r = r + "\n" + "FELIX device " + str(felix_settings.felix_device1_number) + " is configured"
        publish_state("warning", [felix_settings.felix_device1_serial])
    else:
        r = r + "\n" + "Something went wrong, FELIX device " + str(felix_settings.felix_device1_number) + " is not configured"
        publish_state("danger", [felix_settings.felix_device1_serial])

    if success0 and success1:
        publish_state("warning", [felix_settings.serial])

    return r.strip(), success0 and success1


async def start_FELIX(set_felix_state):
    """Start FELIX app"""
    try:
        r1, success1 = run_flx(["s6-rc", "start", "svc-felix-toflx-dev0"])
        r2, success2 = run_flx(["s6-rc", "start", "svc-felix-toflx-dev1"])
        r3, success3 = run_flx(["s6-rc", "start", "svc-felix-tohost-dev0"])
        r4, success4 = run_flx(["s6-rc", "start", "svc-felix-tohost-dev1"])
        r5, success5 = run_flx(["s6-rc", "start", "svc-felix-register"])
    except Exception as e:
        # TODO is this correct?
        set_felix_state("FelixOff")
        logger.exception(e)

    success = success1 and success2 and success3 and success4 and success5
    if success:
        set_felix_state("FelixRunning")
        r = r1 + "\n" + r2 + "\n" + r3 + "\n" + r4 + "\n" + r5 + "\n" + "FELIX is running"
        publish_state("success", [felix_settings.felix_device0_serial, felix_settings.felix_device1_serial, felix_settings.serial])

    else:
        r = r1 + "\n" + r2 + "\n" + r3 + "\n" + r4 + "\n" + r5 + "\n" + "Something went wrong, FELIX is not running"
        publish_state("danger", [felix_settings.felix_device0_serial, felix_settings.felix_device1_serial, felix_settings.serial])
    return r.strip(), success


async def stop_FELIX(set_felix_state):
    """Stop FELIX app"""
    try:
        r1, success1 = run_flx(["s6-rc", "stop", "svc-felix-toflx-dev0"])
        r2, success2 = run_flx(["s6-rc", "stop", "svc-felix-toflx-dev1"])
        r3, success3 = run_flx(["s6-rc", "stop", "svc-felix-tohost-dev0"])
        r4, success4 = run_flx(["s6-rc", "stop", "svc-felix-tohost-dev1"])
        r5, success5 = run_flx(["s6-rc", "stop", "svc-felix-register"])
    except Exception as e:
        logger.exception(e)

    success = success1 and success2 and success3 and success4 and success5
    if success:
        set_felix_state("FelixOff")
        r = r1 + "\n" + r2 + "\n" + r3 + "\n" + r4 + "\n" + r5 + "\n" + "FELIX is stopped"
        publish_state("danger", [felix_settings.felix_device0_serial, felix_settings.felix_device1_serial, felix_settings.serial])
    else:
        r = r1 + "\n" + r2 + "\n" + r3 + "\n" + r4 + "\n" + r5 + "\n" + "Something went wrong, FELIX is not stopped"
        publish_state("danger", [felix_settings.felix_device0_serial, felix_settings.felix_device1_serial, felix_settings.serial])
    return r.strip(), success


# def flx_config_store():
#    try:
#        run_flx(
#            [
#                "flx-config",
#                "-d",
#                str(felix_settings.felix_device_number),
#                "-E",
#                "store",
#                "/config/flx-config-store.txt",
#            ]
#        )
#    except Exception as e:
#        logger.exception(e)
#
#    regs = {}
#
#    with open("/config/flx-config-store.txt") as f:
#        lines = f.readlines()
#        for li in lines:
#            try:
#                reg, val = re.split("=", li)
#            except Exception:
#                logger.error(f"Can't parse {li}")
#                continue
#
#            logger.debug(f"{reg} = {val}")
#            regs[reg] = int(val, 0)
#
#    return regs


def flx_config_get(dev: int, reg: str):
    try:
        r, success = run_flx(["flx-config", "-d", str(dev), "get", reg])
    except Exception as e:
        logger.exception(e)

    return r.strip(), success


def flx_config_set(dev: int, reg: str, value: str, enforce: bool):
    try:
        if enforce:
            r, success = run_flx(["flx-config", "-d", str(dev), "-E", "set", reg, str(value)])
        else:
            r, success = run_flx(["flx-config", "-d", str(dev), "set", reg, str(value)])
    except Exception as e:
        logger.exception(e)

    return r.strip(), success


def gbt_reset(dev: int, tx: bool):
    r1 = ""
    r2 = ""
    success1 = False
    success2 = False
    if tx:
        reg = "GBT_TX_RESET"
    else:
        reg = "GBT_RX_RESET"
    try:
        r1, success1 = flx_config_set(dev=dev, reg=reg, value="0xFFF", enforce=True)
        time.sleep(1)
        r2, success2 = flx_config_set(dev=dev, reg=reg, value="0x0", enforce=True)
    except Exception as e:
        logger.exception(e)

    r = r1.strip() + r2.strip()
    success = success1 and success1

    return r, success


def opto_reg_read(settings: str, reg: str):
    try:
        settings = settings.split("-")[1:]
        command = ["flpgbtconf"]
        for x in settings:
            if x[0] == "G":
                command.append("-G")
                command.append(str(x[1:]))
            if x[0] == "I":
                command.append("-I")
                command.append(str(x[1:]))
            if x[0] == "d":
                command.append("-d")
                command.append(str(x[1:]))
        if "1" in settings:
            command.append("-1")
        command.append(reg)
        r, success = run_flx(command)
    except Exception as e:
        logger.exception(e)
    return r.strip(), success


def opto_reg_write(settings: str, reg: str, value: str):
    try:
        settings = settings.split("-")[1:]
        command = ["flpgbtconf"]
        for x in settings:
            if x[0] == "G":
                command.append("-G")
                command.append(str(x[1:]))
            if x[0] == "I":
                command.append("-I")
                command.append(str(x[1:]))
            if x[0] == "d":
                command.append("-d")
                command.append(str(x[1:]))
        if "1" in settings:
            command.append("-1")
        command.append(reg)
        command.append(str(value))
        r, success = run_flx(command)
    except Exception as e:
        logger.exception(e)
    return r.strip(), success


## GUI part
## http://localhost:3000


def extract_text_between_words(text, word1, word2):
    start = text.find(word1)
    if start == -1:
        return None

    start += len(word1)
    end = text.find(word2, start)
    if end == -1:
        return None

    return text[start:end].strip()


def get_elink_configurations():
    lines = []
    if NOFLX:
        with open(outputs_path + "/elink.txt") as file:
            lines = [line.rstrip() for line in file]
    else:
        allaligned = []
        for dev in [
            felix_settings.felix_device0_number,
            felix_settings.felix_device1_number,
        ]:
            r = subprocess.run(
                # AKBotch    #     ["flx-config list | grep ALIGNED_"],
                ["flx-config", "-d", str(dev), "list"],
                capture_output=True,
                text=True,
            )
            lines = r.stdout.split("\n")
            # alignkey = 'ALIGNED_'
            # aligned = [line for line in lines if re.search(alignkey, line)]
            # Output = {}
            # i = 0
            # AKBotch    # for line in lines:
            # for line in aligned:
            #     text = extract_text_between_words(line, "0x0000000", " Every")

            #     Output[i] = [elink for elink in range(10*4) if ((int(text,16)>>elink)&1)]
            #     i+=1

            # return Output

            # AKBotch        for line in lines:

            #    alignkey = 'ALIGNED_'
            alignkey = "LINK_ALIGNED_"
            aligned = [line for line in lines if re.search(alignkey, line)]
            allaligned.extend(aligned[0:12])

    Output = {}
    i = 0
    for line in allaligned:
        text = extract_text_between_words(line, "0x0000000", " Every")

        Output[i] = [elink for elink in range(10 * 4) if ((int(text, 16) >> elink) & 1)]
        i += 1

    return Output


def get_olink_configurations():
    lines = []
    if NOFLX:
        with open(outputs_path + "/link.txt") as file:
            lines = [line.rstrip() for line in file]
    else:
        r = subprocess.run(["flx-info", "link"], capture_output=True, text=True)
        lines = r.stdout.split("\n")

    # Output = {}
    # i = 0
    # for line in lines:
    #     text = extract_text_between_words(line, "0x0000000", " Every")

    #     Output[i] = [elink for elink in range(10*4) if ((int(text,16)>>elink)&1)]
    #     i+=1

    print(lines)

    Output = {}

    line_device_0 = lines[7]
    line_device_0 = line_device_0.split(" ")
    line_device_0 = line_device_0[2:]
    line_device_0 = [l for l in line_device_0 if l != ""]

    line_device_1 = lines[11]
    line_device_1 = line_device_1.split(" ")
    line_device_1 = line_device_1[2:]
    line_device_1 = [l for l in line_device_1 if l != ""]

    for i in range(len(line_device_0)):
        if line_device_0[i] == "YES":
            Output[i] = True
        else:
            Output[i] = False

    for i in range(len(line_device_1)):
        if line_device_1[i] == "YES":
            Output[i + len(line_device_0)] = True
        else:
            Output[i + len(line_device_0)] = False

    return dict(sorted(Output.items()))


def get_opower():
    lines = []
    if NOFLX:
        with open(outputs_path + "/pod.txt") as file:
            lines = [line.rstrip() for line in file]
    else:
        r = subprocess.run(["flx-info", "pod"], capture_output=True, text=True)
        lines = r.stdout.split("\n")

    # Output = {}
    # i = 0
    # for line in lines:
    #     text = extract_text_between_words(line, "0x0000000", " Every")

    #     Output[i] = [elink for elink in range(10*4) if ((int(text,16)>>elink)&1)]
    #     i+=1

    Output = {"TX": {}, "RX": {}}

    for line in lines[14:22]:
        line_tmp = line.split("|")
        if "1st TX" in line_tmp[0]:
            for i in range(1, len(line_tmp) - 1): Output["TX"][i - 1] = line_tmp[i].strip()
        if "2nd TX" in line_tmp[0]:
            for i in range(1, len(line_tmp) - 1): Output["TX"][i + 11] = line_tmp[i].strip()
        if "1st RX" in line_tmp[0]:
            for i in range(1, len(line_tmp) - 1): Output["RX"][i - 1] = line_tmp[i].strip()
        if "2nd RX" in line_tmp[0]:
            for i in range(1, len(line_tmp) - 1): Output["RX"][i + 11] = line_tmp[i].strip()

    return Output
