"""
It is the file where the content and layout of 
the Monitoring panel are determined.

Ali Can Canbay - acanbay@cern.ch    |   9 Aug 2023
Maren Stratmann - maren.stratmann@cern.ch

"""
import reflex as rx
from GUI_components.navbar import navbar


# Number of Links
def nlinks():
    links = [rx.heading("Links :", font_size="1.2em")]
    heading_stack = [rx.heading(str(i), font_size="1.2em") for i in range(0, 24)]
    return links + heading_stack


# Optical power Part
def pod(State):
    return [
        rx.vstack(
            rx.heading("Optical Power", font_size="1.3em"),
            rx.hstack(
                rx.button(
                    "Read",
                    on_click=State.read_pod,
                    height="30px",
                    width="60px",
                ),
                rx.link(
                    rx.button(
                        "Advanced",
                        on_click=State.read_pod,
                        height="30px",
                        width="100px",
                    ),
                    href="/opower",
                    button=True,
                ),
            ),
        ),
        rx.vstack(
            rx.heading("TX:", font_size="1.2em"),
            rx.heading("RX:", font_size="1.2em"),
        ),
    ]


# checkboxes per link
def pod_checkboxes(State):
    checkbox_stack = [
        rx.vstack(
            rx.checkbox(color_scheme="green", size="lg", is_disabled=True, is_checked=State.Optical_power_bool["TX"][i]),
            rx.checkbox(color_scheme="green", size="lg", is_disabled=True, is_checked=State.Optical_power_bool["RX"][i]),
        )
        for i in range(0, 24)
    ]
    return checkbox_stack


# Optical link alignment part
def link(State):
    return rx.vstack(
        rx.heading("Optical link alignment", font_size="1.3em"),
        rx.button(
            "Read",
            on_click=State.read_link,
            height="30px",
            width="60px",
        ),
    )


def link_checkboxes(State):
    checkbox_stack = [
        rx.vstack(
            rx.checkbox(color_scheme="green", size="lg", is_disabled=True, is_checked=State.Optical_link_bool[i]),
        )
        for i in range(0, 24)
    ]
    return checkbox_stack


# Electrical link alignment part
def elink(State):
    first_col = [
        rx.vstack(
            rx.heading("Electrical link alignment", font_size="1.3em"),
            rx.button(
                "Read",
                on_click=State.read_elink,
                height="30px",
                width="60px",
            ),
        )
    ]

    elinks = [0, 4, 8, 12, 16, 20, 24, 25]
    second_col_cont = [
        rx.heading(
            str(i) + " :",
            font_size="1.2em",
        )
        for i in elinks
    ]
    second_col = [rx.vstack(*second_col_cont)]

    return first_col + second_col


def elink_checkboxes(State):
    checkbox_stack = [
        rx.vstack(
            rx.checkbox(color_scheme="green", size="lg", is_disabled=True, is_checked=State.Electrical_link_bool["0"][i]),
            rx.checkbox(color_scheme="green", size="lg", is_disabled=True, is_checked=State.Electrical_link_bool["4"][i]),
            rx.checkbox(color_scheme="green", size="lg", is_disabled=True, is_checked=State.Electrical_link_bool["8"][i]),
            rx.checkbox(color_scheme="green", size="lg", is_disabled=True, is_checked=State.Electrical_link_bool["12"][i]),
            rx.checkbox(color_scheme="green", size="lg", is_disabled=True, is_checked=State.Electrical_link_bool["16"][i]),
            rx.checkbox(color_scheme="green", size="lg", is_disabled=True, is_checked=State.Electrical_link_bool["20"][i]),
            rx.checkbox(color_scheme="green", size="lg", is_disabled=True, is_checked=State.Electrical_link_bool["24"][i]),
            rx.checkbox(color_scheme="green", size="lg", is_disabled=True, is_checked=State.Electrical_link_bool["25"][i]),
        )
        for i in range(0, 24)
    ]
    return checkbox_stack


def table(State):
    return rx.table_container(
        rx.table(
            rows=[
                (" ", *nlinks()),
                (*pod(State), *pod_checkboxes(State)),
                (link(State), " ", *link_checkboxes(State)),
                (*elink(State), *elink_checkboxes(State)),
            ],
            size="sm",
        )
    )


# Main module combining parts
def main(State):
    return rx.vstack(
        rx.heading("FELIX API - itk-felix-sw", font_size="3em"),
        rx.divider(
            border_color="black",
            # position="fixed",
            # top="60px",
        ),
        # Disable 'Monitoring' as it leads to the currently selected panel
        navbar(State, False, False, True, "fixed"),
        rx.divider(
            border_color="black",
            # position="fixed",
            # top="98px",
        ),
        table(State),
    )
