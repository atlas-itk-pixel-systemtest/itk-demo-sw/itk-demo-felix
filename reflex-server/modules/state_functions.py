"""
It is the file where the content and layout of 
the page at http://localhost:3000/ are determined.

Ali Can Canbay - acanbay@cern.ch    |   9 Aug 2023

Last edit: 27 Aug 2023 by acanbay
"""
import reflex as rx
import modules.base as bs
from typing import Dict, List
import os
from os import listdir
from os.path import isfile, join
import asyncio
import json
import filecmp
from modules.FSM.felix_off import FelixOff
from modules.FSM.felix_initialized import FelixInitialized
from modules.FSM.felix_configured import FelixConfigured
from modules.FSM.felix_running import FelixRunning
from modules.FSM.felix_transitioning import FelixTransitioning

configurations_path = "configurations/"


class State(rx.State):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.FELIX_state = FelixOff()
        self.FELIX_state_name = "FelixOff"
        self.refresh()

        self.FELIX_states = {
            "FelixOff": FelixOff(),
            "FelixInitialized": FelixInitialized(),
            "FelixConfigured": FelixConfigured(),
            "FelixRunning": FelixRunning(),
            "FelixTransitioning": FelixTransitioning(),
        }

    configuration_Files: list
    selected_config: str
    is_uploading: bool

    delete_popup: bool = False

    def set_felix_state(self, stateName):
        if stateName not in self.FELIX_states:
            raise KeyError(f"State '{stateName}' not in list of available states.")
        self.FELIX_state = self.FELIX_states[stateName]
        self.FELIX_state_name = stateName

    target_status = "None"
    target_config: str = "New_Configuration.json"

    target_config_Device_IDs: List[int] = [0, 1]
    target_config_Device_IDs_bool: List[bool] = [False, False]
    target_config_Device_IDs_tag: List[str] = ["", ""]

    target_config_Links: List[List[int]] = [
        list(range(12)) for _ in range(len(target_config_Device_IDs))
    ]

    target_config_Links_bool: List[List[bool]] = [
        [False] * 12 for _ in range(len(target_config_Device_IDs))
    ]

    target_config_Links_comment: List[List[str]] = [
        [""] * 12 for _ in range(len(target_config_Device_IDs))
    ]

    target_config_ICEC: List[List[bool]] = [[False] * 12, [False] * 12]
    target_config_Polarity: List[List[bool]] = [[False] * 12, [False] * 12]

    target_config_possible_TXs: List[List[List[int]]] = [
        [
            [
                num
                for num in range(link * 64, (link + 1) * 64)
                if num // 64 == link and num % 2 == 0
            ][:8]
            for link in links
        ]
        for links in target_config_Links
    ]

    target_config_possible_TXs_bool: List[List[List[bool]]] = [
        [[False] * 8 for link in links] for links in target_config_Links
    ]

    target_config_possible_RXs: List[List[List[int]]] = [
        [
            [
                num
                for num in range(link * 64, (link + 1) * 64)
                if num // 64 == link and num % 4 == 0
            ][:6]
            for link in links
        ]
        for links in target_config_Links
    ]

    target_config_possible_RXs_bool: List[List[List[bool]]] = [
        [[False] * 6 for link in links] for links in target_config_Links
    ]

    Output: str = ""

    Optical_power: Dict[str, List[float]] = {"TX": [0] * 24, "RX": [0] * 24}
    Optical_power_bool: Dict[str, List[bool]] = {"TX": [False] * 24, "RX": [False] * 24}

    Optical_link_bool: List[bool] = [False] * 24
    Electrical_link_bool: Dict[int, List[bool]] = {
        0: [False] * 24,
        4: [False] * 24,
        8: [False] * 24,
        12: [False] * 24,
        16: [False] * 24,
        20: [False] * 24,
        24: [False] * 24,
        25: [False] * 24,
    }

    def update_config_params(self):
        data = bs.get_config_params()
        default_idx = data["Default"]

        config_files = []
        for file in data["Configs"].values():
            if default_idx != -1:
                if file == data["Configs"][default_idx]:
                    file += " (default)"
                    self.selected_config = file
            config_files.append(file)
        self.configuration_Files = config_files

    @rx.var
    def FELIX_button_text(self) -> str:
        if self.FELIX_state_name == "FelixRunning":
            FELIX_button_text = "Stop FELIX"
        elif self.FELIX_state_name == "FelixTransitioning":
            FELIX_button_text = "Wait"
        else:
            FELIX_button_text = "Start FELIX"
        return FELIX_button_text

    @rx.var
    def FELIX_status_color(self) -> str:
        if self.FELIX_state_name == "FelixRunning":
            FELIX_status_color = "green"
        elif self.FELIX_state_name == "FelixTransitioning":
            FELIX_status_color = "gray"
        else:
            FELIX_status_color = "red"
        return FELIX_status_color

    @rx.var
    def FELIX_current_status(self) -> str:
        FELIX_current_status = self.FELIX_state_name
        return FELIX_current_status

    # http://localhost:3000/
    def refresh(self):
        self.update_config_params()

    # http://localhost:3000/ and http://localhost:3000/configurations
    def set_default(self):
        bs.change_default_config(self.selected_config)
        self.update_config_params()

    # http://localhost:3000/ and http://localhost:3000/configurations
    def delete_selected_config(self):
        if "default" in self.selected_config:
            fname = self.selected_config[:-10]
        else:
            fname = self.selected_config

        bs.delete_config(fname)
        self.Output = f"{fname} is deleted"
        self.update_config_params()

    # http://localhost:3000/ and http://localhost:3000/configurations
    def change_delete_popup(self):
        self.delete_popup = not (self.delete_popup)

    # http://localhost:3000/ and http://localhost:3000/configurations
    def cancel_delete(self):
        self.delete_popup = not (self.delete_popup)

    # http://localhost:3000/ and http://localhost:3000/configurations
    def confirm_delete(self):
        self.delete_selected_config()
        self.delete_popup = not (self.delete_popup)

    # http://localhost:3000/
    async def init_FELIX(self):
        Output = await self.FELIX_state.init_FELIX(self.set_felix_state)
        self.Output = Output

    # http://localhost:3000/
    async def configure_FELIX(self):
        Output = await self.FELIX_state.configure_FELIX(self.set_felix_state)
        self.Output = Output

    # http://localhost:3000/
    async def change_Felix_Status(self):
        Output = await self.FELIX_state.change_Felix_Status(self.set_felix_state)
        self.Output = Output

    # http://localhost:3000/configurations
    def add_Files(self):
        Files = [
            f
            for f in listdir(".web/public/")
            if isfile(join(".web/public/", f)) and "." != f[0]
        ]
        for File in Files:
            if File == "favicon.ico":
                continue
            os.system(f"mv .web/public/{File} configurations")
        self.update_config_params()

    # http://localhost:3000/configurations
    async def handle_upload(self, files: List[rx.UploadFile]):
        self.is_uploading = True

        for file in files:
            upload_data = await file.read()
            outfile = rx.get_asset_path(file.filename)

            with open(outfile, "wb") as file_object:
                file_object.write(upload_data)

        return State.stop_upload

    # http://localhost:3000/configurations
    async def stop_upload(self):
        await asyncio.sleep(1)
        self.is_uploading = False
        self.add_Files()

    # http://localhost:3000/configurations
    def reset_target_config(self, target_config="New_Configuration.json"):
        self.target_config = target_config

        for dev in range(len(self.target_config_Device_IDs_bool)):
            self.target_config_Device_IDs_bool[dev] = False
            self.target_config_Device_IDs_tag[dev] = ""

            for link in range(len(self.target_config_Links_bool[dev])):
                self.target_config_Links_bool[dev][link] = False
                self.target_config_ICEC[dev][link] = False
                self.target_config_Polarity[dev][link] = False
                self.target_config_Links_comment[dev][link] = ""

                for num in range(len(self.target_config_possible_TXs_bool[dev][link])):
                    self.target_config_possible_TXs_bool[dev][link][num] = False

                for num in range(len(self.target_config_possible_RXs_bool[dev][link])):
                    self.target_config_possible_RXs_bool[dev][link][num] = False

    # http://localhost:3000/configurations
    def get_target_config_param(self):
        file = configurations_path + self.target_config
        with open(file) as json_str:
            data = json.load(json_str)

        for dev in range(len(self.target_config_Device_IDs)):
            if str(self.target_config_Device_IDs[dev]) in data["Devices"].keys():
                self.target_config_Device_IDs_bool[dev] = True

                device_id = str(self.target_config_Device_IDs[dev])
                self.target_config_Device_IDs_tag[dev] = data["Devices"][device_id][
                    "Tag"
                ]
            else:
                continue

            for link in range(len(self.target_config_Links[dev])):
                if (
                    str(self.target_config_Links[dev][link])
                    in data["Devices"][device_id]["Links"].keys()
                ):
                    self.target_config_Links_bool[dev][link] = True
                else:
                    continue

                link_id = str(self.target_config_Links[dev][link])

                if data["Devices"][device_id]["Links"][link_id]["ICEC"] == 1:
                    self.target_config_ICEC[dev][link] = True

                if data["Devices"][device_id]["Links"][link_id]["Polarity"] == 1:
                    self.target_config_Polarity[dev][link] = True

                self.target_config_Links_comment[dev][link] = data["Devices"][
                    device_id
                ]["Links"][link_id]["Comment"]

                for num in range(len(self.target_config_possible_TXs[dev][link])):
                    if (
                        self.target_config_possible_TXs[dev][link][num]
                        in data["Devices"][device_id]["Links"][link_id]["TX"]
                    ):
                        self.target_config_possible_TXs_bool[dev][link][num] = True
                    else:
                        continue

                for num in range(len(self.target_config_possible_RXs[dev][link])):
                    if (
                        self.target_config_possible_RXs[dev][link][num]
                        in data["Devices"][device_id]["Links"][link_id]["RX"]
                    ):
                        self.target_config_possible_RXs_bool[dev][link][num] = True
                    else:
                        continue

    # http://localhost:3000/configurations
    def change_view_status(self, Status):
        if Status == "Create":
            self.target_status = "Create"
            self.reset_target_config()

        elif Status in ["Inspect", "Edit"]:
            self.target_status = Status

            if " (default)" in self.selected_config:
                self.target_config = self.selected_config[:-10]
            else:
                self.target_config = self.selected_config
            self.reset_target_config(self.target_config)

            self.get_target_config_param()

        elif Status == "None":
            self.target_status = Status

    # http://localhost:3000/configurations
    def edit_target_config_param(self, param, dev, link=None, num=None, text: str = ""):
        if param == "Dev":
            self.target_config_Device_IDs_bool[
                dev
            ] = not self.target_config_Device_IDs_bool[dev]

        elif param == "Tag":
            self.target_config_Device_IDs_tag[dev] = text

        elif param == "Link":
            self.target_config_Links_bool[dev][
                link
            ] = not self.target_config_Links_bool[dev][link]

        elif param == "ICEC":
            self.target_config_ICEC[dev][link] = not self.target_config_ICEC[dev][link]

        elif param == "Polarity":
            self.target_config_Polarity[dev][link] = not self.target_config_Polarity[
                dev
            ][link]

        elif param == "Comment":
            self.target_config_Links_comment[dev][link] = text

        elif param == "TX":
            self.target_config_possible_TXs_bool[dev][link][
                num
            ] = not self.target_config_possible_TXs_bool[dev][link][num]

        elif param == "RX":
            self.target_config_possible_RXs_bool[dev][link][
                num
            ] = not self.target_config_possible_RXs_bool[dev][link][num]

    # http://localhost:3000/configurations
    def change_File_Name(self, text: str):
        self.target_config = text

    # http://localhost:3000/configurations
    def change_device_tag(self, form_data: dict):
        dev_str = list(form_data.keys())[0]
        tag = form_data[dev_str]
        self.edit_target_config_param("Tag", int(dev_str), text=tag)

    # http://localhost:3000/configurations
    def change_link_comment(self, form_data: dict):
        key = list(form_data.keys())[0]
        comment = form_data[key]
        data = key.split("_")
        dev = data[0]
        link = data[1]

        self.edit_target_config_param("Comment", int(dev), int(link), text=comment)

    # http://localhost:3000/configurations
    def write_config(self, form_data: dict):
        if form_data["fname"] != "":
            self.target_config = form_data["fname"]

        data = {"Devices": {}}

        for dev in range(len(self.target_config_Device_IDs)):
            if self.target_config_Device_IDs_bool[dev] == False:
                continue
            device_id = str(dev)
            data["Devices"] = {device_id: {}}
            data["Devices"][device_id]["Tag"] = self.target_config_Device_IDs_tag[dev]
            data["Devices"][device_id]["Links"] = {}

            for link in range(len(self.target_config_Links[dev])):
                if self.target_config_Links_bool[dev][link] == False:
                    continue

                link_id = str(link)

                data["Devices"][device_id]["Links"][link_id] = {}

                if self.target_config_ICEC[dev][link] == True:
                    data["Devices"][device_id]["Links"][link_id]["ICEC"] = 1
                else:
                    data["Devices"][device_id]["Links"][link_id]["ICEC"] = 0

                if self.target_config_Polarity[dev][link] == True:
                    data["Devices"][device_id]["Links"][link_id]["Polarity"] = 1
                else:
                    data["Devices"][device_id]["Links"][link_id]["Polarity"] = 0

                data["Devices"][device_id]["Links"][link_id][
                    "Comment"
                ] = self.target_config_Links_comment[dev][link]

                data["Devices"][device_id]["Links"][link_id]["TX"] = []
                data["Devices"][device_id]["Links"][link_id]["RX"] = []

                for num in range(len(self.target_config_possible_TXs[dev][link])):
                    if self.target_config_possible_TXs_bool[dev][link][num] == False:
                        continue
                    data["Devices"][device_id]["Links"][link_id]["TX"].append(
                        self.target_config_possible_TXs[dev][link][num]
                    )

                for num in range(len(self.target_config_possible_RXs[dev][link])):
                    if self.target_config_possible_RXs_bool[dev][link][num] == False:
                        continue
                    data["Devices"][device_id]["Links"][link_id]["RX"].append(
                        self.target_config_possible_RXs[dev][link][num]
                    )

        is_default = False
        if os.path.exists(configurations_path + self.target_config):
            if filecmp.cmp(
                configurations_path + self.target_config,
                configurations_path + "flx.cfg",
            ):
                is_default = True

        with open(configurations_path + self.target_config, "w") as outfile:
            json.dump(data, outfile, indent=2)

        if is_default:
            os.system(
                f"cp {configurations_path}{self.target_config} {configurations_path}/flx.cfg"
            )

        self.update_config_params()
        self.target_status = "None"

    # http://localhost:3000/monitoring
    def read_pod(self):
        data = bs.get_opower()

        for link in range(24):
            try:
                self.Optical_power["TX"][link] = float(data["TX"][link])
                self.Optical_power_bool["TX"][link] = False
                if float(data["TX"][link]) > 50.0:
                    self.Optical_power_bool["TX"][link] = True

                self.Optical_power["RX"][link] = float(data["RX"][link])
                self.Optical_power_bool["RX"][link] = False
                if float(data["RX"][link]) > 50.0:
                    self.Optical_power_bool["RX"][link] = True
            except KeyError:
                self.Optical_power["TX"][link] = 0
                self.Optical_power_bool["TX"][link] = False
                self.Optical_power["RX"][link] = 0
                self.Optical_power_bool["RX"][link] = False

    # http://localhost:3000/monitoring
    def read_link(self):
        data = bs.get_olink_configurations()

        for link in range(24):
            try:
                self.Optical_link_bool[link] = float(data[link])
            except KeyError:
                self.Optical_link_bool[link] = 0

    # http://localhost:3000/monitoring
    def read_elink(self):
        data = bs.get_elink_configurations()

        for link in range(24):
            self.Electrical_link_bool[0][link] = False
            if 0 in data[link]:
                self.Electrical_link_bool[0][link] = True
            self.Electrical_link_bool[4][link] = False
            if 4 in data[link]:
                self.Electrical_link_bool[4][link] = True
            self.Electrical_link_bool[8][link] = False
            if 8 in data[link]:
                self.Electrical_link_bool[8][link] = True
            self.Electrical_link_bool[12][link] = False
            if 12 in data[link]:
                self.Electrical_link_bool[12][link] = True
            self.Electrical_link_bool[16][link] = False
            if 16 in data[link]:
                self.Electrical_link_bool[16][link] = True
            self.Electrical_link_bool[20][link] = False
            if 20 in data[link]:
                self.Electrical_link_bool[20][link] = True
            self.Electrical_link_bool[24][link] = False
            if 24 in data[link]:
                self.Electrical_link_bool[24][link] = True
            self.Electrical_link_bool[25][link] = False
            if 25 in data[link]:
                self.Electrical_link_bool[25][link] = True
