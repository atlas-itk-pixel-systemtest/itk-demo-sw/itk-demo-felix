
class FelixState:
    def _undefinedError(self, endpoint=""):
        if endpoint == "":
            error_text = (
                f"This endpoint has not been implemented "
                f"for the current state '{self.name}'."
            )
        else:
            error_text = (
                f"The '{endpoint}' endpoint has not been implemented "
                f"for the current state '{self.name}'."
            )
        return error_text

    def __repr__(self):
        return f"<State object - {self.name}>"

    def __init__(self):
        self.name = ""

    async def init_FELIX(self, set_felix_state):
      return self._undefinedError()

    async def configure_FELIX(self, set_felix_state):
      return self._undefinedError()

    async def change_Felix_Status(self, set_felix_state):
      return self._undefinedError()



