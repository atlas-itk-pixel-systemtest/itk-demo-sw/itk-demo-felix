from modules.FSM.state import FelixState
import modules.base as bs

class FelixConfigured(FelixState):
    def __init__(self):
      self.name = "FelixConfigured"

    async def init_FELIX(self, set_felix_state):
      #TODO is this correct?
      Output = "FELIX is already configured"
      return Output

    async def configure_FELIX(self, set_felix_state):
      Output = "FELIX is already configured"
      return Output

    async def change_Felix_Status(self, set_felix_state):
      set_felix_state("FelixTransitioning")
      Output = await bs.start_FELIX(set_felix_state)
      return Output