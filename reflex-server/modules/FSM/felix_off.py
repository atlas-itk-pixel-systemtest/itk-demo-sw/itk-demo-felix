from modules.FSM.state import FelixState
import modules.base as bs

class FelixOff(FelixState):
    def __init__(self):
      self.name = "FelixOff"

    async def init_FELIX(self, set_felix_state):
      Output = await bs.init_FELIX(set_felix_state)
      return Output

    async def configure_FELIX(self, set_felix_state):
      Output = "FELIX is off, initialize first"
      return Output

    async def change_Felix_Status(self, set_felix_state):
      set_felix_state("FelixTransitioning")
      Output = await bs.start_FELIX(set_felix_state)
      return Output