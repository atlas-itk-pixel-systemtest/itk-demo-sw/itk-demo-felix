from modules.FSM.state import FelixState
import modules.base as bs

class FelixRunning(FelixState):
    def __init__(self):
      self.name = "FelixRunning"

    async def init_FELIX(self, set_felix_state):
      #TODO is this correct?
      Output = "FELIX is already running"
      return Output

    async def configure_FELIX(self, set_felix_state):
      #TODO is this correct?
      Output = "FELIX is already running"
      return Output

    async def change_Felix_Status(self, set_felix_state):
      Output = await bs.stop_FELIX(set_felix_state)
      return Output
      