from modules.FSM.state import FelixState

#Class added to the FSM for the 'Wait' state in the GUI
class FelixTransitioning(FelixState):
    def __init__(self):
      self.name = "FelixTransitioning"

    async def init_FELIX(self, set_felix_state):
      Output = "FELIX is currently starting "
      return Output

    async def configure_FELIX(self, set_felix_state):
      Output = "FELIX is currently starting "
      return Output

    async def change_Felix_Status(self, set_felix_state):
      Output = "FELIX is currently starting "
      return Output
      