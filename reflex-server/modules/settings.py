import config_checker as cc
from pydantic import validator
from typing import Union

FILE_DIR = "/config"  # os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


class ConfigSupport(cc.BaseConfig):
    runkey: str = "my_felix_runkey"
    search_dict: str = '{"serial":123}'
    configdb_key: str = "demi/default/itk-demo-configdb/api/url"

    CONFIG_SOURCES = cc.EnvSource(allow_all=True)


class FelixSettings(cc.BaseConfig):
    felix_app: int = 0
    felix_card_number: int = 0
    felix_device0_number: int = 0
    felix_device1_number: int = 1
    felix_device0_serial: str = None
    felix_device1_serial: str = None
    felix_initialize: int = 0
    felix_configure: int = 0
    dryrun: int = 0
    noflx: int = 0
    felix_config_file: str = "felix_config.json"
    felix_data_interface: str = "eth0"
    serial: Union[int, str] = 0
    sr_url: str = None

    @validator("noflx", "felix_initialize", "felix_configure", "dryrun", "noflx")
    def must_be_zero_or_one(cls, v):
        if v not in (0, 1):
            raise ValueError("must be 0 or 1")
        return v


class Links(cc.BaseConfig):
    polarity: int
    icec: int
    rx: list[int]
    tx: list[int]


class Devices(cc.BaseConfig):
    Links: dict[str, Links]


class FelixConfig(cc.BaseConfig):
    Card: int
    Devices: dict[str, Devices]


felix_settings = FelixSettings(config_sources=[cc.EnvSource(allow_all=True)])
