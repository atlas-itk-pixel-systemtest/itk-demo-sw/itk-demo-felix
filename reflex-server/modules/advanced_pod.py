"""
It is the file where the content and layout of 
the page at /opower (accessible from /monitoring via 'Advanced' button)
is determined determined.

Ali Can Canbay - acanbay@cern.ch    |   9 Aug 2023
Maren Stratmann maren.stratmann@cern.ch
"""
import reflex as rx
from GUI_components.navbar import navbar


def table_row(State, link):
    return [
        rx.td(rx.heading(f"{link} :", font_size="1.2em")),
        rx.td(
            rx.hstack(
                rx.box(
                    State.Optical_power["TX"][link],
                    width="100px",
                    border_width="medium",
                ),
                rx.text("μW"),
            )
        ),
        rx.td(
            rx.hstack(
                rx.box(
                    State.Optical_power["RX"][link],
                    width="100px",
                    border_width="medium",
                ),
                rx.text("μW"),
            )
        ),
    ]


# Construct the table
def datas(State):
    nlinks = [i for i in range(0, 24)]
    return rx.table_container(
        rx.table(
            rx.tbody(
                *[
                    rx.tr(
                        rx.td(rx.heading("Links", font_size="1.2em")),
                        rx.td(rx.heading("TX", font_size="1.2em")),
                        rx.td(rx.heading("RX", font_size="1.2em")),
                    ),
                    *[rx.tr(*table_row(State, link)) for link in nlinks],
                ]
            ),
            size="sm",
            variant="unstyled",
        )
    )


# Main module combining parts
def main(State):
    return rx.vstack(
        rx.heading("FELIX API - itk-felix-sw", font_size="3em"),
        rx.divider(
            border_color="black",
        ),
        # rx.hstack(
        navbar(State, False, False, False, "fixed"),
        #   rx.link(
        #     rx.button("Back", height="30px"),
        #     href="/monitoring",
        #     button=True,
        #   ),
        # ),
        rx.divider(
            border_color="black",
        ),
        rx.flex(
            rx.box(
                rx.vstack(
                    rx.heading(
                        "Optical Power",
                        font_size="1.5em",
                    ),
                    rx.button(
                        "Read",
                        on_click=State.read_pod,
                        height="30px",
                        width="60px",
                    ),
                ),
                # width="150px",
                padding=10,
            ),
            rx.spacer(),
            rx.center(datas(State)),
            rx.spacer(),
            width="50%",
            align="left",
        ),
    )
