FROM python:3.10-slim AS backend

ENV HOME=/root
WORKDIR ${HOME}


# RUN python3 -m pip install --no-cache-dir poetry
# ENV PATH $HOME/.local/bin:${PATH}
# RUN poetry config virtualenvs.in-project true

COPY . .

RUN python3 -m pip install --no-cache-dir --upgrade pip
RUN python3 -m pip install .


EXPOSE 3000
EXPOSE 8000

ENTRYPOINT [ "reflex" ]

RUN apt-get update \
&& apt-get install -y --no-install-recommends git bash sudo curl unzip xz-utils \
&& apt-get purge -y --auto-remove \
&& rm -rf /var/lib/apt/lists/*

RUN curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash - &&\
sudo apt-get install -y nodejs

#FROM node:16.9.0

#ENV HOME=/root
#WORKDIR ${HOME}
#COPY --from=backend /root .

RUN reflex init



