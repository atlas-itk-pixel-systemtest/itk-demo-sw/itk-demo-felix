import reflex as rx


def navbar(State, disable_home, disable_config, disable_monitoring, pos):
    return rx.box(
        rx.hstack(
            rx.spacer(),
            rx.link(
                rx.button("Home", height="30px", is_disabled=disable_home),
                href="/",
                button=True,
            ),
            #rx.link(
            #    rx.button("Configuration", height="30px", is_disabled=disable_config),
            #    href="/configuration",
            #    button=True,
            #),
            rx.link(
                rx.button("Monitoring", height="30px", is_disabled=disable_monitoring),
                href="/monitoring",
                button=True,
            ),
            rx.spacer(),
            rx.text(State.FELIX_current_status),
        ),
        # position=pos,
        # left="20px",
        # top="64px",
    )
