FROM registry.cern.ch/docker.io/library/nginx:stable-alpine

COPY --from=gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/frontend /app/.web/_static /usr/share/nginx/html/
#COPY ./nginx.conf /etc/nginx/conf.d/nginx.conf
CMD nginx -g 'daemon off;'

# ARG BASE_URL=gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix

# FROM ${BASE_URL}/frontend AS frontend

# # FROM registry.cern.ch/docker.io/library/nginx:stable-alpine
# # copied FROM tiangolo/uwsgi-nginx
# FROM python:3.11-bullseye

# COPY install-nginx-debian.sh /
# RUN bash /install-nginx-debian.sh

# COPY --from=frontend /app/.web/_static /usr/share/nginx/html/
# COPY ./nginx.conf /etc/nginx/nginx.conf
# COPY ./default.conf.template /etc/nginx/templates/default.conf.template

# # CMD envsubst \
# #     '${ETCD_HOST},${ETCD_PORT},${ETCD_NAMESPACE},${API_URL_KEY},${BACKEND_URL}'\
# #     < /etc/nginx/conf.d/nginx.conf.template > /etc/nginx/nginx.conf &&\
# #     nginx -g 'daemon off;'

# # CMD ["/usr/sbin/nginx","-g","'daemon off;'"]

# CMD nginx -g 'daemon off;'

# EXPOSE 80

# RUN pip install git+https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/pypi-packages/service-registry.git

# ARG S6_OVERLAY_VERSION=3.1.6.2

# ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz /tmp
# RUN tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz
# ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-x86_64.tar.xz /tmp
# RUN tar -C / -Jxpf /tmp/s6-overlay-x86_64.tar.xz

# COPY rootfs/ /

# ENV S6_VERBOSITY=2

# ENTRYPOINT ["/init"]




