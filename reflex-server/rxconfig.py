import os
import reflex as rx
from config_checker import BaseConfig, EnvSource, IPvAnyAddress, FileSource

# TO DO: replace by config-checker

# from dotenv import load_dotenv, dotenv_values

# load_dotenv()

# ev = dotenv_values()
# for k in ev:
#     print(f"{k}={ev[k]}")

# BACKEND_HOST = os.getenv("BACKEND_HOST", "localhost")
# BACKEND_HOSTPORT = os.getenv("BACKEND_HOSTPORT", 8000)
# BACKEND_PORT = os.getenv("BACKEND_PORT", 8000)


class ReflexserverConfig(rx.Config):
    pass


# class Config(BaseConfig):
#     backend_port: int = 8001
#     api_url: str = "http://localhost:8001"

#     CONFIG_SOURCES = [EnvSource(allow_all=True, file="../.env")]


# conf = Config()

config = ReflexserverConfig(
    app_name="reflex_server",
    # db_url="sqlite:///reflex.db",
    # backend_port : port where pynecone backend listen
    # backend_port=BACKEND_PORT,
    # backend_port=8000,
    # backend_port=conf.backend_port,
    #
    # api_url: url where the frontend will try to connect its websocket.
    # api_url=f"http://{BACKEND_HOST}:{BACKEND_HOSTPORT}",
    # api_url=conf.api_url,
    # api_url="http://pcatlitkflx06.cern.ch:8000/",
    #
    # bun_path=f"{os.getcwd()}/.bun/bin/bun",
    env=rx.Env.PROD,
    cors_allowed_origins=["*"],
    telemetry_enabled=False,
)
