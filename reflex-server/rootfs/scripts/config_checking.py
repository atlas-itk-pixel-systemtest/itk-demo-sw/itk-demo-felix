import config_checker as cc
import json, os, logging
from config_classes import FelixSettings, FelixConfig

logger = logging.getLogger(__name__)

FILE_DIR = "/config"  # os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
# Create the directory if it does not exist
os.makedirs(FILE_DIR, exist_ok=True)
os.makedirs("/run/s6/container_environment", exist_ok=True)


def main():
    # Use config from env vars and check them against pydantic model
    felix_settings = FelixSettings(config_sources=cc.EnvSource(allow_all=True))

    file = f"{FILE_DIR}/{felix_settings.felix_config_file}"
    try:
        with open(file) as f:
            try:
                json.load(f)
            except json.JSONDecodeError:
                logger.warning(
                    f"Config file {file} could not be checked, because it is not in json format."
                )
            else:
                felix_config = FelixConfig(
                    config_sources=cc.FileSource(
                        file=f"{FILE_DIR}/{felix_settings.felix_config_file}"
                    )
                )
    except FileNotFoundError:
        logger.error(f"Config file not found: {file}")

    settings_dict = felix_settings.dict()

    for key, value in settings_dict.items():
        print(key.upper())
        print(value)


if __name__ == "__main__":
    main()
