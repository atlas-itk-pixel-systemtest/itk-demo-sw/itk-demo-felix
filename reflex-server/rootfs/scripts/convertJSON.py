import json
import argparse
import os
import subprocess

NUM_DEVICES = 2  # device 0,1
NUM_LINKS = 12  # fiber link = 0..11
NUM_DEC_EGROUPS = 7  # decoder egroup = 0..5, but have to disable egroup #6
NUM_ENC_EGROUPS = 4  # encoder egroup = 0..3


polarities = []
cmds = {}


def add_cmd(dev,cmd):
    if dev not in cmds:
        cmds[dev] = []
    cmds[dev].append(cmd)

def setup_icec(dev, linkD, ena_icec):
    # ic
    add_cmd(dev, f"MINI_EGROUP_FROMHOST_{linkD}_IC_ENABLE=0x{ena_icec}")
    add_cmd(dev, f"MINI_EGROUP_TOHOST_{linkD}_IC_ENABLE=0x{ena_icec}")
    # ec
    add_cmd(dev, f"MINI_EGROUP_FROMHOST_{linkD}_EC_ENABLE=0x{ena_icec}")
    add_cmd(dev, f"MINI_EGROUP_TOHOST_{linkD}_EC_ENABLE=0x{ena_icec}")


def setup_tx(dev, linkD, elinks):
    if type(elinks) == int:
        elinks = [elinks]
    for egroup in range(NUM_ENC_EGROUPS):
        ena = 0x0
        enc = 0x0
        width = 0x1  # 4b for all Epaths
        elink0 = link * 64 + egroup * 4
        elink2 = elink0 + 2
        if elink0 in elinks:
            ena |= 0x1  # Epath0
            enc |= 0x0004  # 4 - RD53 (4b per Epaths) Epath0
#            if elink2 not in elinks:
#                enc = str(hex(enc)).replace('0x', '').zfill(4)
        if elink2 in elinks:
            ena |= 0x4  # Epath2
            enc |= 0x0400  # 4 - RD53 (4b per Epaths) Epath2
        enc = str(hex(enc)).replace('0x', '').zfill(4)

        cmdpref = f"ENCODING_LINK{linkD}_EGROUP{egroup}_CTRL_"
        add_cmd(dev, cmdpref + f"EPATH_ENA=0x{ena}")  # 0x1 - Epath0, 0x4 - Epath2, 0x5 - Epath0&2
        add_cmd(dev, cmdpref + f"EPATH_WIDTH=0x{width}")  # 0x1 - 4b for all Epaths
        add_cmd(dev, cmdpref + f"PATH_ENCODING=0x{enc}")  # 4 - RD53 (4b per Epaths); 0x0004 - Epath0; 0x0400 - Epath2; 0x0404 - Epath0&2


def setup_rx(dev, linkD, elinks):
    if type(elinks) == int:
        elinks = [elinks]
    for egroup in range(NUM_DEC_EGROUPS):
        ena = 0x0
        dec = 0x0
        width = 0x4  # 32b for all Epaths

        elink0 = link * 64 + egroup * 4
        elink1 = elink0 + 1
        if elink0 in elinks:
            ena |= 0x1  # Epath0 (Data Frames)
            dec |= 0x03  # 64b/66b encoding Epath0
        if elink1 in elinks:
            ena |= 0x2  # Epath1 (Register Frames)
            dec |= 0x30  # 64b/66b encoding Epath1
        dec = str(hex(dec)).replace('0x', '').zfill(4)

        cmdpref = f"DECODING_LINK{linkD}_EGROUP{egroup}_CTRL_"
        add_cmd(dev, cmdpref + f"EPATH_ENA=0x{ena}")  # 0x1 - Epath0, 0x4 - Epath2, 0x5 - Epath0&2
        add_cmd(dev, cmdpref + f"EPATH_WIDTH=0x{width}")  # 0x1 - 4b for all Epaths
        add_cmd(dev, cmdpref + f"PATH_ENCODING=0x{dec}")  # 4 - RD53 (4b per Epaths); 0x0004 - Epath0; 0x0400 - Epath2; 0x0404 - Epath0&2



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="Input file (json format)", type=str)
    parser.add_argument("-o", "--output", help="Output directory", type=str)
    args = parser.parse_args()

    if args.input is None:
        inputFile = "/config/flx.cfg.json"
    else:
        inputFile = args.input

    if args.output is None:
        outputFolder = "/config/"
    else:
        outputFolder = args.output+"/"


    with open(inputFile) as json_file:
        dataAll = json.load(json_file)

    card = dataAll["Card"]

    devices = dataAll["Devices"].keys()
    for device in range(card*2,card*2+NUM_DEVICES):
        devStr = str(device)
        if devStr in devices:
            links = dataAll["Devices"][devStr]["Links"].keys()
            for link in range(NUM_LINKS):
                linkD = "%02d" % int(link)
                linkStr = str(link)
                if linkStr in links:
                    #devPolarity = str( dataAll["Links"][linkStr]["polarity"] )+devPolarity
                    if dataAll["Devices"][devStr]["Links"][linkStr]["polarity"] == 0:  # default polarity (0 == default == reset)
                        polarities.append(["fgpolarity", "-c", str(card), "-G", str((device-2*card) * NUM_LINKS + link), "-r", "reset"])
                    else:  # reversed polarity (1 == reverse == set)
                        polarities.append(["fgpolarity", "-c", str(card), "-G", str((device-2*card) * NUM_LINKS + link), "-r", "set"])

                    # icec
                    if dataAll["Devices"][devStr]["Links"][linkStr]["icec"] == 1:
                        ena_icec = 1
                    else:
                        ena_icec = 0
                    setup_icec(devStr, linkD, ena_icec)
                    setup_tx(devStr, linkD, dataAll["Devices"][devStr]["Links"][linkStr]["tx"])
                    setup_rx(devStr, linkD, dataAll["Devices"][devStr]["Links"][linkStr]["rx"])

                else:
                    # devPolarity = "0"+devPolarity
                    setup_icec(devStr, linkD, 0)
                    setup_tx(devStr, linkD, [])
                    setup_rx(devStr, linkD, [])
        else:
            for link in range(NUM_LINKS):
                linkD = "%02d" % int(link)
                setup_icec(devStr, linkD, 0)
                setup_tx(devStr, linkD, [])
                setup_rx(devStr, linkD, [])

    # additional settings for device 0 or 2 (link wrapper)
    cmd2 = []
    cmd2.append(f"LPGBT_FEC=0xFFFFFFFFFFFF")
    cmds[str(card*2)] = cmd2 + cmds[str(card*2)]

    for device in range(card*2,card*2+NUM_DEVICES):
        devStr = str(device)
        outputFileName = os.path.split(inputFile)[-1].removesuffix('.json')
        outputFile = outputFolder+outputFileName+"-dev"+str(device)+".cfg"

        with open(outputFile, "w") as file:
            for cmd in cmds[devStr][:-1]:
                file.write(cmd + "\n")
            file.write(cmds[devStr][-1])

    for pol in polarities:
        subprocess.run(pol)

# for cmd in cmds[1:]:
#   print(f"flx-config -d {device} set {cmd}")

# os.system( f"mv {jsonFile} {jsonFile}.json" )
# os.system( f"mv {fileName} {jsonFile}" )
