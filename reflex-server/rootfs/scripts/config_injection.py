#!/usr/bin/env python3
import config_checker as cc
import json
import logging
import os
from typing import Union, Literal

logger = logging.getLogger(__name__)

FILE_DIR = "/config"  # os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
# Create the directory if it does not exist
os.makedirs(FILE_DIR, exist_ok=True)
os.makedirs("/run/s6/container_environment", exist_ok=True)


class UseConfigDB(cc.BaseConfig):
    use_configdb: bool = False

    CONFIG_SOURCES = cc.EnvSource(allow_all=True)


class ConfigSupport(cc.BaseConfig):
    runkey: str = "detlab_ob"
    search_dict: str = '{"serial": "card1"}'
    configdb_key: str = "demi/default/itk-demo-configdb/api/url"
    sr_url: str = "http://localhost:5111/api/"

    CONFIG_SOURCES = cc.EnvSource(allow_all=True)


class FelixSettings(cc.BaseConfig):
    felix_app: Literal[0, 1, 2] = 0
    felix_card_number: Literal[0, 1] = 0
    felix_device0_number: int = 0
    felix_device1_number: int = 1
    felix_initialize: Literal[0, 1] = 0
    felix_configure: Literal[0, 1] = 0
    dryrun: Literal[0, 1] = 0
    noflx: Literal[0, 1] = 0
    felix_config_file: str = "felix_config.json"
    felix_data_interface: str = "lo"
    felix_toflx_ip: cc.IPType = "localhost"
    felix_tohost_ip: cc.IPType = "localhost"
    felix_tohost_dcs_pages: int = 0
    regmap_version: str = "0x0500"
    serial: Union[int, str] = 0
    configdb_object_id: str = "unknown"


class Links(cc.BaseConfig):
    polarity: int
    icec: int
    rx: list[int]
    tx: list[int]


class Devices(cc.BaseConfig):
    Links: dict[str, Links]


class FelixConfig(cc.BaseConfig):
    Card: int
    Devices: dict[str, Devices]


def main():
    if UseConfigDB().use_configdb:
        # Load configs from configdb and check them against pydantic model
        config_support = ConfigSupport()

        felix_config = FelixConfig(
            config_sources=[
                cc.ConfigDBJsonSource(
                    configdb_key=config_support.configdb_key,
                    sr_url=config_support.sr_url,
                    runkey=config_support.runkey,
                    search={
                        "search_dict": json.loads(config_support.search_dict),
                        "object_type": "felix",
                        "config_type": "felix_config",
                    },
                ),
            ]
        )

        felix_settings = FelixSettings(
            config_sources=[
                cc.ConfigDBJsonSource(
                    configdb_key=config_support.configdb_key,
                    sr_url=config_support.sr_url,
                    runkey=config_support.runkey,
                    search={
                        "search_dict": json.loads(config_support.search_dict),
                        "object_type": "felix",
                        "config_type": "felix_meta",
                    },
                )
            ]
        )

        with open(f"{FILE_DIR}/{felix_settings.felix_config_file}", "w") as f:
            f.write(json.dumps(felix_config.dict()))

    else:
        # Use config from env vars and check them against pydantic model

        felix_settings = FelixSettings(config_sources=cc.EnvSource(allow_all=True))

        file = f"{FILE_DIR}/{felix_settings.felix_config_file}"
        try:
            with open(file) as f:
                try:
                    json.load(f)
                    felix_config = FelixConfig(
                        config_sources=cc.FileSource(
                            file=f"{FILE_DIR}/{felix_settings.felix_config_file}"
                        )
                    )
                except Exception:
                    logger.warning(
                        f"Config file {file} could not be checked, because it is not in json format."
                    )
        except FileNotFoundError:
            logger.error(f"Config file not found: {file}")

    settings_dict = felix_settings.dict()

    for key, value in settings_dict.items():
        with open(f"/run/s6/container_environment/{key.upper()}", "w") as f:
            f.write(str(value))

        print(key.upper())
        print(value)


if __name__ == "__main__":
    main()
