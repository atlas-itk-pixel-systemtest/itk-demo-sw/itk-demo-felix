#e.g: ./config_CalTrigSeq.sh 3 2 #3=DIGSCANSEQB, 2 is device number

SCANTYPE=$1 #1=digitalA, 2=analogA, 3=digitalB, 4=analogB

DIGSCANSEQA=("817e" "6969" "6363" "a971" "a66a" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "566a" "566c" "5671" "5672" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969")
ANASCANSEQA=("817e" "6969" "6363" "a66a" "716a" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "566a" "566c" "5671" "5672" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6363" "a96a" "6a6a" "6969") #diff analog scan
DIGSCANSEQB=("817e" "817e" "aaaa" "63a6" "a66c" "936a" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "4e6a" "566c" "5671" "5672" "2e74" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa")
ANASCANSEQB=("817e" "817e" "aaaa" "63a6" "6a6a" "716a" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "4e6a" "566c" "5671" "5672" "2e74" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "63a6" "a66a" "6a6a" "aaaa")



if [[ $SCANTYPE -eq 1 ]]
then
    echo "Digital Scan A"
    SEQ=("${DIGSCANSEQA[@]}")
elif [[ $SCANTYPE -eq 2 ]]
then
    echo "Analog Scan A"
    SEQ=("${ANASCANSEQA[@]}")
elif [[ $SCANTYPE -eq 3 ]]
then
    echo "Digital Scan B"
    SEQ=("${DIGSCANSEQB[@]}")
elif [[ $SCANTYPE -eq 4 ]]
then
    echo "Analog Scan B"
    SEQ=("${ANASCANSEQB[@]}")
else
  echo "SCANTYPE=$SCANTYPE not supported"
  exit 1
fi

#write into FPGA memory
flx-config -d $2 set YARR_FROMHOST_CALTRIGSEQ_WE=0x1

for ((i = 0 ; i < 32 ; i++)); do
    addr=$(printf '%x\n' $i)
    echo "flx-config -d $2 set YARR_FROMHOST_CALTRIGSEQ_WRADDR=0x${addr}"
    echo "flx-config -d $2 set YARR_FROMHOST_CALTRIGSEQ_WRDATA=0x${SEQ[i]}"
    #flx-config -d $2 set YARR_FROMHOST_CALTRIGSEQ_WRADDR=0x${addr}
    #flx-config -d $2 set YARR_FROMHOST_CALTRIGSEQ_WRDATA=0x${SEQ[i]}

done

#turn of WE
flx-config -d $2 set YARR_FROMHOST_CALTRIGSEQ_WE=0x0

