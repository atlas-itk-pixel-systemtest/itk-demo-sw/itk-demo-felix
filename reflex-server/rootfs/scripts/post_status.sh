post_status() {
  local serial=$1
  local status=$2

  if [[ ! -z "${serial}" ]]; then
    curl -s -o /dev/null -X 'POST' \
    "$SR_URL/set/" \
    -H 'accept: application/json' \
    -H 'Content-Type: application/json' \
    -d "{
    \"key\": \"demi/dashboard/status/${serial}\",
    \"value\": \"${status}\"
    }"
  fi
}