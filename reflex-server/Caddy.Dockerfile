FROM library/caddy

COPY --from=local/reflex-frontend /app/.web/_static /srv
ADD Caddyfile /etc/caddy/Caddyfile

