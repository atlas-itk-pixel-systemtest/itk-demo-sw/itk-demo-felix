import re
import json

regs = {}


def flx_config_list():
    with open("example_flx-config-list.txt") as f:
        lines = f.readlines()
        for l in lines:
            ls = re.split("\s+", l)

            if not len(ls[0]):
                continue

            try:
                offset = int(ls[0], 0)
            except Exception as e:
                print(f"Error: {ls[0]}")
                continue

            rw = ls[1]
            bits = ls[2]
            name = ls[3]
            try:
                value = int(ls[4], 0)
            except:
                print(f"Skipping: {ls[4]}")
                continue
            description = ls[5]

            print(f"{name} = {value}")

            regs[name] = value


def flx_config_store():
    with open("example_flx-config-store.txt") as f:
        lines = f.readlines()
        for li in lines:
            try:
                reg, val = re.split("=", li)
            except:
                print(f"Can't parse {li}")
                continue

            print(f"{reg} = {val}")
            regs[reg] = int(val, 0)


flx_config_store()
print(json.dumps(regs))
