FROM gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/containers/python/root/3.12:latest

WORKDIR /app

# Pass `--build-arg API_URL=http://app.example.com:8000` during build
ARG API_URL
ENV API_URL=${API_URL}

COPY . .
RUN poetry install

RUN apt-get update && apt-get install unzip
# Deploy templates and prepare app
RUN poetry run reflex init

# Export static copy of frontend to /app/.web/_static
RUN poetry run reflex export --frontend-only --no-zip

# Copy static files out of /app to save space in backend image
RUN mv .web/_static /tmp/_static
RUN rm -rf .web && mkdir .web
RUN mv /tmp/_static .web/_static