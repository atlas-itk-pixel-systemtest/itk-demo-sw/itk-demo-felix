# FROM gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/itk-demo-felix-base:latest
# FROM python:3.10-slim AS backend
FROM centos:7

ENV HOME=/root
WORKDIR ${HOME}

RUN yum install -y gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel tk-devel libffi-devel xz-devel gdbm-devel ncurses-devel db4-devel wget && yum clean all

RUN yum install -y git make
#RUN yum groupinstall -y "Development Tools"

SHELL [ "/bin/bash", "--login", "-o", "pipefail", "-c" ]

COPY ./python.sh .
RUN  ./python.sh

RUN git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
RUN echo 'eval "$(pyenv virtualenv-init -)"' >> ${HOME}/.bashrc

COPY ./pyproject.toml .
COPY ./reflex_server/ ./reflex_server
COPY ./README.md .


RUN pyenv virtualenv 3.9.17 venv &&\
    python3 -m pip install --no-cache-dir --upgrade pip &&\
    python3 -m pip install .

EXPOSE 3000
EXPOSE 8000

COPY ./nodejs.sh .
RUN ./nodejs.sh

COPY ./modules .
COPY ./configurations .
COPY ./outputs .

RUN reflex init

COPY ./start_reflex.sh .

# ENTRYPOINT [ "./start_reflex.sh" ]

