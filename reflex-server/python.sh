#!/usr/bin/env bash

# PYTHON

git clone --depth=1 https://github.com/pyenv/pyenv.git .pyenv
export PYENV_ROOT=$HOME/.pyenv
export PATH=$PYENV_ROOT/shims:$PYENV_ROOT/bin:$PATH
eval "$(pyenv init -)"
eval "$(pyenv init --path)"
PYTHON_VERSION=3.9.17
pyenv install -v ${PYTHON_VERSION}
echo 'export PATH="$HOME/.pyenv/bin:$PATH"' >> .bashrc
echo 'eval "$(pyenv init --path)"' >> .bashrc
# echo 'eval "$(pyenv virtualenv-init -)"' >> .bashrc
pyenv local ${PYTHON_VERSION}

# Poetry
# curl -sSL -o get_poetry.py https://install.python-poetry.org && python3 get_poetry.py
# export PATH=$HOME/.local/bin:${PATH}
# PYTHON_KEYRING_BACKEND keyring.backends.null.Keyring
# poetry config virtualenvs.in-project true
# poetry config virtualenvs.prefer-active-python true

#pip + global python packages
#python3 -m pip install --upgrade pip
#python3 -m pip install python-gitlab
