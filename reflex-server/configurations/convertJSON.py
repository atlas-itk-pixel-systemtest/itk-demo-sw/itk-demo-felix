import json, os
from modules.settings import felix_settings, FILE_DIR

configuration_path = "configurations/"

NUM_DEVICES = 2  # device 0,1
NUM_LINKS = 12  # fiber link = 0..11
NUM_DEC_EGROUPS = 7  # decoder egroup = 0..5, but have to disable egroup #6
NUM_ENC_EGROUPS = 4  # encoder egroup = 0..3


class bcolors:
    UNDERLINE = "\033[4m"
    FAIL = "\033[91m"
    OKGREEN = "\033[92m"
    ENDC = "\033[0m"


cmds = []


def add_cmd(cmd):
    cmds.append(cmd)


def setup_icec(dev, linkD, ena_icec):
    # ic
    add_cmd(f"MINI_EGROUP_FROMHOST_{linkD}_IC_ENABLE=0x{ena_icec}")
    add_cmd(f"MINI_EGROUP_TOHOST_{linkD}_IC_ENABLE=0x{ena_icec}")
    # ec
    add_cmd(f"MINI_EGROUP_FROMHOST_{linkD}_EC_ENABLE=0x{ena_icec}")
    add_cmd(f"MINI_EGROUP_TOHOST_{linkD}_EC_ENABLE=0x{ena_icec}")


def setup_tx(dev, linkD, elinks):
    if type(elinks) == int:
        elinks = [elinks]
    for egroup in range(NUM_ENC_EGROUPS):
        ena = 0x0
        enc = 0x0
        width = 0x1  # 4b for all Epaths
        elink0 = link * 64 + egroup * 4
        elink2 = elink0 + 2
        if elink0 in elinks:
            ena |= 0x1  # Epath0
            enc |= 0x0004  # 4 - RD53 (4b per Epaths) Epath0
        if elink2 in elinks:
            ena |= 0x4  # Epath2
            enc |= 0x0400  # 4 - RD53 (4b per Epaths) Epath2

        cmdpref = f"ENCODING_LINK{linkD}_EGROUP{egroup}_CTRL_"
        add_cmd(
            cmdpref + f"EPATH_ENA=0x{ena}"
        )  # 0x1 - Epath0, 0x4 - Epath2, 0x5 - Epath0&2
        add_cmd(cmdpref + f"EPATH_WIDTH=0x{width}")  # 0x1 - 4b for all Epaths
        add_cmd(
            cmdpref + f"PATH_ENCODING=0x{enc}"
        )  # 4 - RD53 (4b per Epaths); 0x0004 - Epath0; 0x0400 - Epath2; 0x0404 - Epath0&2


def setup_rx(dev, linkD, elinks):
    if type(elinks) == int:
        elinks = [elinks]
    for egroup in range(NUM_DEC_EGROUPS):
        ena = 0x0
        dec = 0x0
        width = 0x4  # 32b for all Epaths

        elink0 = link * 64 + egroup * 4
        elink1 = elink0 + 1
        if elink0 in elinks:
            ena |= 0x1  # Epath0 (Data Frames)
            dec |= 0x03  # 64b/66b encoding Epath0
        if elink1 in elinks:
            ena |= 0x2  # Epath1 (Register Frames)
            dec |= 0x30  # 64b/66b encoding Epath1

        cmdpref = f"DECODING_LINK{linkD}_EGROUP{egroup}_CTRL_"
        add_cmd(
            cmdpref + f"EPATH_ENA=0x{ena}"
        )  # 0x1 - Epath0, 0x4 - Epath2, 0x5 - Epath0&2
        add_cmd(cmdpref + f"EPATH_WIDTH=0x{width}")  # 0x1 - 4b for all Epaths
        add_cmd(
            cmdpref + f"PATH_ENCODING=0x{dec}"
        )  # 4 - RD53 (4b per Epaths); 0x0004 - Epath0; 0x0400 - Epath2; 0x0404 - Epath0&2


if __name__ == "__main__":
    inputFile = os.path.join(FILE_DIR, str(felix_settings.felix_config_file))

    outputFiles = os.path.join(
        FILE_DIR, str(felix_settings.felix_config_file).split(".")[0] + ".1cfg"
    )
    # for i in range(NUM_DEVICES):
    #     outputFiles.append(f"flx_dev{i}.cfg")

    with open(inputFile) as json_file:
        dataAll = json.load(json_file)

    devices = dataAll["Devices"].keys()

    dev = felix_settings.felix_device_number
    devPolarity = ""
    devStr = str(dev)
    if devStr in devices:
        links = dataAll["Devices"][devStr]["Links"].keys()
        for link in range(NUM_LINKS):
            linkD = "%02d" % int(link)

            linkStr = str(link)
            if linkStr in links:
                devPolarity = (
                    str(dataAll["Devices"][devStr]["Links"][linkStr]["Polarity"])
                    + devPolarity
                )

                # icec
                if dataAll["Devices"][devStr]["Links"][linkStr]["ICEC"] == 1:
                    ena_icec = 1
                else:
                    ena_icec = 0
                setup_icec(dev, linkD, ena_icec)
                # tx
                txs = dataAll["Devices"][devStr]["Links"][linkStr]["TX"]
                setup_tx(dev, linkD, txs)
                # rx
                rxs = dataAll["Devices"][devStr]["Links"][linkStr]["RX"]
                setup_rx(dev, linkD, rxs)

            else:
                devPolarity = "0" + devPolarity
                # icec
                setup_icec(dev, linkD, 0)
                # tx
                setup_tx(dev, linkD, [])
                # rx
                setup_rx(dev, linkD, [])

    else:
        for link in range(NUM_LINKS):
            linkD = "%02d" % int(link)
            devPolarity = "0" + devPolarity
            # icec
            setup_icec(dev, linkD, 0)
            # tx
            setup_tx(dev, linkD, [])
            # rx
            setup_rx(dev, linkD, [])

    devPolarity = hex(int(devPolarity, 2))
    cmd2 = []
    cmd2.append(f"GBT_DATA_RXFORMAT2=0xFFF")
    cmd2.append(f"GBT_RXPOLARITY={devPolarity}")
    cmd2.append(f"DECODING_REVERSE_10B=0x1")
    cmds = cmd2 + cmds

    with open(outputFiles, "w") as file:
        for cmd in cmds[:-1]:
            file.write(cmd + "\n")
        file.write(cmds[-1])
    cmds = []
