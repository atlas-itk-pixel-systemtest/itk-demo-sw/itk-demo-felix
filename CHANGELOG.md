# Changelog

All notable changes[^1] to the `itk-demo-felix` monorepo.

This repo is "living at head" which means tags correspond to the overall repo.
All subrepos/workspaces, all packages in the package registry and all container images in the image registry follow the repo tag.

[^1]: The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
(always keep this section to take note of untagged changes at HEAD)

## [demi-15] - 28-01-25
- GBT reset function fixed

## [demi-13] - 2024-11-21
 - Fixes to config-injection scripts
 - (05-00-04 base image) Soft error counter clearing command fixed

## [demi-12] - 2024-09-05

## [demi-11] - 2024-09-05
- Changed base-image of the felix-microservice to gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/felix-baseimage:05-00-04

## [demi-10] - 2024-08-27
- Added gbtreset endpoint
- Small bugfixes to monitoring view

## [demi-9] - 2024-05-15
- Added enforce query parameter to flxconfigset
- Bugfix for internal server error when sr_url is not defined

## [demi-8] - 2024-05-09
- Improved error handling for felix commands
- Changed health endpoint to only return http 200 statuscode
- New info endpoint added that return the state of the ms
- Bugfix in config injection

## [demi-7] - 2024-04-25
- Adapted pydantic model to new felix config format
- Added --free-cmem option to felix-tohost service for dev1
- Added new endpoint that serves SideBarContent for dashboard
- S6-services and HTTP-endpoints publish the felix-state to the SR

## [demi-6] - 2024-04-09

- FELIX container now supports entire felix card (two devices)
  - Note the new felix config format
- Added FELIX_CONFIGURE env var to define whether FELIX should be configured on startup
- Added s6 commands to PATH in base-image
- Added echo of felixstar commands
- Added free-cmem to felix-tohost command
- Support for felix-core in the http endpoints was dropped!
  - It is now only supported via the startup settings.

## [demi-5] - 2024-02-28

- Fix to default flx.cfg
- Fix to flx-init for S6_VERBOSITY < 2
- Added device 1 configuration to default flx.cfg config file
- Fixed bug in convertJSON.py which set TX values to decimal rather than hex
- Use correct env variable for FELIX device (felix-toflx & felix-tohost)
- Handle $DRYRUN the same way as in felixcore (felix-toflx & felix-tohost)

## [demi-4] - 2024-01-26

- Reworked config-injection in order for it to use the contenv functionality from s6
- Refactored config-checking into seperate script
- BREAKING CHANGE: docker compose command changed!
    New command: ["with-contenv", "bash", "--login", "-c", "reflex init && reflex run"]

## [demi-2] - 2024-01-26

### Fixed

- [MR] Fix to RECVPORT: Felixcore automatically adds the device number to the RECVPORT and herefore with the current code the chosen port for device 1 was 12342 instead of 12341.

## [demi-1] - 2024-01-22

- updated README
- moved and updated developer instructions to CONTRIBUTING
- added AUTHORS
- reflex-server/
  - bump reflex to 0.3.5
- base-image/
  - drop support for centos7
- Readded config injection on startup
- Added health endpoint
- Fixed various bugs in felix endpoints

## [0.10.4] - 2023-12-07

### Fixed
- Issue with config validation if config file was not in json format
  - File will only be checked if it is in valid json format

### Added
  - Improved implementation of config-checker
    - Now also checks local json config file (on startup)


## [0.10.3] - 2023-11-30

### Added
  - Improved implementation of config-checker
    - Now also checks local json config file (on startup)

## [0.10.2] - 2023-11-30

### Added
  - Pydantic model for felix json config (if read from configDB)

## [0.10.1] - 2023-11-30

### Fixed
  - Added device option to all flx-config commands in base.py

## [0.10.0] - 2023-11-28

### Added
 - FSM and GUI improvements
 - ConfigDB access on container startup (to get configs and settings)
 - New nginx dockerfile
 - Example docker compose file

## [0.9.3] - 2023-07-25

### Added

- separate frontend and backend servers
- add release section to CI
- reflex_server functional

### Removed

- previous api and ui moved to attic

### Changed

- add alma9 (centos9) base-image
- use felix-image `master` tag for `latest` tag

## [0.9.2] - 2023-07-18

### Added

- Change to monorepo structure.
- Add base-image/ lightweight layer on top of FELIX image.
  - It's a complete subrepo with separate standard scripts, Dockerfile, compose file README and child pipeline.
- Update READMEs.
- Add this CHANGELOG.
- Clean up moving unused stuff to the attic/.

### Fixed

- CI pipeline YAML.

## [0.6.2] - 2023-04-09

### Added

- Last tag before starting changelog. This and older tags not documented here.

## [0.6.0] - 2023-04-08

## [0.5.1] - 2023-04-08

## [0.5-alpha] - 2023-04-06

## [0.4-alpha] - 2023-04-06

## [0.3-alpha] - 2023-04-06

## [0.2-alpha] - 2023-03-24

### Added

- initial revision: 2022-05-31

[0.6.2]: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/-/compare/0.6.0...0.6.2
[0.6.0]: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/-/compare/0.5.1...0.6.0
[0.5.1]: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/-/compare/0.5-alpha...0.5.1
[0.5-alpha]: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/-/compare/0.4-alpha...0.5-alpha
[0.4-alpha]: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/-/compare/0.3-alpha...0.4-alpha
[0.3-alpha]: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/-/compare/0.2-alpha...0.3-alpha
[0.2-alpha]: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/-/tree/0.2-alpha
