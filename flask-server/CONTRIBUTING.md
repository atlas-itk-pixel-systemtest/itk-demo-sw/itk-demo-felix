# CONTRIBUTING to itk-demo-felix

## Development Method 1: Local Setup (git clone)

1. Clone Git repository:

```shell

git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix.git

cd itk-demo-felix
```

2. Write configuration to `.env` files using `config` script:

```shell
./config
```

This writes a default config mostly initialized by the `config` script in the
[`defaults/` repository](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/defaults).
Also, `.env` and interpolated `docker-compose.yml` is written to a temporary `instance` folder within the repo directory
(orchestration scripts may write multiple instances in case multiple FELIX containers should be started).

The container is completely managed by `docker compose` which can now be run without having to set further environment variables:

3. To check the instance environment is correct, run

`docker compose config`

4.

- To pull the images run

`docker compose pull`

- To build the image

If you want to build a local version of the image for development, simply do:

```shell
docker compose build
```

(Normally the images are built by the GitLab CI/CD (see `gitlab-ci.yml`).
The image should be flexible enough that all configuration is possible at runtime.
If this is not the case, please contact us!)

5. To start the FELIX container

`docker compose up [-d]`

If you want to pass further environment variables (or override those in `.env`) you can do so on the cmdline, eg. to start felixcore and use the network on `eno1`:

```shell
DATA_INTERFACE=eno1 FLX_START_APP=1 docker compose up [-d]
```

6. FELIX commands and tools can now be run from the host in the running container. Some shortcuts are provided in the taskfile `tasks`, for example

```shell
./tasks flx-info
```

arbitrary commands can be run eg. like this:

```shell
docker compose exec api bash --login -c flx-info link
```

(TO DO: there will be shims to call the FELIX commands in the right container automatically)

### Method 2: Use a Python virtual environment to run natively

Activate and use the venv:

```shell
poetry shell
poetry update
poetry install
```

Test the `itk-demo-felix` CLI locally:

`itk-demo-felix`

Run the API development server locally:

`flask run`

Start the [gunicorn](https://gunicorn.org/) web server locally:

`tasks serve`

n.b. this will also require local setup of FELIX software. Check the original instructions to do that or look into the Dockerfile how we do it.

The Flask Python app lives in the `src/itk_demo_felix` package directory.

### Modifying the configuration and start-up procedure

Configuration is injected at runtime via

- `docker-compose.yml` directly (local hacks only - better implement a variable in `config`).
- `./config` (which writes `.env`).
- Environment variables which can override certain values in the scripts (check inside).
- via a runkey read from the ConfigDb during the s6 startup, see below

s6-init

- The `s6-init` directory structure is copied from `./root` to `/` in the container during image building.
- The `s6-rc` script to init + configure FELIX is at `root/etc/s6-overlay/s6-rc.d/init-felixcore/run`. This calls scripts copied from `scripts/`.
- eg. the `s6-rc` script to start `felixcore` is at `root/etc/s6-overlay/s6-rc.d/svc-felixcore/run`.

### reflex API server and UI in `reflex-server/`

The new API+UI is defined in Python only using the [reflex](https://reflex.dev/) framework.
The React UI is generated by "compiling" the reflex code.

### Flask API + React + Patternfly UI (depreceated)

#### Flask API server app in `api/`

Frontend and API communicate via a REST API described in
[OpenAPI 3.0](https://swagger.io/docs/specification/about/).

The API description file is dynamically converted to Flask routes using the
spec-first Python module [connexion](https://github.com/zalando/connexion).

The Flask app was originally adapted from [this blogpost](https://mark.douthwaite.io/getting-production-ready-a-minimal-flask-app/).

The app controls the FELIX sw through calls in the backend.

#### Frontend in `ui/`

The frontend is a completely separate image (and container ) in the `ui/` subdirectory.

It is based on `create-react-app` and uses Patternfly 4 (which is based
on Bootstrap 4) components and layouts.

Links:
[JavaScript](https://www.w3schools.com/js/DEFAULT.asp) |
[React.js](https://reactjs.org/) |
[Patternfly](https://www.patternfly.org/v4/) |
[Bootstrap](https://getbootstrap.com/)

To develop locally:

1. Install and build the React toolchain based on `create-react-app`

```shell
cd ui/
nvm use stable
npm i
npm run build
```

2. You can test the transpiled static React app alone using [`serve`](https://www.npmjs.com/package/serve):

```shell
serve -s build
```

## Tagging

- Define a full [SEMVER](https://semver.org/) `<tag>`. This SEMVER tag needs to be [PyPA Version Specification](https://packaging.python.org/en/latest/specifications/version-specifiers/#version-specifiers) compatible to be able to be used with Python packages as well! i.e. use full `<tag>`=`<MAJOR>.<MINOR>.<PATCH>` format.
- If in doubt contact the `AUTHORS`.
- Update `CHANGELOG.md` according to [Keep a changelog 1.1.0](https://keepachangelog.com/en/1.1.0).
- Commit last changes for `<tag>`.

```shell
git commit -m "<useful message>"
```

- Tag commit

```shell
git tag <MAJOR>.<MINOR>.<PATCH>
```

- Push `<tag>`

```shell
git push origin <MAJOR>.<MINOR>.<PATCH>
```

If the repo is pushed without tagging, the `latest` tag is used.

## Publishing

Packages and images follow the `<tag>` of the Git repo, see above.
Method depends on language.


### Packages

- publish all packages using the Git tag as package version to the
  [package registry](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/-/packages).

### Docker images

The Gitlab Docker image tag format is `<registry>/<organization>/<repo>/<image>:<tag>` where

|                 |                                           |
| --------------- | ----------------------------------------- |
| `<registry>`    | `gitlab-registry.cern.ch`                 |
| `<organisation>`    | `atlas-itk-pixel-systemtest/itk-demo-sw/` |
| `<repo>`        | `itk-demo-felix`                          |
| `<image>:<tag>` | see below                                 |

| Source Directory | Name                    | Tag    | Example                           |
| ---------------- | ----------------------- | ------ | --------------------------------- |
| `base-image/`    | `base-<upstream_version>` | \<tag> | _base-05-00-02-rm5-centos9:1.0.0_ |
|                  |                         |        | _base-master-rm5-centos9:1.0.0_   |
| `reflex-server/` | api                     | \<tag> | _api:1.0.0_                       |
| `reflex-server/` | frontend                | \<tag> | _frontend:1.0.0_                  |
| `reflex-server/` | ui                      | \<tag> | _ui:1.0.0_                        |

depreceated:

| `api/` | api | \<tag> | _itk-demo-felix/api:1.0.0_ |
| `ui/` | ui | \<tag> | _itk-demo-felix/ui:1.0.0_ |

5. The CI will

- build all images and tag using the Git tag as image tag and push to the
  [container registry](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/container_registry)
