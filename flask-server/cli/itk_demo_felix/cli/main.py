import getpass
import logging
import os
import re
import sys
import subprocess
from pathlib import Path

import typer
from dotenv import load_dotenv
from python_on_whales import DockerClient
from rich import print as rprint
from rich.logging import RichHandler

# Strip 7-bit C1 ANSI sequences
ansi_escape = re.compile(
    r"""
    \x1B  # ESC
    (?:   # 7-bit C1 Fe (except CSI)
        [@-Z\\-_]
    |     # or [ for CSI, followed by a control sequence
        \[
        [0-?]*  # Parameter bytes
        [ -/]*  # Intermediate bytes
        [@-~]   # Final byte
    )
""",
    re.VERBOSE,
)

FORMAT = "%(message)s"
logging.basicConfig(
    level=logging.DEBUG,
    format=FORMAT,
    datefmt="[%X]",
    handlers=[RichHandler()],
)
log = logging.getLogger("rich")

# TO DO: Use our own configuration library
load_dotenv()

# # @typer_app.callback()
# def callback():
#     """
#     itk-demo-felix Command Line Tool
#     """
#     print("running callback")
#     r = bootstrap()
#     if not r:
#         raise typer.Exit()


typer_app = typer.Typer(
    no_args_is_help=True,
    # callback=callback,
)

cwd = Path(os.getcwd())

# TO DO: read from environment
site = getpass.getuser()
stack = "itk-demo-felix"

inst = f"{cwd}/instance/{site}/{stack}"

docker = DockerClient(
    compose_project_directory=inst,
    compose_files=[f"{inst}/docker-compose.yml"],
)


@typer_app.command("config")
def config():
    """Configure instances by calling config script

    Returns shell-style returncode (0: ok, 1: error)
    """
    log.info(f"Configuring {inst}")

    # 2. config
    config_script = cwd / "libexec/config"

    log.debug(f"Script {config_script}")

    r = subprocess.run(
        config_script,
        capture_output=True,
        text=True,
        # shell=True,
        # stdout=subprocess.PIPE,
        # stderr=subprocess.PIPE,
        cwd=cwd,
        env={
            "VERBOSE": "1",
            "DEMI_SITE": site,
            "DEMI_STACK": stack,
        },
    )

    rprint(ansi_escape.sub("", r.stdout))
    if r.returncode:
        log.error(f"Configuration errors [{r.returncode}]:")
        rprint(ansi_escape.sub("", r.stderr))
    return r.returncode


# @typer_app.command()
# def serve():
#     """Locally run gunicorn to serve API"""

#     API_PORT = os.environ.get("API_PORT", 5009)
#     subprocess.run(
#         ["gunicorn", f"{__package__}.wsgi:app", "--bind", f"0.0.0.0:{API_PORT}"]
#     )

#     # from gunicorn.app.wsgiapp import WSGIApplication
#     # gapp.run("wsgi:app", f"--bind 0.0.0.0:${PORT}")


@typer_app.command()
def up():
    """Docker compose up"""
    log.debug(f"docker compose up")
    docker.compose.up()


@typer_app.command()
def down():
    """Docker compose down"""
    log.debug(f"docker compose down")
    docker.compose.down()


@typer_app.command()
def shell():
    """Docker compose run bash"""
    log.debug(f"Shell")
    docker.compose.run("bash")


@typer_app.command()
def check():
    """Run bootstrap script to check prerequisites are fulfilled"""

    bootstrap_script = cwd / "libexec/bootstrap"

    log.debug(f"Running {bootstrap_script}")

    r = subprocess.run(
        bootstrap_script,
        capture_output=True,
        text=True,
        cwd=cwd,
    )

    rprint(ansi_escape.sub("", r.stdout))
    if r.returncode:
        log.error(f"Prerequisites are not [{r.returncode}]:")
        rprint(ansi_escape.sub("", r.stderr))
    return r.returncode


def app():
    # bootstrap()
    typer_app()


if __name__ == "__main__":
    app()
