# itk-demo-felix CLI tool

## Method 1: Run the tool in a Docker container using the provided `itk-demo-felix` script

```shell
curl -sSLO https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/-/raw/main/itk-demo-felix
chmod a+x itk-demo-felix
./itk-demo-felix
```

## Method 2: Install the tool using pip

Additional prerequisites:

- Working [Python >3.9](https://demi.docs.cern.ch/general/python/) installation.
- Ability to install PyPI packages, eg. in a Python virtual environment.

Install from the prebuilt package in the GitLab PyPI package registry:

```shell
pip install itk-demo-felix-cli --index-url https://gitlab.cern.ch/api/v4/projects/137084/packages/pypi/simple
itk-demo-felix
```

Install directly from the Git repo (pip will build the package locally):

```shell
pip install git+https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix#subdirectory=cli
itk-demo-felix
```

## Method 3: Clone this repo and call the tool from the checked out repo

```shell
git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix.git
cd itk-demo-felix
./itk-demo-felix
```
