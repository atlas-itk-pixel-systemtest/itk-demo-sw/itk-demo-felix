from tests.parent_test import ParentTest


class TestExamples(ParentTest):
    def test_health(self):
        result = self.get("health")
        assert result["data"] == "OK"

    def test_post(self):
        dic = {"data": "Hello"}
        result = self.post(dic, "post_example")
        assert result["data"] == "Hello back!"

    def test_post_missing_data(self):
        dic = {}
        result = self.post(dic, "post_example")
        assert result["data"] == "'data' is a required property"

    def test_404(self):
        result = self.get("test")
        assert (
            result["data"]
            == "The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again."
        )
