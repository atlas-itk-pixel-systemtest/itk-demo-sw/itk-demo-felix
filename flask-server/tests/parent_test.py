import json

from itk_demo_felix.app import create_app


class ParentTest:
    def setup_method(self):
        self.headers = {"content-type": "application/json"}
        self.app = create_app()
        self.client = self.app.app.test_client()

    def teardown_method(self):
        pass

    def post(self, con, method):
        res = self.client.post("/" + method, data=json.dumps(con), headers=self.headers)
        return json.loads(res.data)

    def get(self, method):
        res = self.client.get("/" + method, headers=self.headers)
        return json.loads(res.data)
