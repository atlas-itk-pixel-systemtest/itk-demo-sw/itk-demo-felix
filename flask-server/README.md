# `itk-demo-felix` Containerized FELIX Software + Microservice

## Overview

Containerized wrapper for the FELIX software.

This `README` contains the user instructions.
For developer instructions see `CONTRIBUTING`.

The `itk-demo-felix` microservice consists of the following OCI images:

1. **[`itk-demo-felix-base`](gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/itk-demo-felix-base:latest)**

    Based on the [FELIX software image](https://gitlab.cern.ch/atlas-tdaq-felix/felix-image) from the FELIX group adding the following features:

    - [s6-overlay](https://github.com/just-containers/s6-overlay) for process management (replacing supervisord used by the FELIX group).
      - FELIX cards are automatically initialized and configured when the container starts.
      - If FELIX is successfully configured the FELIX application is run (optionally).
      - Option to use configuration in summary JSON-format.
      - If the FELIX application crashes, it is automatically restarted by the `s6-init` system.
  
2. **[`itk-demo-felix-api`](gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/itk-demo-felix-api:latest)**

    This is the base image plus the following features:

    - A Flask API server served by `gunicorn` for remote control of the FELIX SW.

3. **[`itk-demo-felix-ui`](gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/itk-demo-felix-ui:latest
)**

  React UI to use the API from a web browser.

4. **[`itk-demo-felix-cli`](gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix/itk-demo-felix-api:latest)**

   Command line tool `itk-demo-felix` to manage the other two containers.

```mermaid
graph TD
  id3[itk-demo-felix-cli]
  ide[felix-image]
  id0[itk-demo-felix-base]
  id1[itk-demo-felix-api]
  id2[itk-demo-felix-ui]
  ide-->id0
  id0-->id1
  id2<-. http:// + JSON .-> id1
  id3-- manage --> id1
  id3-- manage --> id2
```

## User Instructions

### Prerequisites

- [Working Docker installation](https://demi.docs.cern.ch/containers/docker/) with `docker compose` plug-in (or compatible runtime / compose tool).
- Working FELIX card(s) installed in PC.
- Compatible FELIX firmware flashed to the FELIX card(s).
- FELIX driver installed (natively for the time being).

|`itk-demo-felix` Release tag | felix-image tag used |
|--|--|
| latest | master-rm5-centos7 |
| 0.9.2 | 05-00-02-rm5-centos7 |

No further local installation / setup of the FELIX software is neccessary.

The prerequisites can be checked with the `./bootstrap` script.

### Method 1: Run in a Deployment Stack

The simplest method to use these containers is via a deployment orchestration stack.
The FELIX containers are for example part of the
[`opto-stack`](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix) ([instructions](https://demi.docs.cern.ch/opto-stack/)).

### Method 2: Run using the Python CLI tool `itk-demo-felix`

[see tool README](cli/README)

### Method 3: Run natively

```shell
./bootstrap       # check dependencies
./config          # write configured instance(s)
cd api/
docker compose up # pull API image and start API
cd ../ui/
docker compose up # pull UI image and start UI
```

### Method 4: Run in the development container

Devcontainers are practical if you are working in an environment that does not have all required development dependencies installed locally.

Devcontainers work easiest with the VSCode editor where they can be built on the fly and accessed in the built-in terminal.
However, we do not use any of the proprietary devcontainer features in order to not lock users into VSCode.
The Dockerfile / docker-compose.yml are sufficient.
To use it independently from VScode:

```shell
git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-felix.git
cd itk-demo-felix/.devcontainer
```

Using Docker:

```shell
docker build -t itk-demo-felix-dev .
docker run \
  --rm \
  -it \
  -v $(realpath ..):/workspace \
  -v /var/run/docker.sock:/var/run/docker.sock.host \
  itk-demo-felix-dev \
  bash --login
```

Using docker compose:

```shell
docker compose run --rm --build dev
```

Inside the container a user called `itkpix` is used per default.
This `itkpix` user has sudo rights (in the container) and can use Docker without sudo.
Please note that this user is different from a local `itkpix` user you may be using.

By default the local working directory is mounted at `/workspace`.
So, to work locally but with all the development tools available go to:

```shell
cd /workspace
```

### Method 5: Use Shims

Once the FELIX API container is up and running, you can source the `setup` script to install the itk-demo-felix shims for all commands defined in `aliases.list`.

The shims allow to run FELIX commands inside the container as if they were available locally.

If there are several instances, the instance defined via the .env file in the current working directory is used.

### Configuration

For all configuration details see the base-image README.

#### FELIX configuration files

At startup the FELIX cards are configured with `flx-config` using the following dump files:

```shell
/config/flx0.cfg
```

If the files are not found, nothing is configured.
To use files from the host system mount the corresponding directory into the container at the standard `/config` location.

#### Environment Variables

| Variable | Purpose |
|--|--|
| START_API | Start gunicorn+Flask API server 1=yes 0=no |
| FLX_START_APP | Start FELIX Application 0=none 1=felixcore 2=felix-star |

